﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef18_1
{
    class Spaarrekening : Rekening
    {
        // Attributen
        private double _percentage;


        // Properties

        public double Percentage
        {
            get { return _percentage; }
            set { _percentage = value; }
        }


        // Constructor 
        public Spaarrekening() : base("", 0.0)
        {

        }


        // Methods
        public void Schrijfrentebij()
        {
            Saldo += Saldo * Percentage/100;
        }

        public override string ToString()
        {
            return base.ToString() + $" (rente percentage {Percentage}%)";
        }

    }
}
