﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_5
{
    class Bewerking
    {
        // Variabelen
        private int _getal1;
        private int _getal2;

        // Properties
        public int Getal1
        {
            get { return _getal1; }
            set
            {
                _getal1 = value;
            }
        }

        public int Getal2
        {
            get { return _getal2; }
            set
            {
                _getal2 = value;
            }
        }

        // Constructor
        public Bewerking() { }
        public Bewerking(int getal1, int getal2)
        {
            Getal1 = getal1;
            Getal2 = getal2;
        }

        // Methods
        public int Som()
        {
            return Getal1 + Getal2;
        }

        public int Verschil()
        {
            return Getal1 - Getal2;
        }

        public int Product()
        {
            return Getal1 * Getal2;
        }

        public int Quotient()
        {
            return Getal1 / Getal2;
        }
    }
}
