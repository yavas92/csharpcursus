﻿namespace Oef15_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGetal1 = new System.Windows.Forms.Label();
            this.lblGetal2 = new System.Windows.Forms.Label();
            this.txtGetal1 = new System.Windows.Forms.TextBox();
            this.txtGetal2 = new System.Windows.Forms.TextBox();
            this.btnGenereer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnSom = new System.Windows.Forms.RadioButton();
            this.rbtnVerschil = new System.Windows.Forms.RadioButton();
            this.rbtnQuotient = new System.Windows.Forms.RadioButton();
            this.rbtnProduct = new System.Windows.Forms.RadioButton();
            this.lblResultaat = new System.Windows.Forms.Label();
            this.lblWatDenkJe = new System.Windows.Forms.Label();
            this.txtResultaat = new System.Windows.Forms.TextBox();
            this.txtResultaatGebruiker = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btnBereken = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.btnHulp = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGetal1
            // 
            this.lblGetal1.AutoSize = true;
            this.lblGetal1.Location = new System.Drawing.Point(12, 19);
            this.lblGetal1.Name = "lblGetal1";
            this.lblGetal1.Size = new System.Drawing.Size(44, 13);
            this.lblGetal1.TabIndex = 0;
            this.lblGetal1.Text = "Getal 1:";
            // 
            // lblGetal2
            // 
            this.lblGetal2.AutoSize = true;
            this.lblGetal2.Location = new System.Drawing.Point(12, 46);
            this.lblGetal2.Name = "lblGetal2";
            this.lblGetal2.Size = new System.Drawing.Size(44, 13);
            this.lblGetal2.TabIndex = 0;
            this.lblGetal2.Text = "Getal 2:";
            // 
            // txtGetal1
            // 
            this.txtGetal1.Location = new System.Drawing.Point(102, 16);
            this.txtGetal1.Name = "txtGetal1";
            this.txtGetal1.Size = new System.Drawing.Size(100, 20);
            this.txtGetal1.TabIndex = 1;
            // 
            // txtGetal2
            // 
            this.txtGetal2.Location = new System.Drawing.Point(102, 43);
            this.txtGetal2.Name = "txtGetal2";
            this.txtGetal2.Size = new System.Drawing.Size(100, 20);
            this.txtGetal2.TabIndex = 1;
            // 
            // btnGenereer
            // 
            this.btnGenereer.Location = new System.Drawing.Point(242, 16);
            this.btnGenereer.Name = "btnGenereer";
            this.btnGenereer.Size = new System.Drawing.Size(75, 47);
            this.btnGenereer.TabIndex = 2;
            this.btnGenereer.Text = "Genereer";
            this.btnGenereer.UseVisualStyleBackColor = true;
            this.btnGenereer.Click += new System.EventHandler(this.btnGenereer_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnProduct);
            this.groupBox1.Controls.Add(this.rbtnQuotient);
            this.groupBox1.Controls.Add(this.rbtnVerschil);
            this.groupBox1.Controls.Add(this.rbtnSom);
            this.groupBox1.Location = new System.Drawing.Point(15, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(302, 89);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Berekening";
            // 
            // rbtnSom
            // 
            this.rbtnSom.AutoSize = true;
            this.rbtnSom.Checked = true;
            this.rbtnSom.Location = new System.Drawing.Point(18, 19);
            this.rbtnSom.Name = "rbtnSom";
            this.rbtnSom.Size = new System.Drawing.Size(46, 17);
            this.rbtnSom.TabIndex = 0;
            this.rbtnSom.TabStop = true;
            this.rbtnSom.Text = "Som";
            this.rbtnSom.UseVisualStyleBackColor = true;
            // 
            // rbtnVerschil
            // 
            this.rbtnVerschil.AutoSize = true;
            this.rbtnVerschil.Location = new System.Drawing.Point(18, 53);
            this.rbtnVerschil.Name = "rbtnVerschil";
            this.rbtnVerschil.Size = new System.Drawing.Size(62, 17);
            this.rbtnVerschil.TabIndex = 0;
            this.rbtnVerschil.Text = "Verschil";
            this.rbtnVerschil.UseVisualStyleBackColor = true;
            // 
            // rbtnQuotient
            // 
            this.rbtnQuotient.AutoSize = true;
            this.rbtnQuotient.Location = new System.Drawing.Point(175, 53);
            this.rbtnQuotient.Name = "rbtnQuotient";
            this.rbtnQuotient.Size = new System.Drawing.Size(65, 17);
            this.rbtnQuotient.TabIndex = 0;
            this.rbtnQuotient.Text = "Quotient";
            this.rbtnQuotient.UseVisualStyleBackColor = true;
            // 
            // rbtnProduct
            // 
            this.rbtnProduct.AutoSize = true;
            this.rbtnProduct.Location = new System.Drawing.Point(175, 19);
            this.rbtnProduct.Name = "rbtnProduct";
            this.rbtnProduct.Size = new System.Drawing.Size(62, 17);
            this.rbtnProduct.TabIndex = 0;
            this.rbtnProduct.Text = "Product";
            this.rbtnProduct.UseVisualStyleBackColor = true;
            // 
            // lblResultaat
            // 
            this.lblResultaat.AutoSize = true;
            this.lblResultaat.Location = new System.Drawing.Point(12, 207);
            this.lblResultaat.Name = "lblResultaat";
            this.lblResultaat.Size = new System.Drawing.Size(58, 13);
            this.lblResultaat.TabIndex = 0;
            this.lblResultaat.Text = "Resultaat: ";
            // 
            // lblWatDenkJe
            // 
            this.lblWatDenkJe.AutoSize = true;
            this.lblWatDenkJe.Location = new System.Drawing.Point(12, 234);
            this.lblWatDenkJe.Name = "lblWatDenkJe";
            this.lblWatDenkJe.Size = new System.Drawing.Size(71, 13);
            this.lblWatDenkJe.TabIndex = 0;
            this.lblWatDenkJe.Text = "Wat denk je?";
            // 
            // txtResultaat
            // 
            this.txtResultaat.Enabled = false;
            this.txtResultaat.Location = new System.Drawing.Point(102, 204);
            this.txtResultaat.Name = "txtResultaat";
            this.txtResultaat.Size = new System.Drawing.Size(100, 20);
            this.txtResultaat.TabIndex = 1;
            // 
            // txtResultaatGebruiker
            // 
            this.txtResultaatGebruiker.Location = new System.Drawing.Point(102, 231);
            this.txtResultaatGebruiker.Name = "txtResultaatGebruiker";
            this.txtResultaatGebruiker.Size = new System.Drawing.Size(100, 20);
            this.txtResultaatGebruiker.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(-128, 466);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 1;
            // 
            // btnBereken
            // 
            this.btnBereken.Location = new System.Drawing.Point(12, 278);
            this.btnBereken.Name = "btnBereken";
            this.btnBereken.Size = new System.Drawing.Size(75, 47);
            this.btnBereken.TabIndex = 2;
            this.btnBereken.Text = "Bereken";
            this.btnBereken.UseVisualStyleBackColor = true;
            this.btnBereken.Click += new System.EventHandler(this.btnBereken_Click);
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(-13, 466);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 1;
            // 
            // btnHulp
            // 
            this.btnHulp.Location = new System.Drawing.Point(127, 278);
            this.btnHulp.Name = "btnHulp";
            this.btnHulp.Size = new System.Drawing.Size(75, 47);
            this.btnHulp.TabIndex = 2;
            this.btnHulp.Text = "Hulp";
            this.btnHulp.UseVisualStyleBackColor = true;
            this.btnHulp.Click += new System.EventHandler(this.btnHulp_Click);
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(102, 466);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 1;
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(242, 278);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(75, 47);
            this.btnSluiten.TabIndex = 2;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 349);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSluiten);
            this.Controls.Add(this.btnHulp);
            this.Controls.Add(this.btnBereken);
            this.Controls.Add(this.btnGenereer);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.txtResultaatGebruiker);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.txtGetal2);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.txtResultaat);
            this.Controls.Add(this.txtGetal1);
            this.Controls.Add(this.lblWatDenkJe);
            this.Controls.Add(this.lblResultaat);
            this.Controls.Add(this.lblGetal2);
            this.Controls.Add(this.lblGetal1);
            this.Name = "Form1";
            this.Text = "Rekenwonder";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGetal1;
        private System.Windows.Forms.Label lblGetal2;
        private System.Windows.Forms.TextBox txtGetal1;
        private System.Windows.Forms.TextBox txtGetal2;
        private System.Windows.Forms.Button btnGenereer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnProduct;
        private System.Windows.Forms.RadioButton rbtnQuotient;
        private System.Windows.Forms.RadioButton rbtnVerschil;
        private System.Windows.Forms.RadioButton rbtnSom;
        private System.Windows.Forms.Label lblResultaat;
        private System.Windows.Forms.Label lblWatDenkJe;
        private System.Windows.Forms.TextBox txtResultaat;
        private System.Windows.Forms.TextBox txtResultaatGebruiker;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btnBereken;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button btnHulp;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button btnSluiten;
    }
}

