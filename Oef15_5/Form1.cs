﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_5
{
    public partial class Form1 : Form
    {
        Bewerking b1;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBereken_Click(object sender, EventArgs e)
        {
            b1 = new Bewerking(Convert.ToInt32(txtGetal1.Text), Convert.ToInt32(txtGetal2.Text));

            if (rbtnSom.Checked)
                txtResultaat.Text = b1.Som().ToString();

            if (rbtnProduct.Checked)
                txtResultaat.Text = b1.Product().ToString();

            if (rbtnQuotient.Checked)
            {
                if (b1.Getal1 % b1.Getal2 == 0)
                    txtResultaat.Text = b1.Quotient().ToString();
                else
                    MessageBox.Show("Getal1 is niet geheel deelbaar door Getal2", "Opmerking", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            if (rbtnVerschil.Checked)
            {
                if (b1.Getal1 >= b1.Getal2)
                    txtResultaat.Text = b1.Verschil().ToString();
                else
                {
                    MessageBox.Show("Getal1 moet groter of gelijk zijn aan Getal2", "Opmerking", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }



            if (txtResultaat.Text == txtResultaatGebruiker.Text)
                MessageBox.Show("Proficiat! U had het juist.", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Fout antwoord!", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void btnGenereer_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            b1 = new Bewerking(random.Next(100), random.Next(100));            

            if (rbtnVerschil.Checked)
            {
                if (b1.Getal1 < b1.Getal2)
                    b1.Getal2 = new Random().Next(b1.Getal1);
            }

            if (rbtnQuotient.Checked)
            {
                if (b1.Getal1 == 0)
                    b1.Getal1 = new Random().Next(1, 101);

                while (b1.Getal1 % b1.Getal2 != 0)
                {
                    b1.Getal2 = new Random().Next(1, b1.Getal1 + 1);
                }

            

            }

            txtGetal1.Text = b1.Getal1.ToString();
            txtGetal2.Text = b1.Getal2.ToString();
        }

        private void btnHulp_Click(object sender, EventArgs e)
        {
            if (rbtnSom.Checked)
                txtResultaatGebruiker.Text = b1.Som().ToString();
            if (rbtnProduct.Checked)
                txtResultaatGebruiker.Text = b1.Product().ToString();
            if (rbtnQuotient.Checked)
                txtResultaatGebruiker.Text = b1.Quotient().ToString();
            if (rbtnVerschil.Checked)
                txtResultaatGebruiker.Text = b1.Verschil().ToString();
        }
    }
}
