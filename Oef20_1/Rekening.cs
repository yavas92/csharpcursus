﻿using Oef19_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef20_1
{
    class Rekening
    {
        // Attributen
        private string _rekeningnr;
        private double _saldo;
        private double _minimum = 0;


        // Properties
        public string Rekeningnr
        {
            get { return _rekeningnr; }
            set { _rekeningnr = value; }
        }


        public double Saldo
        {
            get { return _saldo; }
            set
            {
                if (value < _minimum)
                    throw new CustomException($"Saldo mag niet lager zijn dan {_minimum}");
                _saldo = value;
            }
        }


        public double Minimum
        {
            get { return _minimum; }
            set { _minimum = value; }
        }


        // Constructor
        public Rekening(string rekeningnr, double saldo)
        {
            Rekeningnr = rekeningnr;
            Saldo = saldo;
        }

        // Methods
        public void Afhalen(double bedrag)
        {
            try
            {
                Saldo -= bedrag;

            }
            catch (CustomException ce)
            {
                System.Windows.Forms.MessageBox.Show(ce.Message);
            }
        }

        public void Storten(double bedrag)
        {
            Saldo += bedrag;
        }

        public override string ToString()
        {
            return $"Rekeningnummer {Rekeningnr} met saldo {Saldo}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            Rekening r = (Rekening)obj;

            return (this.Rekeningnr == r.Rekeningnr);
        }
    }
}
