﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef20_1
{
    public partial class Form1 : Form
    {
        Rekening rekening = new Rekening("123-4530396-13", 1000);
        Spaarrekening spaarrekening = new Spaarrekening();
        Zichtrekening zichtrekening = new Zichtrekening("987-6543210-12", 50);

        public Form1()
        {
            InitializeComponent();

            spaarrekening.Rekeningnr = "735-1420224-12";
            spaarrekening.Saldo = 2000;
            spaarrekening.Percentage = 5;

            lblRekeningInfo.Text = rekening.ToString();
            lblSpaarrekeningInfo.Text = spaarrekening.ToString();
            lblZichtrekeningInfo.Text = zichtrekening.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnStortenRekening_Click(object sender, EventArgs e)
        {
            if (double.TryParse(txtRekening.Text, out double x))
            {
                rekening.Storten(Convert.ToDouble(txtRekening.Text));
                Refresh();
            }
            else
            {
                MessageBox.Show("Ongeldig bedrag");
            }
        }

        private void btnStortenSpaarrekening_Click(object sender, EventArgs e)
        {
            if (double.TryParse(txtSpaarrekening.Text, out double x))
            {
                spaarrekening.Storten(Convert.ToDouble(txtSpaarrekening.Text));
                Refresh();
            }
            else
            {
                MessageBox.Show("Ongeldig bedrag");
            }
        }

        private void btnStortenZichtrekening_Click(object sender, EventArgs e)
        {
            if (double.TryParse(txtZichtrekening.Text, out double x))
            {
                zichtrekening.Storten(Convert.ToDouble(txtZichtrekening.Text));
                Refresh();
            }
            else
            {
                MessageBox.Show("Ongeldig bedrag");
            }
        }

        private void btnAfhalenRekening_Click(object sender, EventArgs e)
        {
            if (double.TryParse(txtRekening.Text, out double x))
            {
                rekening.Afhalen(Convert.ToDouble(txtRekening.Text));
                Refresh();
            }
            else
            {
                MessageBox.Show("Ongeldig bedrag");
            }
        }

        private void btnAfhalenSpaarrekening_Click(object sender, EventArgs e)
        {
            if (double.TryParse(txtSpaarrekening.Text, out double x))
            {
                spaarrekening.Afhalen(Convert.ToDouble(txtSpaarrekening.Text));
                Refresh();
            }
            else
            {
                MessageBox.Show("Ongeldig bedrag");
            }
        }

        private void btnAfhalenZichtrekening_Click(object sender, EventArgs e)
        {
            if (double.TryParse(txtZichtrekening.Text, out double x))
            {
                zichtrekening.Afhalen(Convert.ToDouble(txtZichtrekening.Text));
                Refresh();
            }
            else
            {
                MessageBox.Show("Ongeldig bedrag");
            }
        }

        private void btnVoegRenteToe_Click(object sender, EventArgs e)
        {
            spaarrekening.Schrijfrentebij();
            Refresh();
        }

        public new void Refresh()
        {
            lblRekeningInfo.Text = rekening.ToString();
            lblSpaarrekeningInfo.Text = spaarrekening.ToString();
            lblZichtrekeningInfo.Text = zichtrekening.ToString();
        }

        private void btnEquals_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Rekeningnummer: 123-4530396-13" + Environment.NewLine + "Zichtrekeningnummer: 123-4530396-13" + Environment.NewLine + "Equals: " + rekening.Equals(new Zichtrekening("123-4530396-13", 0
                )));

            MessageBox.Show("Rekeningnummer: 123-4530396-13" + Environment.NewLine + "Rekeningnummer: 999-4530396-13" + Environment.NewLine + "Equals: " + rekening.Equals(new Rekening("999-4530396-13", 0
                )));
        }
    }
}
