﻿namespace Oef20_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRekening = new System.Windows.Forms.Label();
            this.txtRekening = new System.Windows.Forms.TextBox();
            this.btnStortenRekening = new System.Windows.Forms.Button();
            this.btnAfhalenRekening = new System.Windows.Forms.Button();
            this.lblRekeningInfo = new System.Windows.Forms.Label();
            this.btnAfhalenSpaarrekening = new System.Windows.Forms.Button();
            this.btnStortenSpaarrekening = new System.Windows.Forms.Button();
            this.txtSpaarrekening = new System.Windows.Forms.TextBox();
            this.lblSpaarrekeningInfo = new System.Windows.Forms.Label();
            this.lblSpaarrekening = new System.Windows.Forms.Label();
            this.lblZichtrekening = new System.Windows.Forms.Label();
            this.lblZichtrekeningInfo = new System.Windows.Forms.Label();
            this.txtZichtrekening = new System.Windows.Forms.TextBox();
            this.btnStortenZichtrekening = new System.Windows.Forms.Button();
            this.btnAfhalenZichtrekening = new System.Windows.Forms.Button();
            this.btnVoegRenteToe = new System.Windows.Forms.Button();
            this.btnEquals = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblRekening
            // 
            this.lblRekening.AutoSize = true;
            this.lblRekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRekening.Location = new System.Drawing.Point(15, 13);
            this.lblRekening.Name = "lblRekening";
            this.lblRekening.Size = new System.Drawing.Size(61, 13);
            this.lblRekening.TabIndex = 0;
            this.lblRekening.Text = "Rekening";
            // 
            // txtRekening
            // 
            this.txtRekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRekening.Location = new System.Drawing.Point(15, 39);
            this.txtRekening.Name = "txtRekening";
            this.txtRekening.Size = new System.Drawing.Size(325, 20);
            this.txtRekening.TabIndex = 1;
            // 
            // btnStortenRekening
            // 
            this.btnStortenRekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStortenRekening.Location = new System.Drawing.Point(374, 32);
            this.btnStortenRekening.Name = "btnStortenRekening";
            this.btnStortenRekening.Size = new System.Drawing.Size(52, 32);
            this.btnStortenRekening.TabIndex = 2;
            this.btnStortenRekening.Text = "+";
            this.btnStortenRekening.UseVisualStyleBackColor = true;
            this.btnStortenRekening.Click += new System.EventHandler(this.btnStortenRekening_Click);
            // 
            // btnAfhalenRekening
            // 
            this.btnAfhalenRekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAfhalenRekening.Location = new System.Drawing.Point(441, 32);
            this.btnAfhalenRekening.Name = "btnAfhalenRekening";
            this.btnAfhalenRekening.Size = new System.Drawing.Size(52, 32);
            this.btnAfhalenRekening.TabIndex = 2;
            this.btnAfhalenRekening.Text = "-";
            this.btnAfhalenRekening.UseVisualStyleBackColor = true;
            this.btnAfhalenRekening.Click += new System.EventHandler(this.btnAfhalenRekening_Click);
            // 
            // lblRekeningInfo
            // 
            this.lblRekeningInfo.AutoSize = true;
            this.lblRekeningInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRekeningInfo.Location = new System.Drawing.Point(15, 62);
            this.lblRekeningInfo.Name = "lblRekeningInfo";
            this.lblRekeningInfo.Size = new System.Drawing.Size(0, 13);
            this.lblRekeningInfo.TabIndex = 0;
            // 
            // btnAfhalenSpaarrekening
            // 
            this.btnAfhalenSpaarrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAfhalenSpaarrekening.Location = new System.Drawing.Point(440, 121);
            this.btnAfhalenSpaarrekening.Name = "btnAfhalenSpaarrekening";
            this.btnAfhalenSpaarrekening.Size = new System.Drawing.Size(52, 32);
            this.btnAfhalenSpaarrekening.TabIndex = 6;
            this.btnAfhalenSpaarrekening.Text = "-";
            this.btnAfhalenSpaarrekening.UseVisualStyleBackColor = true;
            this.btnAfhalenSpaarrekening.Click += new System.EventHandler(this.btnAfhalenSpaarrekening_Click);
            // 
            // btnStortenSpaarrekening
            // 
            this.btnStortenSpaarrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStortenSpaarrekening.Location = new System.Drawing.Point(373, 121);
            this.btnStortenSpaarrekening.Name = "btnStortenSpaarrekening";
            this.btnStortenSpaarrekening.Size = new System.Drawing.Size(52, 32);
            this.btnStortenSpaarrekening.TabIndex = 7;
            this.btnStortenSpaarrekening.Text = "+";
            this.btnStortenSpaarrekening.UseVisualStyleBackColor = true;
            this.btnStortenSpaarrekening.Click += new System.EventHandler(this.btnStortenSpaarrekening_Click);
            // 
            // txtSpaarrekening
            // 
            this.txtSpaarrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpaarrekening.Location = new System.Drawing.Point(15, 128);
            this.txtSpaarrekening.Name = "txtSpaarrekening";
            this.txtSpaarrekening.Size = new System.Drawing.Size(325, 20);
            this.txtSpaarrekening.TabIndex = 5;
            // 
            // lblSpaarrekeningInfo
            // 
            this.lblSpaarrekeningInfo.AutoSize = true;
            this.lblSpaarrekeningInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpaarrekeningInfo.Location = new System.Drawing.Point(15, 151);
            this.lblSpaarrekeningInfo.Name = "lblSpaarrekeningInfo";
            this.lblSpaarrekeningInfo.Size = new System.Drawing.Size(0, 13);
            this.lblSpaarrekeningInfo.TabIndex = 3;
            // 
            // lblSpaarrekening
            // 
            this.lblSpaarrekening.AutoSize = true;
            this.lblSpaarrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpaarrekening.Location = new System.Drawing.Point(14, 102);
            this.lblSpaarrekening.Name = "lblSpaarrekening";
            this.lblSpaarrekening.Size = new System.Drawing.Size(89, 13);
            this.lblSpaarrekening.TabIndex = 4;
            this.lblSpaarrekening.Text = "Spaarrekening";
            // 
            // lblZichtrekening
            // 
            this.lblZichtrekening.AutoSize = true;
            this.lblZichtrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZichtrekening.Location = new System.Drawing.Point(15, 190);
            this.lblZichtrekening.Name = "lblZichtrekening";
            this.lblZichtrekening.Size = new System.Drawing.Size(85, 13);
            this.lblZichtrekening.TabIndex = 4;
            this.lblZichtrekening.Text = "Zichtrekening";
            // 
            // lblZichtrekeningInfo
            // 
            this.lblZichtrekeningInfo.AutoSize = true;
            this.lblZichtrekeningInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZichtrekeningInfo.Location = new System.Drawing.Point(15, 239);
            this.lblZichtrekeningInfo.Name = "lblZichtrekeningInfo";
            this.lblZichtrekeningInfo.Size = new System.Drawing.Size(0, 13);
            this.lblZichtrekeningInfo.TabIndex = 3;
            // 
            // txtZichtrekening
            // 
            this.txtZichtrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZichtrekening.Location = new System.Drawing.Point(15, 216);
            this.txtZichtrekening.Name = "txtZichtrekening";
            this.txtZichtrekening.Size = new System.Drawing.Size(325, 20);
            this.txtZichtrekening.TabIndex = 5;
            // 
            // btnStortenZichtrekening
            // 
            this.btnStortenZichtrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStortenZichtrekening.Location = new System.Drawing.Point(374, 209);
            this.btnStortenZichtrekening.Name = "btnStortenZichtrekening";
            this.btnStortenZichtrekening.Size = new System.Drawing.Size(52, 32);
            this.btnStortenZichtrekening.TabIndex = 7;
            this.btnStortenZichtrekening.Text = "+";
            this.btnStortenZichtrekening.UseVisualStyleBackColor = true;
            this.btnStortenZichtrekening.Click += new System.EventHandler(this.btnStortenZichtrekening_Click);
            // 
            // btnAfhalenZichtrekening
            // 
            this.btnAfhalenZichtrekening.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAfhalenZichtrekening.Location = new System.Drawing.Point(441, 209);
            this.btnAfhalenZichtrekening.Name = "btnAfhalenZichtrekening";
            this.btnAfhalenZichtrekening.Size = new System.Drawing.Size(52, 32);
            this.btnAfhalenZichtrekening.TabIndex = 6;
            this.btnAfhalenZichtrekening.Text = "-";
            this.btnAfhalenZichtrekening.UseVisualStyleBackColor = true;
            this.btnAfhalenZichtrekening.Click += new System.EventHandler(this.btnAfhalenZichtrekening_Click);
            // 
            // btnVoegRenteToe
            // 
            this.btnVoegRenteToe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoegRenteToe.Location = new System.Drawing.Point(510, 121);
            this.btnVoegRenteToe.Name = "btnVoegRenteToe";
            this.btnVoegRenteToe.Size = new System.Drawing.Size(52, 32);
            this.btnVoegRenteToe.TabIndex = 6;
            this.btnVoegRenteToe.Text = "Rente";
            this.btnVoegRenteToe.UseVisualStyleBackColor = true;
            this.btnVoegRenteToe.Click += new System.EventHandler(this.btnVoegRenteToe_Click);
            // 
            // btnEquals
            // 
            this.btnEquals.Location = new System.Drawing.Point(570, 280);
            this.btnEquals.Name = "btnEquals";
            this.btnEquals.Size = new System.Drawing.Size(75, 23);
            this.btnEquals.TabIndex = 8;
            this.btnEquals.Text = "Equals?";
            this.btnEquals.UseVisualStyleBackColor = true;
            this.btnEquals.Click += new System.EventHandler(this.btnEquals_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 336);
            this.Controls.Add(this.btnEquals);
            this.Controls.Add(this.btnAfhalenZichtrekening);
            this.Controls.Add(this.btnVoegRenteToe);
            this.Controls.Add(this.btnAfhalenSpaarrekening);
            this.Controls.Add(this.btnStortenZichtrekening);
            this.Controls.Add(this.btnStortenSpaarrekening);
            this.Controls.Add(this.txtZichtrekening);
            this.Controls.Add(this.lblZichtrekeningInfo);
            this.Controls.Add(this.txtSpaarrekening);
            this.Controls.Add(this.lblZichtrekening);
            this.Controls.Add(this.lblSpaarrekeningInfo);
            this.Controls.Add(this.lblSpaarrekening);
            this.Controls.Add(this.btnAfhalenRekening);
            this.Controls.Add(this.btnStortenRekening);
            this.Controls.Add(this.txtRekening);
            this.Controls.Add(this.lblRekeningInfo);
            this.Controls.Add(this.lblRekening);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.Text = "Rekeningen";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRekening;
        private System.Windows.Forms.TextBox txtRekening;
        private System.Windows.Forms.Button btnStortenRekening;
        private System.Windows.Forms.Button btnAfhalenRekening;
        private System.Windows.Forms.Label lblRekeningInfo;
        private System.Windows.Forms.Button btnAfhalenSpaarrekening;
        private System.Windows.Forms.Button btnStortenSpaarrekening;
        private System.Windows.Forms.TextBox txtSpaarrekening;
        private System.Windows.Forms.Label lblSpaarrekeningInfo;
        private System.Windows.Forms.Label lblSpaarrekening;
        private System.Windows.Forms.Label lblZichtrekening;
        private System.Windows.Forms.Label lblZichtrekeningInfo;
        private System.Windows.Forms.TextBox txtZichtrekening;
        private System.Windows.Forms.Button btnStortenZichtrekening;
        private System.Windows.Forms.Button btnAfhalenZichtrekening;
        private System.Windows.Forms.Button btnVoegRenteToe;
        private System.Windows.Forms.Button btnEquals;
    }
}

