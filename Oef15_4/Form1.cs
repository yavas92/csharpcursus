﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_4
{
    public partial class Form1 : Form
    {
        Zwembad zwembad = new Zwembad();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBereken_Click(object sender, EventArgs e)
        {
            zwembad.Diepte = Convert.ToDouble(txtDiepte.Text);
            zwembad.Lengte = Convert.ToDouble(txtLengte.Text);
            zwembad.Breedte = Convert.ToDouble(txtBreedte.Text);
            zwembad.Randafstand = Convert.ToDouble(txtRandafstand.Text);

            txtOutput.Text = zwembad.LiterWater();
        }
    }
}
