﻿namespace Oef15_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDiepte = new System.Windows.Forms.Label();
            this.txtDiepte = new System.Windows.Forms.TextBox();
            this.txtLengte = new System.Windows.Forms.TextBox();
            this.txtBreedte = new System.Windows.Forms.TextBox();
            this.lblLengte = new System.Windows.Forms.Label();
            this.lblBreedte = new System.Windows.Forms.Label();
            this.lblRandafstand = new System.Windows.Forms.Label();
            this.txtRandafstand = new System.Windows.Forms.TextBox();
            this.btnBereken = new System.Windows.Forms.Button();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblDiepte
            // 
            this.lblDiepte.AutoSize = true;
            this.lblDiepte.Location = new System.Drawing.Point(45, 13);
            this.lblDiepte.Name = "lblDiepte";
            this.lblDiepte.Size = new System.Drawing.Size(66, 13);
            this.lblDiepte.TabIndex = 0;
            this.lblDiepte.Text = "Diepte (in m)";
            // 
            // txtDiepte
            // 
            this.txtDiepte.Location = new System.Drawing.Point(161, 10);
            this.txtDiepte.Name = "txtDiepte";
            this.txtDiepte.Size = new System.Drawing.Size(89, 20);
            this.txtDiepte.TabIndex = 1;
            // 
            // txtLengte
            // 
            this.txtLengte.Location = new System.Drawing.Point(161, 36);
            this.txtLengte.Name = "txtLengte";
            this.txtLengte.Size = new System.Drawing.Size(89, 20);
            this.txtLengte.TabIndex = 2;
            // 
            // txtBreedte
            // 
            this.txtBreedte.Location = new System.Drawing.Point(161, 62);
            this.txtBreedte.Name = "txtBreedte";
            this.txtBreedte.Size = new System.Drawing.Size(89, 20);
            this.txtBreedte.TabIndex = 3;
            // 
            // lblLengte
            // 
            this.lblLengte.AutoSize = true;
            this.lblLengte.Location = new System.Drawing.Point(45, 39);
            this.lblLengte.Name = "lblLengte";
            this.lblLengte.Size = new System.Drawing.Size(68, 13);
            this.lblLengte.TabIndex = 0;
            this.lblLengte.Text = "Lengte (in m)";
            // 
            // lblBreedte
            // 
            this.lblBreedte.AutoSize = true;
            this.lblBreedte.Location = new System.Drawing.Point(45, 65);
            this.lblBreedte.Name = "lblBreedte";
            this.lblBreedte.Size = new System.Drawing.Size(72, 13);
            this.lblBreedte.TabIndex = 0;
            this.lblBreedte.Text = "Breedte (in m)";
            // 
            // lblRandafstand
            // 
            this.lblRandafstand.AutoSize = true;
            this.lblRandafstand.Location = new System.Drawing.Point(45, 106);
            this.lblRandafstand.Name = "lblRandafstand";
            this.lblRandafstand.Size = new System.Drawing.Size(96, 13);
            this.lblRandafstand.TabIndex = 0;
            this.lblRandafstand.Text = "Randafstand (in m)";
            // 
            // txtRandafstand
            // 
            this.txtRandafstand.Location = new System.Drawing.Point(161, 103);
            this.txtRandafstand.Name = "txtRandafstand";
            this.txtRandafstand.Size = new System.Drawing.Size(89, 20);
            this.txtRandafstand.TabIndex = 4;
            // 
            // btnBereken
            // 
            this.btnBereken.Location = new System.Drawing.Point(48, 156);
            this.btnBereken.Name = "btnBereken";
            this.btnBereken.Size = new System.Drawing.Size(202, 21);
            this.btnBereken.TabIndex = 5;
            this.btnBereken.Text = "Bereken";
            this.btnBereken.UseVisualStyleBackColor = true;
            this.btnBereken.Click += new System.EventHandler(this.btnBereken_Click);
            // 
            // txtOutput
            // 
            this.txtOutput.Enabled = false;
            this.txtOutput.Location = new System.Drawing.Point(48, 200);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(202, 190);
            this.txtOutput.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 403);
            this.Controls.Add(this.btnBereken);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.txtRandafstand);
            this.Controls.Add(this.txtBreedte);
            this.Controls.Add(this.txtLengte);
            this.Controls.Add(this.lblRandafstand);
            this.Controls.Add(this.txtDiepte);
            this.Controls.Add(this.lblBreedte);
            this.Controls.Add(this.lblLengte);
            this.Controls.Add(this.lblDiepte);
            this.Name = "Form1";
            this.Text = "Zwembad";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDiepte;
        private System.Windows.Forms.TextBox txtDiepte;
        private System.Windows.Forms.TextBox txtLengte;
        private System.Windows.Forms.TextBox txtBreedte;
        private System.Windows.Forms.Label lblLengte;
        private System.Windows.Forms.Label lblBreedte;
        private System.Windows.Forms.Label lblRandafstand;
        private System.Windows.Forms.TextBox txtRandafstand;
        private System.Windows.Forms.Button btnBereken;
        private System.Windows.Forms.TextBox txtOutput;
    }
}

