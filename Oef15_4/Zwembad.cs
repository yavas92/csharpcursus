﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef15_4
{
    class Zwembad
    {
        // Attributen
        private double _diepte;
        private double _lengte;
        private double _breedte;
        private double _randafstand;

        // Properties
        public double Diepte
        {
            get { return _diepte; }
            set { _diepte = value; }
        }

        public double Lengte
        {
            get { return _lengte; }
            set { _lengte = value; }
        }

        public double Breedte
        {
            get { return _breedte; }
            set { _breedte = value; }
        }

        public double Randafstand
        {
            get { return _randafstand; }
            set { _randafstand = value; }
        }

        // Constructor
        public Zwembad()
        {

        }

        // Methods
        public string LiterWater()
        {
            return $"diepte={Diepte}" + Environment.NewLine +
                $"breedte={Breedte}" + Environment.NewLine +
                $"lengte={Lengte}" + Environment.NewLine +
                $"randafstand={Randafstand}" + Environment.NewLine + Environment.NewLine + Environment.NewLine +
                $"liters water: {Lengte * Breedte * (Diepte - Randafstand) * 100}";
        }
    }
}
