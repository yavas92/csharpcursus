﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef18_2
{
    class Mens : Wezen
    {
        // Constructor
        public Mens(string naam) : base(naam)
        {

        }

        // Methodes
        public override string Eten()
        {
            return "Lekker!";
        }

        public override string Praten(string zin)
        {
            if (zin == "Hallo")
                return "Heey";
            else if (zin == "Goede morgen")
                return "Goeiemorgen";
            else if (zin == "Hoe gaat het?")
                return "Goed, met jou?";
            return "Warm weer vandaag";
        }

        public override string Strelen()
        {
            return "Blijf van mijn lijf. Arrh.";
        }
    }
}
