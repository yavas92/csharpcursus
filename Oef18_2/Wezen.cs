﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef18_2
{
    abstract class Wezen
    {
        // Attributen
        private string _naam;


        // Properties
        public string Naam
        {
            get { return _naam; }
        }


        // Constructor
        protected Wezen(string naam)
        {
            _naam = naam;
        }

        // Methodes
        public virtual string Eten()
        {
            return "";
        }

        public virtual string Praten(string zin)
        {
            return "";
        }

        public virtual string Strelen()
        {
            return "";
        }
    }
}
