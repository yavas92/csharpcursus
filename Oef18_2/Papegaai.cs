﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef18_2
{
    class Papegaai:Wezen
    {
        public Papegaai(string naam):base(naam)
        {

        }

        public override string Praten(string zin)
        {
            Random random = new Random();
            if (random.Next(1, 6) == 1)
                return "Koko kopke krabben";
            return zin;
        }

        public override string Strelen()
        {
            return "Koko.";
        }
    }
}
