﻿namespace Oef18_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbMens = new System.Windows.Forms.RadioButton();
            this.rdbPapegaai = new System.Windows.Forms.RadioButton();
            this.rdbKat = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.btnAanmaken = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbZinnen = new System.Windows.Forms.ListBox();
            this.btnPraten = new System.Windows.Forms.Button();
            this.btnStrelen = new System.Windows.Forms.Button();
            this.btnEten = new System.Windows.Forms.Button();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbMens);
            this.groupBox1.Controls.Add(this.rdbPapegaai);
            this.groupBox1.Controls.Add(this.rdbKat);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(93, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dieren";
            // 
            // rdbMens
            // 
            this.rdbMens.AutoSize = true;
            this.rdbMens.Location = new System.Drawing.Point(7, 66);
            this.rdbMens.Name = "rdbMens";
            this.rdbMens.Size = new System.Drawing.Size(51, 17);
            this.rdbMens.TabIndex = 0;
            this.rdbMens.TabStop = true;
            this.rdbMens.Text = "Mens";
            this.rdbMens.UseVisualStyleBackColor = true;
            // 
            // rdbPapegaai
            // 
            this.rdbPapegaai.AutoSize = true;
            this.rdbPapegaai.Location = new System.Drawing.Point(7, 43);
            this.rdbPapegaai.Name = "rdbPapegaai";
            this.rdbPapegaai.Size = new System.Drawing.Size(70, 17);
            this.rdbPapegaai.TabIndex = 0;
            this.rdbPapegaai.TabStop = true;
            this.rdbPapegaai.Text = "Papegaai";
            this.rdbPapegaai.UseVisualStyleBackColor = true;
            // 
            // rdbKat
            // 
            this.rdbKat.AutoSize = true;
            this.rdbKat.Checked = true;
            this.rdbKat.Location = new System.Drawing.Point(7, 20);
            this.rdbKat.Name = "rdbKat";
            this.rdbKat.Size = new System.Drawing.Size(41, 17);
            this.rdbKat.TabIndex = 0;
            this.rdbKat.TabStop = true;
            this.rdbKat.Text = "Kat";
            this.rdbKat.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Geef je \"wezen\" een naam.";
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(150, 32);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(135, 20);
            this.txtNaam.TabIndex = 2;
            // 
            // btnAanmaken
            // 
            this.btnAanmaken.Location = new System.Drawing.Point(343, 29);
            this.btnAanmaken.Name = "btnAanmaken";
            this.btnAanmaken.Size = new System.Drawing.Size(75, 23);
            this.btnAanmaken.TabIndex = 3;
            this.btnAanmaken.Text = "Aanmaken";
            this.btnAanmaken.UseVisualStyleBackColor = true;
            this.btnAanmaken.Click += new System.EventHandler(this.btnAanmaken_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Kies een zin uit om te praten:";
            // 
            // lbZinnen
            // 
            this.lbZinnen.FormattingEnabled = true;
            this.lbZinnen.Location = new System.Drawing.Point(12, 146);
            this.lbZinnen.Name = "lbZinnen";
            this.lbZinnen.Size = new System.Drawing.Size(143, 56);
            this.lbZinnen.TabIndex = 5;
            // 
            // btnPraten
            // 
            this.btnPraten.Location = new System.Drawing.Point(228, 157);
            this.btnPraten.Name = "btnPraten";
            this.btnPraten.Size = new System.Drawing.Size(103, 30);
            this.btnPraten.TabIndex = 6;
            this.btnPraten.Text = "Praten";
            this.btnPraten.UseVisualStyleBackColor = true;
            this.btnPraten.Click += new System.EventHandler(this.btnPraten_Click);
            // 
            // btnStrelen
            // 
            this.btnStrelen.Location = new System.Drawing.Point(12, 227);
            this.btnStrelen.Name = "btnStrelen";
            this.btnStrelen.Size = new System.Drawing.Size(107, 45);
            this.btnStrelen.TabIndex = 7;
            this.btnStrelen.Text = "Strelen";
            this.btnStrelen.UseVisualStyleBackColor = true;
            this.btnStrelen.Click += new System.EventHandler(this.btnStrelen_Click);
            // 
            // btnEten
            // 
            this.btnEten.Location = new System.Drawing.Point(163, 227);
            this.btnEten.Name = "btnEten";
            this.btnEten.Size = new System.Drawing.Size(107, 45);
            this.btnEten.TabIndex = 8;
            this.btnEten.Text = "Eten";
            this.btnEten.UseVisualStyleBackColor = true;
            this.btnEten.Click += new System.EventHandler(this.btnEten_Click);
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(311, 227);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(107, 45);
            this.btnSluiten.TabIndex = 9;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 301);
            this.Controls.Add(this.btnSluiten);
            this.Controls.Add(this.btnEten);
            this.Controls.Add(this.btnStrelen);
            this.Controls.Add(this.btnPraten);
            this.Controls.Add(this.lbZinnen);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAanmaken);
            this.Controls.Add(this.txtNaam);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Dieren";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbMens;
        private System.Windows.Forms.RadioButton rdbPapegaai;
        private System.Windows.Forms.RadioButton rdbKat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.Button btnAanmaken;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lbZinnen;
        private System.Windows.Forms.Button btnPraten;
        private System.Windows.Forms.Button btnStrelen;
        private System.Windows.Forms.Button btnEten;
        private System.Windows.Forms.Button btnSluiten;
    }
}

