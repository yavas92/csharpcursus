﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_7
{
    public partial class Form1 : Form
    {
        Vierkant vierkant = new Vierkant();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTeken_Click(object sender, EventArgs e)
        {
            vierkant.Zijde = Convert.ToInt32(txtZijde.Text);
            lblOutput.Text = vierkant.Teken();
        }


        private void txtZijde_Leave(object sender, EventArgs e)
        {
            if (!int.TryParse(txtZijde.Text, out int x))
                MessageBox.Show("Dit is geen getal!");
        }

        private void btnOmtrek_Click(object sender, EventArgs e)
        {
            MessageBox.Show("De omtrek is " + vierkant.Omtrek(), "Resultaat", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void btnOppervlakte_Click(object sender, EventArgs e)
        {
            MessageBox.Show("De oppervlakte is " + vierkant.Oppervlakte(), "Resultaat", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnDiagonaal_Click(object sender, EventArgs e)
        {
            MessageBox.Show("De diagonaal is " + vierkant.Diagonaal(), "Resultaat", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
