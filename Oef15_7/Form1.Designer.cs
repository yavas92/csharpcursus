﻿namespace Oef15_7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTeken = new System.Windows.Forms.Button();
            this.btnOmtrek = new System.Windows.Forms.Button();
            this.btnOppervlakte = new System.Windows.Forms.Button();
            this.btnDiagonaal = new System.Windows.Forms.Button();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.lblZijde = new System.Windows.Forms.Label();
            this.txtZijde = new System.Windows.Forms.TextBox();
            this.lblOutput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnTeken
            // 
            this.btnTeken.Location = new System.Drawing.Point(507, 43);
            this.btnTeken.Name = "btnTeken";
            this.btnTeken.Size = new System.Drawing.Size(75, 23);
            this.btnTeken.TabIndex = 0;
            this.btnTeken.Text = "Teken";
            this.btnTeken.UseVisualStyleBackColor = true;
            this.btnTeken.Click += new System.EventHandler(this.btnTeken_Click);
            // 
            // btnOmtrek
            // 
            this.btnOmtrek.Location = new System.Drawing.Point(507, 72);
            this.btnOmtrek.Name = "btnOmtrek";
            this.btnOmtrek.Size = new System.Drawing.Size(75, 23);
            this.btnOmtrek.TabIndex = 0;
            this.btnOmtrek.Text = "Omtrek";
            this.btnOmtrek.UseVisualStyleBackColor = true;
            this.btnOmtrek.Click += new System.EventHandler(this.btnOmtrek_Click);
            // 
            // btnOppervlakte
            // 
            this.btnOppervlakte.Location = new System.Drawing.Point(507, 101);
            this.btnOppervlakte.Name = "btnOppervlakte";
            this.btnOppervlakte.Size = new System.Drawing.Size(75, 23);
            this.btnOppervlakte.TabIndex = 0;
            this.btnOppervlakte.Text = "Oppervlakte";
            this.btnOppervlakte.UseVisualStyleBackColor = true;
            this.btnOppervlakte.Click += new System.EventHandler(this.btnOppervlakte_Click);
            // 
            // btnDiagonaal
            // 
            this.btnDiagonaal.Location = new System.Drawing.Point(507, 130);
            this.btnDiagonaal.Name = "btnDiagonaal";
            this.btnDiagonaal.Size = new System.Drawing.Size(75, 23);
            this.btnDiagonaal.TabIndex = 0;
            this.btnDiagonaal.Text = "Diagonaal";
            this.btnDiagonaal.UseVisualStyleBackColor = true;
            this.btnDiagonaal.Click += new System.EventHandler(this.btnDiagonaal_Click);
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(507, 159);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(75, 23);
            this.btnSluiten.TabIndex = 0;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // lblZijde
            // 
            this.lblZijde.AutoSize = true;
            this.lblZijde.Location = new System.Drawing.Point(13, 13);
            this.lblZijde.Name = "lblZijde";
            this.lblZijde.Size = new System.Drawing.Size(36, 13);
            this.lblZijde.TabIndex = 1;
            this.lblZijde.Text = "Zijde: ";
            // 
            // txtZijde
            // 
            this.txtZijde.Location = new System.Drawing.Point(117, 10);
            this.txtZijde.Name = "txtZijde";
            this.txtZijde.Size = new System.Drawing.Size(100, 20);
            this.txtZijde.TabIndex = 2;
            this.txtZijde.Leave += new System.EventHandler(this.txtZijde_Leave);
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.Location = new System.Drawing.Point(13, 61);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(0, 14);
            this.lblOutput.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 396);
            this.Controls.Add(this.txtZijde);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.lblZijde);
            this.Controls.Add(this.btnSluiten);
            this.Controls.Add(this.btnDiagonaal);
            this.Controls.Add(this.btnOppervlakte);
            this.Controls.Add(this.btnOmtrek);
            this.Controls.Add(this.btnTeken);
            this.Name = "Form1";
            this.Text = "Vierkant";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTeken;
        private System.Windows.Forms.Button btnOmtrek;
        private System.Windows.Forms.Button btnOppervlakte;
        private System.Windows.Forms.Button btnDiagonaal;
        private System.Windows.Forms.Button btnSluiten;
        private System.Windows.Forms.Label lblZijde;
        private System.Windows.Forms.TextBox txtZijde;
        private System.Windows.Forms.Label lblOutput;
    }
}

