﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_7
{
    class Vierkant
    {
        // Variabelen
        private int _zijde;

        // Properties

        public int Zijde
        {
            get { return _zijde; }
            set
            {
                if (0 <= value && value <= 25)
                    _zijde = value;
                else
                    MessageBox.Show("De zijde moet tussen 0 en 25 liggen.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Constructors
        public Vierkant()
        {

        }
        public Vierkant(int lengte)
        {
            Zijde = lengte;
        }

        // Methods
        public double Diagonaal()
        {
            return  Zijde*Math.Sqrt(2);
        }

        public int Omtrek()
        {
            return Zijde*4;
        }

        public int Oppervlakte()
        {
            return Zijde*Zijde;
        }

        public string Teken()
        {
            string output = "";

            for (int i = 0; i < Zijde; i++)
            {
                for (int j = 0; j < Zijde; j++)
                {
                    output += "*".PadRight(2);
                }
                output += Environment.NewLine;
            }
            return output;
        }

    }
}
