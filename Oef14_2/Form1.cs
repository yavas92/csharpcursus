﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef14_2
{
    public partial class Form1 : Form
    {
        int[,] arrGetallen = new int[5, 3];
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGenereren_Click(object sender, EventArgs e)
        {
            GenereerArray();
        }

        void GenereerArray()
        {
            Random random = new Random();
            for (int rij = 0; rij <= arrGetallen.GetUpperBound(0); rij++)
            {
                for (int kol = 0; kol < arrGetallen.GetUpperBound(1); kol++)
                {
                    arrGetallen[rij, kol] = random.Next(101);
                }
                arrGetallen[rij, 2] = arrGetallen[rij, 0] + arrGetallen[rij, 1];
            }

            StringBuilder output = new StringBuilder();
            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= arrGetallen.GetUpperBound(1); j++)
                {
                    output.Append(arrGetallen[i, j].ToString().PadRight(10));
                }
                output.Append(Environment.NewLine);
            }
            txtOutput.Text = output.ToString();
        }
    }
}
