﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef14_5
{
    public partial class Form1 : Form
    {
        int[,] arrGegevens = new int[5, 4];

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnVerwerken_Click(object sender, EventArgs e)
        {
            ArrayBewerken();

            StringBuilder output = new StringBuilder();

            output.Append("".PadRight(10) + "E".PadRight(5) + "F".PadRight(5) + "OV".PadRight(5) + "A".PadRight(5) + Environment.NewLine);



            output.Append("<1960".PadRight(10));
            output.Append(arrGegevens[0, 0].ToString().PadRight(5));
            output.Append(arrGegevens[0, 1].ToString().PadRight(5));
            output.Append(arrGegevens[0, 2].ToString().PadRight(5));
            output.Append(arrGegevens[0, 3].ToString().PadRight(5));
            output.Append(Environment.NewLine);

            output.Append("<1970".PadRight(10));
            output.Append(arrGegevens[1, 0].ToString().PadRight(5));
            output.Append(arrGegevens[1, 1].ToString().PadRight(5));
            output.Append(arrGegevens[1, 2].ToString().PadRight(5));
            output.Append(arrGegevens[1, 3].ToString().PadRight(5));
            output.Append(Environment.NewLine);

            output.Append("<1980".PadRight(10));
            output.Append(arrGegevens[2, 0].ToString().PadRight(5));
            output.Append(arrGegevens[2, 1].ToString().PadRight(5));
            output.Append(arrGegevens[2, 2].ToString().PadRight(5));
            output.Append(arrGegevens[2, 3].ToString().PadRight(5));
            output.Append(Environment.NewLine);

            output.Append("<1990".PadRight(10));
            output.Append(arrGegevens[3, 0].ToString().PadRight(5));
            output.Append(arrGegevens[3, 1].ToString().PadRight(5));
            output.Append(arrGegevens[3, 2].ToString().PadRight(5));
            output.Append(arrGegevens[3, 3].ToString().PadRight(5));
            output.Append(Environment.NewLine);

            output.Append("-2000".PadRight(10));
            output.Append(arrGegevens[4, 0].ToString().PadRight(5));
            output.Append(arrGegevens[4, 1].ToString().PadRight(5));
            output.Append(arrGegevens[4, 2].ToString().PadRight(5));
            output.Append(arrGegevens[4, 3].ToString().PadRight(5));
            output.Append(Environment.NewLine);

            txtOutput.Text = output.ToString();

        }

        void ArrayBewerken()
        {
            using (StreamReader reader = new StreamReader(@"C:\BestandenCSharp\Les 14 - Enquete.txt"))
            {
                string regel, geslacht, verplaatsingsmiddel;
                int geboortejaar, rij=0, kol=0;

                while (!reader.EndOfStream)
                {
                    regel = reader.ReadLine();
                    regel = regel.Replace('\"', ' ');

                    geslacht = regel.Substring(0, regel.IndexOf(',')).Trim();

                    regel = regel.Substring(regel.IndexOf(',') + 1);

                    geboortejaar = Convert.ToInt32(regel.Substring(0, regel.IndexOf(',')).Trim());

                    regel = regel.Substring(regel.IndexOf(',') + 1);

                    verplaatsingsmiddel = regel.Substring(0).Trim();

                    if (geboortejaar < 1960)
                        rij = 0;
                    else if (geboortejaar < 1970)
                        rij = 1;
                    else if (geboortejaar < 1980)
                        rij = 2;
                    else if (geboortejaar < 1990)
                        rij = 3;
                    else if (geboortejaar <= 2000)
                        rij = 4;


                    switch (verplaatsingsmiddel)
                    {
                        case "E":
                            kol = 0;
                            break;
                        case "F":
                            kol = 1;
                            break;
                        case "OV":
                            kol = 2;
                            break;
                        case "A":
                            kol = 3;
                            break;
                        default:
                            break;
                    }

                    arrGegevens[rij, kol] += 1;
                }
            }
        }
    }
}
