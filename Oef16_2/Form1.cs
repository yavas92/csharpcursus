﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef16_2
{
    public partial class Form1 : Form
    {
        ArrayList dieren = new ArrayList();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnInlezen_Click(object sender, EventArgs e)
        {
            string bestand = @"C:\Users\Abdullah\Documents\Visual Studio 2019\Projects\CevoraCursus\Oef16_2\Dieren.txt";

            if (File.Exists(bestand))
            {
                using (StreamReader reader = new StreamReader(bestand))
                {
                    while (!reader.EndOfStream)
                    {
                        dieren.Add(reader.ReadLine());
                    }
                }
            }
            else
                MessageBox.Show("Bestand niet gevonden!");

            foreach (var dier in dieren)
            {
                lbOutput.Items.Add(dier);
            }
        }

        private void btnToevoegen_Click(object sender, EventArgs e)
        {
            if (txtDier.Text != "")
            {
                dieren.Add(txtDier.Text);
                lbOutput.Items.Add(txtDier.Text);
            }
            else
                MessageBox.Show("Tekstvakje is leeg!");
        }

        private void btnWissen_Click(object sender, EventArgs e)
        {
            lbOutput.Items.Clear();
        }

        private void btnSorteren_Click(object sender, EventArgs e)
        {
            dieren.Sort();

            //foreach (var dier in dieren)
            //{
            //    lbOutput.Items.Add(dier);
            //}

            lbOutput.DataSource = dieren;
        }
    }
}
