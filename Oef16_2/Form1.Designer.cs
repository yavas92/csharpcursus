﻿namespace Oef16_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbOutput = new System.Windows.Forms.ListBox();
            this.btnInlezen = new System.Windows.Forms.Button();
            this.btnToevoegen = new System.Windows.Forms.Button();
            this.btnWissen = new System.Windows.Forms.Button();
            this.btnSorteren = new System.Windows.Forms.Button();
            this.lblDier = new System.Windows.Forms.Label();
            this.txtDier = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbOutput
            // 
            this.lbOutput.FormattingEnabled = true;
            this.lbOutput.Location = new System.Drawing.Point(13, 13);
            this.lbOutput.Name = "lbOutput";
            this.lbOutput.Size = new System.Drawing.Size(267, 420);
            this.lbOutput.TabIndex = 0;
            // 
            // btnInlezen
            // 
            this.btnInlezen.Location = new System.Drawing.Point(310, 25);
            this.btnInlezen.Name = "btnInlezen";
            this.btnInlezen.Size = new System.Drawing.Size(141, 61);
            this.btnInlezen.TabIndex = 1;
            this.btnInlezen.Text = "Inlezen + Overzicht";
            this.btnInlezen.UseVisualStyleBackColor = true;
            this.btnInlezen.Click += new System.EventHandler(this.btnInlezen_Click);
            // 
            // btnToevoegen
            // 
            this.btnToevoegen.Location = new System.Drawing.Point(320, 219);
            this.btnToevoegen.Name = "btnToevoegen";
            this.btnToevoegen.Size = new System.Drawing.Size(117, 49);
            this.btnToevoegen.TabIndex = 1;
            this.btnToevoegen.Text = "Toevoegen";
            this.btnToevoegen.UseVisualStyleBackColor = true;
            this.btnToevoegen.Click += new System.EventHandler(this.btnToevoegen_Click);
            // 
            // btnWissen
            // 
            this.btnWissen.Location = new System.Drawing.Point(320, 286);
            this.btnWissen.Name = "btnWissen";
            this.btnWissen.Size = new System.Drawing.Size(117, 49);
            this.btnWissen.TabIndex = 1;
            this.btnWissen.Text = "Wissen";
            this.btnWissen.UseVisualStyleBackColor = true;
            this.btnWissen.Click += new System.EventHandler(this.btnWissen_Click);
            // 
            // btnSorteren
            // 
            this.btnSorteren.Location = new System.Drawing.Point(320, 353);
            this.btnSorteren.Name = "btnSorteren";
            this.btnSorteren.Size = new System.Drawing.Size(117, 49);
            this.btnSorteren.TabIndex = 1;
            this.btnSorteren.Text = "Sorteren";
            this.btnSorteren.UseVisualStyleBackColor = true;
            this.btnSorteren.Click += new System.EventHandler(this.btnSorteren_Click);
            // 
            // lblDier
            // 
            this.lblDier.AutoSize = true;
            this.lblDier.Location = new System.Drawing.Point(320, 117);
            this.lblDier.Name = "lblDier";
            this.lblDier.Size = new System.Drawing.Size(29, 13);
            this.lblDier.TabIndex = 2;
            this.lblDier.Text = "Dier:";
            // 
            // txtDier
            // 
            this.txtDier.Location = new System.Drawing.Point(320, 150);
            this.txtDier.Name = "txtDier";
            this.txtDier.Size = new System.Drawing.Size(117, 20);
            this.txtDier.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 450);
            this.Controls.Add(this.txtDier);
            this.Controls.Add(this.lblDier);
            this.Controls.Add(this.btnSorteren);
            this.Controls.Add(this.btnWissen);
            this.Controls.Add(this.btnToevoegen);
            this.Controls.Add(this.btnInlezen);
            this.Controls.Add(this.lbOutput);
            this.Name = "Form1";
            this.Text = "Les 16 - ArrayList";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbOutput;
        private System.Windows.Forms.Button btnInlezen;
        private System.Windows.Forms.Button btnToevoegen;
        private System.Windows.Forms.Button btnWissen;
        private System.Windows.Forms.Button btnSorteren;
        private System.Windows.Forms.Label lblDier;
        private System.Windows.Forms.TextBox txtDier;
    }
}

