﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Oef15_8
{
    class Trein
    {
        // Attributen
        private int _passagiers;
        private int _snelheid;
        private bool _deurOpen;

        // Properties
        public int Passagiers
        {
            get { return _passagiers; }
            set
            {
                if (value >= 0)
                    _passagiers = value;
            }
        }

        public int Snelheid
        {
            get { return _snelheid; }
            set
            {
                if (value >= 0 && value <= 120)
                    _snelheid = value;
            }
        }


        public bool DeurOpen
        {
            get { return _deurOpen; }
            set { _deurOpen = value; }
        }

        // Constructor
        public Trein()
        {
            Passagiers = 0;
            DeurOpen = true;
        }

        // Methods

        // Public
        public bool Afstappen(int aantalAfstappen)
        {
            if (DeurOpen)
            {
                Passagiers -= aantalAfstappen;
                return DeurOpen;
            }

            return false;
        }

        public bool Opstappen(int aantalOpstappen)
        {
            if (DeurOpen)
            {
                Passagiers += aantalOpstappen;
                return DeurOpen;
            }

            return false;
        }

        public void SluitDeur()
        {
            DeurOpen = false;
        }

        public string StandVanZaken()
        {
            return $"Snelheid: {Snelheid + Environment.NewLine}" +
                $"Passagiers: {Passagiers + Environment.NewLine}" +
                $"Deuren: {(DeurOpen ? "Open" : "Gesloten") + Environment.NewLine}";
        }

        public void Stoppen()
        {
            Snelheid = 0;
            DeurOpen = true;
        }

        public bool Versnellen(int versnelling)
        {
            if (!DeurOpen)
            {
                WijzigSnelheid(versnelling);
                return true;
            }

            return false;
        }

        public void Remmen(int vertraging)
        {
            WijzigSnelheid(-vertraging);
        }

        // Private
        private void WijzigSnelheid(int wijzigingSnelheid)
        {
            Snelheid += wijzigingSnelheid;
        }
    }
}
