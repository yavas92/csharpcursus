﻿namespace Oef15_8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOpstappen = new System.Windows.Forms.Label();
            this.lblAfstappen = new System.Windows.Forms.Label();
            this.lblVersnellen = new System.Windows.Forms.Label();
            this.lblVertragen = new System.Windows.Forms.Label();
            this.nudOpstappen = new System.Windows.Forms.NumericUpDown();
            this.nudAfstappen = new System.Windows.Forms.NumericUpDown();
            this.nudVersnellen = new System.Windows.Forms.NumericUpDown();
            this.nudVertragen = new System.Windows.Forms.NumericUpDown();
            this.btnOpstappen = new System.Windows.Forms.Button();
            this.btnAfstappen = new System.Windows.Forms.Button();
            this.btnVersnellen = new System.Windows.Forms.Button();
            this.btnVertragen = new System.Windows.Forms.Button();
            this.btnStoppen = new System.Windows.Forms.Button();
            this.btnDeurenSluiten = new System.Windows.Forms.Button();
            this.btnStandVanZaken = new System.Windows.Forms.Button();
            this.txtStandVanZaken = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudOpstappen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAfstappen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVersnellen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVertragen)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOpstappen
            // 
            this.lblOpstappen.AutoSize = true;
            this.lblOpstappen.Location = new System.Drawing.Point(67, 23);
            this.lblOpstappen.Name = "lblOpstappen";
            this.lblOpstappen.Size = new System.Drawing.Size(59, 13);
            this.lblOpstappen.TabIndex = 0;
            this.lblOpstappen.Text = "Opstappen";
            // 
            // lblAfstappen
            // 
            this.lblAfstappen.AutoSize = true;
            this.lblAfstappen.Location = new System.Drawing.Point(67, 73);
            this.lblAfstappen.Name = "lblAfstappen";
            this.lblAfstappen.Size = new System.Drawing.Size(55, 13);
            this.lblAfstappen.TabIndex = 0;
            this.lblAfstappen.Text = "Afstappen";
            // 
            // lblVersnellen
            // 
            this.lblVersnellen.AutoSize = true;
            this.lblVersnellen.Location = new System.Drawing.Point(67, 117);
            this.lblVersnellen.Name = "lblVersnellen";
            this.lblVersnellen.Size = new System.Drawing.Size(56, 13);
            this.lblVersnellen.TabIndex = 0;
            this.lblVersnellen.Text = "Versnellen";
            // 
            // lblVertragen
            // 
            this.lblVertragen.AutoSize = true;
            this.lblVertragen.Location = new System.Drawing.Point(67, 169);
            this.lblVertragen.Name = "lblVertragen";
            this.lblVertragen.Size = new System.Drawing.Size(53, 13);
            this.lblVertragen.TabIndex = 0;
            this.lblVertragen.Text = "Vertragen";
            // 
            // nudOpstappen
            // 
            this.nudOpstappen.Location = new System.Drawing.Point(146, 21);
            this.nudOpstappen.Name = "nudOpstappen";
            this.nudOpstappen.Size = new System.Drawing.Size(120, 20);
            this.nudOpstappen.TabIndex = 2;
            // 
            // nudAfstappen
            // 
            this.nudAfstappen.Location = new System.Drawing.Point(146, 71);
            this.nudAfstappen.Name = "nudAfstappen";
            this.nudAfstappen.Size = new System.Drawing.Size(120, 20);
            this.nudAfstappen.TabIndex = 2;
            // 
            // nudVersnellen
            // 
            this.nudVersnellen.Location = new System.Drawing.Point(146, 115);
            this.nudVersnellen.Name = "nudVersnellen";
            this.nudVersnellen.Size = new System.Drawing.Size(120, 20);
            this.nudVersnellen.TabIndex = 2;
            // 
            // nudVertragen
            // 
            this.nudVertragen.Location = new System.Drawing.Point(146, 167);
            this.nudVertragen.Name = "nudVertragen";
            this.nudVertragen.Size = new System.Drawing.Size(120, 20);
            this.nudVertragen.TabIndex = 2;
            // 
            // btnOpstappen
            // 
            this.btnOpstappen.Location = new System.Drawing.Point(323, 18);
            this.btnOpstappen.Name = "btnOpstappen";
            this.btnOpstappen.Size = new System.Drawing.Size(75, 23);
            this.btnOpstappen.TabIndex = 3;
            this.btnOpstappen.Text = "Opstappen";
            this.btnOpstappen.UseVisualStyleBackColor = true;
            this.btnOpstappen.Click += new System.EventHandler(this.btnOpstappen_Click);
            // 
            // btnAfstappen
            // 
            this.btnAfstappen.Location = new System.Drawing.Point(323, 68);
            this.btnAfstappen.Name = "btnAfstappen";
            this.btnAfstappen.Size = new System.Drawing.Size(75, 23);
            this.btnAfstappen.TabIndex = 3;
            this.btnAfstappen.Text = "Afstappen";
            this.btnAfstappen.UseVisualStyleBackColor = true;
            this.btnAfstappen.Click += new System.EventHandler(this.btnAfstappen_Click);
            // 
            // btnVersnellen
            // 
            this.btnVersnellen.Location = new System.Drawing.Point(323, 112);
            this.btnVersnellen.Name = "btnVersnellen";
            this.btnVersnellen.Size = new System.Drawing.Size(75, 23);
            this.btnVersnellen.TabIndex = 3;
            this.btnVersnellen.Text = "Versnellen";
            this.btnVersnellen.UseVisualStyleBackColor = true;
            this.btnVersnellen.Click += new System.EventHandler(this.btnVersnellen_Click);
            // 
            // btnVertragen
            // 
            this.btnVertragen.Location = new System.Drawing.Point(323, 164);
            this.btnVertragen.Name = "btnVertragen";
            this.btnVertragen.Size = new System.Drawing.Size(75, 23);
            this.btnVertragen.TabIndex = 3;
            this.btnVertragen.Text = "Vertragen";
            this.btnVertragen.UseVisualStyleBackColor = true;
            this.btnVertragen.Click += new System.EventHandler(this.btnVertragen_Click);
            // 
            // btnStoppen
            // 
            this.btnStoppen.Location = new System.Drawing.Point(35, 229);
            this.btnStoppen.Name = "btnStoppen";
            this.btnStoppen.Size = new System.Drawing.Size(91, 39);
            this.btnStoppen.TabIndex = 3;
            this.btnStoppen.Text = "Stoppen";
            this.btnStoppen.UseVisualStyleBackColor = true;
            this.btnStoppen.Click += new System.EventHandler(this.btnStoppen_Click);
            // 
            // btnDeurenSluiten
            // 
            this.btnDeurenSluiten.Location = new System.Drawing.Point(165, 229);
            this.btnDeurenSluiten.Name = "btnDeurenSluiten";
            this.btnDeurenSluiten.Size = new System.Drawing.Size(91, 39);
            this.btnDeurenSluiten.TabIndex = 3;
            this.btnDeurenSluiten.Text = "Deuren Sluiten";
            this.btnDeurenSluiten.UseVisualStyleBackColor = true;
            this.btnDeurenSluiten.Click += new System.EventHandler(this.btnDeurenSluiten_Click);
            // 
            // btnStandVanZaken
            // 
            this.btnStandVanZaken.Location = new System.Drawing.Point(297, 229);
            this.btnStandVanZaken.Name = "btnStandVanZaken";
            this.btnStandVanZaken.Size = new System.Drawing.Size(91, 39);
            this.btnStandVanZaken.TabIndex = 3;
            this.btnStandVanZaken.Text = "Stand van zaken";
            this.btnStandVanZaken.UseVisualStyleBackColor = true;
            this.btnStandVanZaken.Click += new System.EventHandler(this.btnStandVanZaken_Click);
            // 
            // txtStandVanZaken
            // 
            this.txtStandVanZaken.Location = new System.Drawing.Point(35, 274);
            this.txtStandVanZaken.Multiline = true;
            this.txtStandVanZaken.Name = "txtStandVanZaken";
            this.txtStandVanZaken.Size = new System.Drawing.Size(353, 111);
            this.txtStandVanZaken.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 424);
            this.Controls.Add(this.txtStandVanZaken);
            this.Controls.Add(this.btnStandVanZaken);
            this.Controls.Add(this.btnDeurenSluiten);
            this.Controls.Add(this.btnStoppen);
            this.Controls.Add(this.btnVertragen);
            this.Controls.Add(this.btnVersnellen);
            this.Controls.Add(this.btnAfstappen);
            this.Controls.Add(this.btnOpstappen);
            this.Controls.Add(this.nudVertragen);
            this.Controls.Add(this.nudVersnellen);
            this.Controls.Add(this.nudAfstappen);
            this.Controls.Add(this.nudOpstappen);
            this.Controls.Add(this.lblVertragen);
            this.Controls.Add(this.lblVersnellen);
            this.Controls.Add(this.lblAfstappen);
            this.Controls.Add(this.lblOpstappen);
            this.Name = "Form1";
            this.Text = "Les 15 deel 2: Oefening Trein";
            ((System.ComponentModel.ISupportInitialize)(this.nudOpstappen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAfstappen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVersnellen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVertragen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOpstappen;
        private System.Windows.Forms.Label lblAfstappen;
        private System.Windows.Forms.Label lblVersnellen;
        private System.Windows.Forms.Label lblVertragen;
        private System.Windows.Forms.NumericUpDown nudOpstappen;
        private System.Windows.Forms.NumericUpDown nudAfstappen;
        private System.Windows.Forms.NumericUpDown nudVersnellen;
        private System.Windows.Forms.NumericUpDown nudVertragen;
        private System.Windows.Forms.Button btnOpstappen;
        private System.Windows.Forms.Button btnAfstappen;
        private System.Windows.Forms.Button btnVersnellen;
        private System.Windows.Forms.Button btnVertragen;
        private System.Windows.Forms.Button btnStoppen;
        private System.Windows.Forms.Button btnDeurenSluiten;
        private System.Windows.Forms.Button btnStandVanZaken;
        private System.Windows.Forms.TextBox txtStandVanZaken;
    }
}

