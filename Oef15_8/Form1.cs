﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_8
{
    public partial class Form1 : Form
    {
        private Trein trein = new Trein();
        public Form1()
        {
            InitializeComponent();
        }
        private void btnOpstappen_Click(object sender, EventArgs e)
        {
            if (!trein.Opstappen(Convert.ToInt32(nudOpstappen.Value)))
                txtStandVanZaken.Text = "Kan niet opstappen, deuren zijn toe!" + Environment.NewLine;
        }

        private void btnAfstappen_Click(object sender, EventArgs e)
        {
            if (!trein.Afstappen(Convert.ToInt32(nudAfstappen.Value)))
                txtStandVanZaken.Text = "Kan niet afstappen, deuren zijn toe!" + Environment.NewLine;
        }

        private void btnVersnellen_Click(object sender, EventArgs e)
        {
            if (!trein.Versnellen(Convert.ToInt32(nudVersnellen.Value)))
                txtStandVanZaken.Text = "Kan niet versnellen, deuren zijn nog open!" + Environment.NewLine;
        }

        private void btnVertragen_Click(object sender, EventArgs e)
        {
            trein.Remmen(Convert.ToInt32(nudVertragen.Value));
        }
        
        private void btnStoppen_Click(object sender, EventArgs e)
        {
            trein.Stoppen();
            trein.DeurOpen = true;
        }
        
        private void btnDeurenSluiten_Click(object sender, EventArgs e)
        {
            trein.Stoppen();
            trein.DeurOpen = false;
        }
        
        private void btnStandVanZaken_Click(object sender, EventArgs e)
        {
            txtStandVanZaken.Text = trein.StandVanZaken();
        }



    }
}
