﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Oef21_1
{
    public partial class FrmLogin : Form
    {
        List<Gebruiker> gebruikers = new List<Gebruiker>();
        public FrmLogin()
        {
            InitializeComponent();
            DataInladen();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Gebruiker login = new Gebruiker(txtGebruikersnaam.Text, txtWachtwoord.Text);

            if (gebruikers.Contains(login))
                this.Close();
            else
                MessageBox.Show("Gebruikersnaam en/of wachtwoord onjuist.");
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void DataInladen()
        {
            string path = @"C:\Users\Abdullah\Desktop\Cevora\Advanced_Csharp_-_WinForms\Oef 21.1\Gebruikers.txt";
            string inhoud = "";
            string[] inhoud2;

            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    inhoud = sr.ReadLine();
                    inhoud2 = inhoud.Split(';');
                    gebruikers.Add(new Gebruiker(inhoud2[0], inhoud2[1]));
                }
            }
        }
    }
}
