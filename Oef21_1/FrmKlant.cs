﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Oef21_1
{
    public partial class FrmKlant : Form
    {
        List<Klant> klanten = new List<Klant>();
        string filepath = @"C:\Users\Abdullah\Desktop\Cevora\Advanced_Csharp_-_WinForms\Oef 21.1\Klanten.txt";
        public FrmKlant()
        {
            InitializeComponent();
            KlantenLaden();
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void KlantenLaden()
        {
            string inhoud = "";
            string[] inhoud2;

            using (StreamReader sr = new StreamReader(filepath))
            {
                while (!sr.EndOfStream)
                {
                    inhoud = sr.ReadLine();
                    inhoud2 = inhoud.Split(';');
                    klanten.Add(new Klant(inhoud2[0], inhoud2[1], int.Parse(inhoud2[2]), inhoud2[3], inhoud2[4], inhoud2[5], inhoud2[6]));
                }
            }
        }

        private void ArtikelenOpslaan()
        {
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                string gegevens = "";

                foreach (Klant klant in klanten)
                {
                    gegevens += klant.ToString();
                }
                sw.Write(gegevens);
            }
        }

        private void FormulierResetten()
        {
            txtNaam.Text = "";
            txtStraatEnNr.Text = "";
            txtPostcode.Text = "";
            txtGemeente.Text = "";
            txtTelefoon.Text = "";
            txtEmail.Text = "";
            cbHardware.Checked = false;
            cbInternet.Checked = false;
            cbMultimedia.Checked = false;
            cbSoftware.Checked = false;
        }

        private void btnOpslaan_Click(object sender, EventArgs e)
        {
            bool validated = true;
            string naam, straatEnNr, gemeente, telefoon, email, mailVoorkeuren = "";
            int postcode;

            if (!int.TryParse(txtPostcode.Text, out postcode))
            {
                validated = false;
                MessageBox.Show("Eenheidsprijs zonder BTW: Cijfers ingeven!");
            }

            naam = txtNaam.Text;
            straatEnNr = txtStraatEnNr.Text;
            gemeente = txtGemeente.Text;
            telefoon = txtTelefoon.Text;
            email = txtEmail.Text;
            mailVoorkeuren += cbHardware.Checked ? "Hardware " :
                cbInternet.Checked ? "Internet " : cbMultimedia.Checked ? "Multimedia " : cbSoftware.Checked ? "Software " : "";

            if (validated)
            {
                klanten.Add(new Klant(naam, straatEnNr, postcode, gemeente, telefoon,email,mailVoorkeuren));
                ArtikelenOpslaan();
                FormulierResetten();
            }
            else
            {
                MessageBox.Show("Opslaan mislukt");
            }
        }
    }
}
