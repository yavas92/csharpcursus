﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef21_1
{
    class Artikel
    {
        // Variabelen
        private int _nummer;
        private string _omschrijving;
        private double _prijsExlBtw;
        private double _btwPercentage;


        // Properties
        public int Nummer
        {
            get { return _nummer; }
            set { _nummer = value; }
        }

        public string Omschrijving
        {
            get { return _omschrijving; }
            set { _omschrijving = value; }
        }

        public double PrijsExclBtw
        {
            get { return _prijsExlBtw; }
            set { _prijsExlBtw = value; }
        }

        public double BtwPercentage
        {
            get { return _btwPercentage; }
            set { _btwPercentage = value; }
        }

        public double Btw
        {
            get { return PrijsExclBtw*BtwPercentage/100; }
        }

        public double PrijsInclBtw
        {
            get { return PrijsExclBtw + Btw; }
        }


        // Constructor
        public Artikel(int nummer, string omschrijving, double prijsExclBtw, double btwPercentage)
        {
            Nummer = nummer;
            Omschrijving = omschrijving;
            PrijsExclBtw = prijsExclBtw;
            BtwPercentage = btwPercentage;
        }


        // Methods
        public override string ToString()
        {
            return $"{Nummer};{Omschrijving};{PrijsExclBtw};{BtwPercentage}" + Environment.NewLine;
        }


    }
}
