﻿namespace Oef21_1
{
    class Gebruiker
    {
        // Attributen
        private string _login;
        private string _wachtwoord;


        // Properties

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string Wachtwoord
        {
            get { return _wachtwoord; }
            set { _wachtwoord = value; }
        }

        // Constructor
        public Gebruiker(string login, string wachtwoord)
        {
            Login = login;
            Wachtwoord = wachtwoord;
        }



        // Methods

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (!(obj is Gebruiker)) return false;

            Gebruiker g = (Gebruiker)obj;

            if (this.Login.ToUpper() != g.Login.ToUpper()) return false;
            if(this.Wachtwoord != g.Wachtwoord) return false;

            return true;
        }
    }
}