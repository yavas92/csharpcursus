﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef21_1
{
    class Klant
    {
        // Variabelen
        private string _naam;
        private string _straatEnNr;
        private int _postcode;
        private string _gemeente;
        private string _telefoon;
        private string _email;
        private string _mailVoorkeuren;

        // Properties
        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }

        public string StraatEnNr
        {
            get { return _straatEnNr; }
            set { _straatEnNr = value; }
        }

        public int Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }

        public string Gemeente
        {
            get { return _gemeente; }
            set { _gemeente = value; }
        }

        public string Telefoon
        {
            get { return _telefoon; }
            set { _telefoon = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Mailvoorkeuren
        {
            get { return _mailVoorkeuren; }
            set { _mailVoorkeuren = value; }
        }

        // Constructor
        public Klant(string naam, string straatEnNr, int postcode, string gemeente, string telefoon, string email, string mailVoorkeuren)
        {
            Naam = naam;
            StraatEnNr = straatEnNr;
            Postcode = postcode;
            Gemeente = gemeente;
            Telefoon = telefoon;
            Email = email;
            Mailvoorkeuren = mailVoorkeuren;
        }

        // Methods
        public override string ToString()
        {
            return $"{Naam};{StraatEnNr};{Postcode};{Gemeente};{Telefoon};{Email};{Mailvoorkeuren}" + Environment.NewLine;
        }

    }
}
