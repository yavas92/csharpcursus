﻿namespace Oef21_1
{
    partial class FrmKlant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStraatEnNr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTelefoon = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGemeente = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbHardware = new System.Windows.Forms.CheckBox();
            this.cbSoftware = new System.Windows.Forms.CheckBox();
            this.cbInternet = new System.Windows.Forms.CheckBox();
            this.cbMultimedia = new System.Windows.Forms.CheckBox();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.btnOpslaan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(151, 13);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(334, 20);
            this.txtNaam.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Naam";
            // 
            // txtStraatEnNr
            // 
            this.txtStraatEnNr.Location = new System.Drawing.Point(151, 47);
            this.txtStraatEnNr.Name = "txtStraatEnNr";
            this.txtStraatEnNr.Size = new System.Drawing.Size(334, 20);
            this.txtStraatEnNr.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Straat en nr";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Location = new System.Drawing.Point(151, 81);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.Size = new System.Drawing.Size(79, 20);
            this.txtPostcode.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Postcode";
            // 
            // txtTelefoon
            // 
            this.txtTelefoon.Location = new System.Drawing.Point(151, 114);
            this.txtTelefoon.Name = "txtTelefoon";
            this.txtTelefoon.Size = new System.Drawing.Size(334, 20);
            this.txtTelefoon.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Telefoon";
            // 
            // txtGemeente
            // 
            this.txtGemeente.Location = new System.Drawing.Point(386, 81);
            this.txtGemeente.Name = "txtGemeente";
            this.txtGemeente.Size = new System.Drawing.Size(99, 20);
            this.txtGemeente.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(277, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Gemeente";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(151, 148);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(334, 20);
            this.txtEmail.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "E-mail";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(517, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "`````````````````````````````````````````````````````````````````````````````````" +
    "````````````````````````````````````````````````````````````````````````````````" +
    "`````````";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(42, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mailing m.b.v.";
            // 
            // cbHardware
            // 
            this.cbHardware.AutoSize = true;
            this.cbHardware.Location = new System.Drawing.Point(45, 257);
            this.cbHardware.Name = "cbHardware";
            this.cbHardware.Size = new System.Drawing.Size(72, 17);
            this.cbHardware.TabIndex = 6;
            this.cbHardware.Text = "Hardware";
            this.cbHardware.UseVisualStyleBackColor = true;
            // 
            // cbSoftware
            // 
            this.cbSoftware.AutoSize = true;
            this.cbSoftware.Location = new System.Drawing.Point(45, 294);
            this.cbSoftware.Name = "cbSoftware";
            this.cbSoftware.Size = new System.Drawing.Size(68, 17);
            this.cbSoftware.TabIndex = 8;
            this.cbSoftware.Text = "Software";
            this.cbSoftware.UseVisualStyleBackColor = true;
            // 
            // cbInternet
            // 
            this.cbInternet.AutoSize = true;
            this.cbInternet.Location = new System.Drawing.Point(175, 257);
            this.cbInternet.Name = "cbInternet";
            this.cbInternet.Size = new System.Drawing.Size(62, 17);
            this.cbInternet.TabIndex = 7;
            this.cbInternet.Text = "Internet";
            this.cbInternet.UseVisualStyleBackColor = true;
            // 
            // cbMultimedia
            // 
            this.cbMultimedia.AutoSize = true;
            this.cbMultimedia.Location = new System.Drawing.Point(175, 294);
            this.cbMultimedia.Name = "cbMultimedia";
            this.cbMultimedia.Size = new System.Drawing.Size(76, 17);
            this.cbMultimedia.TabIndex = 9;
            this.cbMultimedia.Text = "Multimedia";
            this.cbMultimedia.UseVisualStyleBackColor = true;
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.Location = new System.Drawing.Point(399, 225);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(86, 30);
            this.btnAnnuleren.TabIndex = 11;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // btnOpslaan
            // 
            this.btnOpslaan.Location = new System.Drawing.Point(386, 273);
            this.btnOpslaan.Name = "btnOpslaan";
            this.btnOpslaan.Size = new System.Drawing.Size(99, 38);
            this.btnOpslaan.TabIndex = 10;
            this.btnOpslaan.Text = "Opslaan";
            this.btnOpslaan.UseVisualStyleBackColor = true;
            this.btnOpslaan.Click += new System.EventHandler(this.btnOpslaan_Click);
            // 
            // FrmKlant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 371);
            this.Controls.Add(this.btnOpslaan);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.cbMultimedia);
            this.Controls.Add(this.cbSoftware);
            this.Controls.Add(this.cbInternet);
            this.Controls.Add(this.cbHardware);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtTelefoon);
            this.Controls.Add(this.txtGemeente);
            this.Controls.Add(this.txtPostcode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtStraatEnNr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNaam);
            this.Name = "FrmKlant";
            this.Text = "Klant";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStraatEnNr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPostcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTelefoon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGemeente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox cbHardware;
        private System.Windows.Forms.CheckBox cbSoftware;
        private System.Windows.Forms.CheckBox cbInternet;
        private System.Windows.Forms.CheckBox cbMultimedia;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.Button btnOpslaan;
    }
}