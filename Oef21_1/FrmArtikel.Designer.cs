﻿namespace Oef21_1
{
    partial class FrmArtikel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtArtikelnummer = new System.Windows.Forms.TextBox();
            this.txtArtikelomschrijving = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPrijsExclBtw = new System.Windows.Forms.TextBox();
            this.gbBTW = new System.Windows.Forms.GroupBox();
            this.rdbTypeBTW2 = new System.Windows.Forms.RadioButton();
            this.rdbTypeBTW1 = new System.Windows.Forms.RadioButton();
            this.txtBtw = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrijsInclBtw = new System.Windows.Forms.TextBox();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.btnOpslaan = new System.Windows.Forms.Button();
            this.gbBTW.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Artikelnummer";
            // 
            // txtArtikelnummer
            // 
            this.txtArtikelnummer.Location = new System.Drawing.Point(164, 13);
            this.txtArtikelnummer.Name = "txtArtikelnummer";
            this.txtArtikelnummer.Size = new System.Drawing.Size(100, 20);
            this.txtArtikelnummer.TabIndex = 0;
            // 
            // txtArtikelomschrijving
            // 
            this.txtArtikelomschrijving.Location = new System.Drawing.Point(164, 54);
            this.txtArtikelomschrijving.Name = "txtArtikelomschrijving";
            this.txtArtikelomschrijving.Size = new System.Drawing.Size(100, 20);
            this.txtArtikelomschrijving.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Artikelomschrijving";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Eenheidsprijs zonder BTW";
            // 
            // txtPrijsExclBtw
            // 
            this.txtPrijsExclBtw.Location = new System.Drawing.Point(164, 97);
            this.txtPrijsExclBtw.Name = "txtPrijsExclBtw";
            this.txtPrijsExclBtw.Size = new System.Drawing.Size(100, 20);
            this.txtPrijsExclBtw.TabIndex = 2;
            this.txtPrijsExclBtw.TextChanged += new System.EventHandler(this.txtPrijsExclBtw_TextChanged);
            // 
            // gbBTW
            // 
            this.gbBTW.Controls.Add(this.rdbTypeBTW2);
            this.gbBTW.Controls.Add(this.rdbTypeBTW1);
            this.gbBTW.Location = new System.Drawing.Point(15, 139);
            this.gbBTW.Name = "gbBTW";
            this.gbBTW.Size = new System.Drawing.Size(169, 90);
            this.gbBTW.TabIndex = 4;
            this.gbBTW.TabStop = false;
            this.gbBTW.Text = "BTW percentage";
            // 
            // rdbTypeBTW2
            // 
            this.rdbTypeBTW2.AutoSize = true;
            this.rdbTypeBTW2.Location = new System.Drawing.Point(12, 52);
            this.rdbTypeBTW2.Name = "rdbTypeBTW2";
            this.rdbTypeBTW2.Size = new System.Drawing.Size(45, 17);
            this.rdbTypeBTW2.TabIndex = 3;
            this.rdbTypeBTW2.TabStop = true;
            this.rdbTypeBTW2.Text = "21%";
            this.rdbTypeBTW2.UseVisualStyleBackColor = true;
            this.rdbTypeBTW2.CheckedChanged += new System.EventHandler(this.rdbTypeBTW2_CheckedChanged);
            // 
            // rdbTypeBTW1
            // 
            this.rdbTypeBTW1.AutoSize = true;
            this.rdbTypeBTW1.Checked = true;
            this.rdbTypeBTW1.Location = new System.Drawing.Point(13, 24);
            this.rdbTypeBTW1.Name = "rdbTypeBTW1";
            this.rdbTypeBTW1.Size = new System.Drawing.Size(39, 17);
            this.rdbTypeBTW1.TabIndex = 3;
            this.rdbTypeBTW1.TabStop = true;
            this.rdbTypeBTW1.Text = "6%";
            this.rdbTypeBTW1.UseVisualStyleBackColor = true;
            this.rdbTypeBTW1.CheckedChanged += new System.EventHandler(this.rdbTypeBTW1_CheckedChanged);
            // 
            // txtBtw
            // 
            this.txtBtw.Location = new System.Drawing.Point(164, 258);
            this.txtBtw.Name = "txtBtw";
            this.txtBtw.Size = new System.Drawing.Size(100, 20);
            this.txtBtw.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "BTW";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Eenheidsprijd met BTW";
            // 
            // txtPrijsInclBtw
            // 
            this.txtPrijsInclBtw.Location = new System.Drawing.Point(164, 308);
            this.txtPrijsInclBtw.Name = "txtPrijsInclBtw";
            this.txtPrijsInclBtw.Size = new System.Drawing.Size(100, 20);
            this.txtPrijsInclBtw.TabIndex = 5;
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.Location = new System.Drawing.Point(370, 258);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(102, 36);
            this.btnAnnuleren.TabIndex = 7;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // btnOpslaan
            // 
            this.btnOpslaan.Location = new System.Drawing.Point(362, 310);
            this.btnOpslaan.Name = "btnOpslaan";
            this.btnOpslaan.Size = new System.Drawing.Size(115, 53);
            this.btnOpslaan.TabIndex = 6;
            this.btnOpslaan.Text = "Opslaan";
            this.btnOpslaan.UseVisualStyleBackColor = true;
            this.btnOpslaan.Click += new System.EventHandler(this.btnOpslaan_Click);
            // 
            // FrmArtikel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 377);
            this.Controls.Add(this.btnOpslaan);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.txtPrijsInclBtw);
            this.Controls.Add(this.txtBtw);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbBTW);
            this.Controls.Add(this.txtPrijsExclBtw);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtArtikelomschrijving);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtArtikelnummer);
            this.Controls.Add(this.label1);
            this.Name = "FrmArtikel";
            this.Text = "Artikel";
            this.gbBTW.ResumeLayout(false);
            this.gbBTW.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtArtikelnummer;
        private System.Windows.Forms.TextBox txtArtikelomschrijving;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPrijsExclBtw;
        private System.Windows.Forms.GroupBox gbBTW;
        private System.Windows.Forms.RadioButton rdbTypeBTW2;
        private System.Windows.Forms.RadioButton rdbTypeBTW1;
        private System.Windows.Forms.TextBox txtBtw;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrijsInclBtw;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.Button btnOpslaan;
    }
}