﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Oef21_1
{
    public partial class FrmCreatura : Form
    {
        List<Gebruiker> gebruikers = new List<Gebruiker>();
        List<Artikel> artikelen = new List<Artikel>();
        List<Klant> klanten = new List<Klant>();

        public FrmCreatura()
        {
            InitializeComponent();
        }

        private void FrmCreatura_Load(object sender, EventArgs e)
        {
            FrmLogin loginForm = new FrmLogin();
            loginForm.ShowDialog();
        }

        private void mnuBestandArtikel_Click(object sender, EventArgs e)
        {
            FrmArtikel artikelForm = new FrmArtikel();
            artikelForm.ShowDialog();
        }

        private void mnuBestandKlant_Click(object sender, EventArgs e)
        {
            FrmKlant klantForm = new FrmKlant();
            klantForm.ShowDialog();
        }

        private void mnuHelpOver_Click(object sender, EventArgs e)
        {
            FrmOver infoForm = new FrmOver();
            infoForm.ShowDialog();
        }


        private void mnuBestandAndere_Click(object sender, EventArgs e)
        {
            FrmLogin loginForm = new FrmLogin();
            loginForm.ShowDialog();
        }

        private void mnuBestandAfsluiten_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
