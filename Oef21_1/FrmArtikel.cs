﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Oef21_1
{
    public partial class FrmArtikel : Form
    {
        List<Artikel> artikelen = new List<Artikel>();
        string filepath = @"C:\Users\Abdullah\Desktop\Cevora\Advanced_Csharp_-_WinForms\Oef 21.1\Artikelen.txt";

        public FrmArtikel()
        {
            InitializeComponent();
            ArtikelenLaden();
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ArtikelenLaden()
        {
            string inhoud = "";
            string[] inhoud2;

            using (StreamReader sr = new StreamReader(filepath))
            {
                while (!sr.EndOfStream)
                {
                    inhoud = sr.ReadLine();
                    inhoud2 = inhoud.Split(';');
                    artikelen.Add(new Artikel(int.Parse(inhoud2[0]), inhoud2[1], double.Parse(inhoud2[2]), double.Parse(inhoud2[3])));
                }
            }
        }

        private void ArtikelenOpslaan()
        {
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                string gegevens = "";

                foreach (Artikel artikel in artikelen)
                {
                    gegevens += artikel.ToString();
                }
                sw.Write(gegevens);
            }
        }

        private void FormulierResetten()
        {
            txtArtikelnummer.Text = "";
            txtArtikelomschrijving.Text = "";
            txtPrijsExclBtw.Text = "";
            rdbTypeBTW1.Checked = true;
            rdbTypeBTW2.Checked = false;
            txtBtw.Text = "";
            txtPrijsInclBtw.Text = "";
        }

        private void btnOpslaan_Click(object sender, EventArgs e)
        {
            bool validated = true;
            int nummer, btwPercentage;
            double prijs;
            if (!int.TryParse(txtArtikelnummer.Text, out nummer))
            {
                validated = false;
                MessageBox.Show("Artikelnummer: Geheel getal ingeven!");

            }
            string omschrijving = txtArtikelomschrijving.Text;

            if (!double.TryParse(txtPrijsExclBtw.Text, out prijs))
            {
                validated = false;
                MessageBox.Show("Eenheidsprijs zonder BTW: Cijfers ingeven!");
            }

            if (rdbTypeBTW1.Checked)
                btwPercentage = 6;
            else
                btwPercentage = 21;

            if (validated)
            {
                artikelen.Add(new Artikel(nummer, omschrijving, prijs, btwPercentage));
                ArtikelenOpslaan();
                FormulierResetten();
            }
            else
            {
                MessageBox.Show("Opslaan mislukt");
            }
        }

        private void txtPrijsExclBtw_TextChanged(object sender, EventArgs e)
        {
            BerekenBtwEnPrijsInclBtw();

        }

        private void BerekenBtwEnPrijsInclBtw()
        {
            int btwPercentage;
            double prijs;

            if (rdbTypeBTW1.Checked)
                btwPercentage = 6;
            else
                btwPercentage = 21;

            if (!double.TryParse(txtPrijsExclBtw.Text, out prijs))
            {
                txtBtw.Text = "";
                txtPrijsInclBtw.Text = "";
            }
            else
            {
                txtBtw.Text = (prijs * btwPercentage / 100).ToString();
                txtPrijsInclBtw.Text = (prijs + double.Parse(txtBtw.Text)).ToString();
            }
        }

        private void rdbTypeBTW1_CheckedChanged(object sender, EventArgs e)
        {
            BerekenBtwEnPrijsInclBtw();
        }

        private void rdbTypeBTW2_CheckedChanged(object sender, EventArgs e)
        {
            BerekenBtwEnPrijsInclBtw();
        }
    }
}
