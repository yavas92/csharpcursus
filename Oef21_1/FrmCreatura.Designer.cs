﻿namespace Oef21_1
{
    partial class FrmCreatura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuBestand = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBestandArtikel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBestandKlant = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBestandAndere = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBestandAfsluiten = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuBestand,
            this.mnuHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(498, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuBestand
            // 
            this.mnuBestand.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuBestandArtikel,
            this.mnuBestandKlant,
            this.mnuBestandAndere,
            this.mnuBestandAfsluiten});
            this.mnuBestand.Name = "mnuBestand";
            this.mnuBestand.Size = new System.Drawing.Size(61, 20);
            this.mnuBestand.Text = "Bestand";
            // 
            // mnuBestandArtikel
            // 
            this.mnuBestandArtikel.Name = "mnuBestandArtikel";
            this.mnuBestandArtikel.Size = new System.Drawing.Size(180, 22);
            this.mnuBestandArtikel.Text = "Artikel";
            this.mnuBestandArtikel.Click += new System.EventHandler(this.mnuBestandArtikel_Click);
            // 
            // mnuBestandKlant
            // 
            this.mnuBestandKlant.Name = "mnuBestandKlant";
            this.mnuBestandKlant.Size = new System.Drawing.Size(180, 22);
            this.mnuBestandKlant.Text = "Klant";
            this.mnuBestandKlant.Click += new System.EventHandler(this.mnuBestandKlant_Click);
            // 
            // mnuBestandAndere
            // 
            this.mnuBestandAndere.Name = "mnuBestandAndere";
            this.mnuBestandAndere.Size = new System.Drawing.Size(180, 22);
            this.mnuBestandAndere.Text = "Andere medewerker";
            this.mnuBestandAndere.Click += new System.EventHandler(this.mnuBestandAndere_Click);
            // 
            // mnuBestandAfsluiten
            // 
            this.mnuBestandAfsluiten.Name = "mnuBestandAfsluiten";
            this.mnuBestandAfsluiten.Size = new System.Drawing.Size(180, 22);
            this.mnuBestandAfsluiten.Text = "Afsluiten";
            this.mnuBestandAfsluiten.Click += new System.EventHandler(this.mnuBestandAfsluiten_Click);
            // 
            // mnuHelp
            // 
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHelpInfo});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(44, 20);
            this.mnuHelp.Text = "Help";
            // 
            // mnuHelpInfo
            // 
            this.mnuHelpInfo.Name = "mnuHelpInfo";
            this.mnuHelpInfo.Size = new System.Drawing.Size(95, 22);
            this.mnuHelpInfo.Text = "Info";
            this.mnuHelpInfo.Click += new System.EventHandler(this.mnuHelpOver_Click);
            // 
            // FrmCreatura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 310);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FrmCreatura";
            this.Text = "Creatura";
            this.Load += new System.EventHandler(this.FrmCreatura_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuBestand;
        private System.Windows.Forms.ToolStripMenuItem mnuBestandArtikel;
        private System.Windows.Forms.ToolStripMenuItem mnuBestandKlant;
        private System.Windows.Forms.ToolStripMenuItem mnuBestandAndere;
        private System.Windows.Forms.ToolStripMenuItem mnuBestandAfsluiten;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpInfo;
    }
}

