﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace Oef13_2
{
    public partial class Form1 : Form
    {
        int index;

        public Form1()
        {
            InitializeComponent();
        }

        // Regel toevoegen in woordenboeklijst
        private void btnVoegToeAanWoordenboek_Click(object sender, EventArgs e)
        {
            ListViewItem lv = new ListViewItem(txtNederlands.Text);
            lv.SubItems.Add(new ListViewSubItem(lv, txtFrans.Text));
            lv.SubItems.Add(new ListViewSubItem(lv, txtEngels.Text));
            lvWoordenboek.Items.Add(lv);

            txtNederlands.Text = string.Empty;
            txtFrans.Text = string.Empty;
            txtEngels.Text = string.Empty;
        }

        // Regel verwijderen in woordenboeklijst
        private void btnVerwijderUitWoordenboek_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvWoordenboek.SelectedItems)
            {
                lvWoordenboek.Items.Remove(item);
            }
        }

        // Regel aanpassen in woordenboeklijst
        private void btnWijzigInWoordenboek_Click(object sender, EventArgs e)
        {
            lvWoordenboek.SelectedItems[0].SubItems[0].Text = txtNederlands.Text;
            lvWoordenboek.SelectedItems[0].SubItems[1].Text = txtFrans.Text;
            lvWoordenboek.SelectedItems[0].SubItems[2].Text = txtEngels.Text;
        }

        // Woordenboeklijst opslaan (aanmaken/overschrijven)
        private void btnBewaarWoordenboek_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter("C:/BestandenCSharp/woordenboek.txt"))
            {
                foreach (ListViewItem item in lvWoordenboek.Items)
                {
                    writer.WriteLine($"{item.SubItems[0].Text}, {item.SubItems[1].Text}, {item.SubItems[2].Text}");
                }

            }
        }

        // Woordenboeklijst inladen
        private void btnLaadWoordenboek_Click(object sender, EventArgs e)
        {
            lvWoordenboek.Items.Clear();
            using (StreamReader reader = new StreamReader("C:/BestandenCSharp/woordenboek.txt"))
            {
                ListViewItem lv;

                while (!reader.EndOfStream)
                {
                    string itemString = reader.ReadLine();
                    int woordLengte = itemString.IndexOf(',');
                    
                    lv = new ListViewItem(itemString.Substring(0, woordLengte));
                    itemString = itemString.Substring(woordLengte+2);
                    woordLengte = itemString.IndexOf(',');

                    lv.SubItems.Add(new ListViewSubItem(lv, itemString.Substring(0, woordLengte)));
                    itemString = itemString.Substring(woordLengte + 2);

                    lv.SubItems.Add(new ListViewSubItem(lv, itemString));
                    lvWoordenboek.Items.Add(lv);
                }
            }
        }

        // Programma afsluiten
        private void btnAfsluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Als er een index geselecteerd wordt de geselecteerde gegevens in de textboxen zetten. De indexnr bewaren in 'index' variabele
        private void lvWoordenboek_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnWijzigInWoordenboek.Enabled = true;
            btnVerwijderUitWoordenboek.Enabled = true;

            txtNederlands.Text = lvWoordenboek.SelectedItems[0].SubItems[0].Text;
            txtFrans.Text = lvWoordenboek.SelectedItems[0].SubItems[1].Text;
            txtEngels.Text = lvWoordenboek.SelectedItems[0].SubItems[2].Text;

        }

    }
}
