﻿namespace Oef13_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNederlands = new System.Windows.Forms.Label();
            this.lblFrans = new System.Windows.Forms.Label();
            this.lblEngels = new System.Windows.Forms.Label();
            this.btnWijzigInWoordenboek = new System.Windows.Forms.Button();
            this.txtNederlands = new System.Windows.Forms.TextBox();
            this.txtFrans = new System.Windows.Forms.TextBox();
            this.txtEngels = new System.Windows.Forms.TextBox();
            this.btnVerwijderUitWoordenboek = new System.Windows.Forms.Button();
            this.btnVoegToeAanWoordenboek = new System.Windows.Forms.Button();
            this.lvWoordenboek = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnWisSelectie = new System.Windows.Forms.Button();
            this.btnLaadWoordenboek = new System.Windows.Forms.Button();
            this.btnBewaarWoordenboek = new System.Windows.Forms.Button();
            this.btnAfsluiten = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNederlands
            // 
            this.lblNederlands.AutoSize = true;
            this.lblNederlands.Location = new System.Drawing.Point(13, 14);
            this.lblNederlands.Name = "lblNederlands";
            this.lblNederlands.Size = new System.Drawing.Size(61, 13);
            this.lblNederlands.TabIndex = 0;
            this.lblNederlands.Text = "Nederlands";
            // 
            // lblFrans
            // 
            this.lblFrans.AutoSize = true;
            this.lblFrans.Location = new System.Drawing.Point(268, 14);
            this.lblFrans.Name = "lblFrans";
            this.lblFrans.Size = new System.Drawing.Size(33, 13);
            this.lblFrans.TabIndex = 1;
            this.lblFrans.Text = "Frans";
            // 
            // lblEngels
            // 
            this.lblEngels.AutoSize = true;
            this.lblEngels.Location = new System.Drawing.Point(520, 14);
            this.lblEngels.Name = "lblEngels";
            this.lblEngels.Size = new System.Drawing.Size(39, 13);
            this.lblEngels.TabIndex = 2;
            this.lblEngels.Text = "Engels";
            // 
            // btnWijzigInWoordenboek
            // 
            this.btnWijzigInWoordenboek.Enabled = false;
            this.btnWijzigInWoordenboek.Location = new System.Drawing.Point(16, 56);
            this.btnWijzigInWoordenboek.Name = "btnWijzigInWoordenboek";
            this.btnWijzigInWoordenboek.Size = new System.Drawing.Size(246, 23);
            this.btnWijzigInWoordenboek.TabIndex = 3;
            this.btnWijzigInWoordenboek.Text = "Wijzig in woordenboek";
            this.btnWijzigInWoordenboek.UseVisualStyleBackColor = true;
            this.btnWijzigInWoordenboek.Click += new System.EventHandler(this.btnWijzigInWoordenboek_Click);
            // 
            // txtNederlands
            // 
            this.txtNederlands.Location = new System.Drawing.Point(16, 30);
            this.txtNederlands.Name = "txtNederlands";
            this.txtNederlands.Size = new System.Drawing.Size(246, 20);
            this.txtNederlands.TabIndex = 4;
            // 
            // txtFrans
            // 
            this.txtFrans.Location = new System.Drawing.Point(271, 30);
            this.txtFrans.Name = "txtFrans";
            this.txtFrans.Size = new System.Drawing.Size(246, 20);
            this.txtFrans.TabIndex = 5;
            // 
            // txtEngels
            // 
            this.txtEngels.Location = new System.Drawing.Point(523, 30);
            this.txtEngels.Name = "txtEngels";
            this.txtEngels.Size = new System.Drawing.Size(246, 20);
            this.txtEngels.TabIndex = 6;
            // 
            // btnVerwijderUitWoordenboek
            // 
            this.btnVerwijderUitWoordenboek.Enabled = false;
            this.btnVerwijderUitWoordenboek.Location = new System.Drawing.Point(271, 56);
            this.btnVerwijderUitWoordenboek.Name = "btnVerwijderUitWoordenboek";
            this.btnVerwijderUitWoordenboek.Size = new System.Drawing.Size(246, 23);
            this.btnVerwijderUitWoordenboek.TabIndex = 7;
            this.btnVerwijderUitWoordenboek.Text = "Verwijder uit woordenboek";
            this.btnVerwijderUitWoordenboek.UseVisualStyleBackColor = true;
            this.btnVerwijderUitWoordenboek.Click += new System.EventHandler(this.btnVerwijderUitWoordenboek_Click);
            // 
            // btnVoegToeAanWoordenboek
            // 
            this.btnVoegToeAanWoordenboek.Location = new System.Drawing.Point(523, 56);
            this.btnVoegToeAanWoordenboek.Name = "btnVoegToeAanWoordenboek";
            this.btnVoegToeAanWoordenboek.Size = new System.Drawing.Size(246, 23);
            this.btnVoegToeAanWoordenboek.TabIndex = 8;
            this.btnVoegToeAanWoordenboek.Text = "Voeg toe aan woordenboek";
            this.btnVoegToeAanWoordenboek.UseVisualStyleBackColor = true;
            this.btnVoegToeAanWoordenboek.Click += new System.EventHandler(this.btnVoegToeAanWoordenboek_Click);
            // 
            // lvWoordenboek
            // 
            this.lvWoordenboek.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvWoordenboek.GridLines = true;
            this.lvWoordenboek.HideSelection = false;
            this.lvWoordenboek.Location = new System.Drawing.Point(16, 85);
            this.lvWoordenboek.Name = "lvWoordenboek";
            this.lvWoordenboek.Size = new System.Drawing.Size(753, 285);
            this.lvWoordenboek.TabIndex = 9;
            this.lvWoordenboek.UseCompatibleStateImageBehavior = false;
            this.lvWoordenboek.View = System.Windows.Forms.View.Details;
            this.lvWoordenboek.SelectedIndexChanged += new System.EventHandler(this.lvWoordenboek_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nederlands";
            this.columnHeader1.Width = 250;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Frans";
            this.columnHeader2.Width = 254;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Engels";
            this.columnHeader3.Width = 245;
            // 
            // btnWisSelectie
            // 
            this.btnWisSelectie.Location = new System.Drawing.Point(16, 396);
            this.btnWisSelectie.Name = "btnWisSelectie";
            this.btnWisSelectie.Size = new System.Drawing.Size(152, 23);
            this.btnWisSelectie.TabIndex = 10;
            this.btnWisSelectie.Text = "Wis Selectie";
            this.btnWisSelectie.UseVisualStyleBackColor = true;
            // 
            // btnLaadWoordenboek
            // 
            this.btnLaadWoordenboek.Location = new System.Drawing.Point(271, 396);
            this.btnLaadWoordenboek.Name = "btnLaadWoordenboek";
            this.btnLaadWoordenboek.Size = new System.Drawing.Size(157, 23);
            this.btnLaadWoordenboek.TabIndex = 11;
            this.btnLaadWoordenboek.Text = "Laad WoordenBoek";
            this.btnLaadWoordenboek.UseVisualStyleBackColor = true;
            this.btnLaadWoordenboek.Click += new System.EventHandler(this.btnLaadWoordenboek_Click);
            // 
            // btnBewaarWoordenboek
            // 
            this.btnBewaarWoordenboek.Location = new System.Drawing.Point(434, 396);
            this.btnBewaarWoordenboek.Name = "btnBewaarWoordenboek";
            this.btnBewaarWoordenboek.Size = new System.Drawing.Size(157, 23);
            this.btnBewaarWoordenboek.TabIndex = 12;
            this.btnBewaarWoordenboek.Text = "Bewaar WoordenBoek";
            this.btnBewaarWoordenboek.UseVisualStyleBackColor = true;
            this.btnBewaarWoordenboek.Click += new System.EventHandler(this.btnBewaarWoordenboek_Click);
            // 
            // btnAfsluiten
            // 
            this.btnAfsluiten.Location = new System.Drawing.Point(597, 396);
            this.btnAfsluiten.Name = "btnAfsluiten";
            this.btnAfsluiten.Size = new System.Drawing.Size(157, 23);
            this.btnAfsluiten.TabIndex = 13;
            this.btnAfsluiten.Text = "Afsluiten";
            this.btnAfsluiten.UseVisualStyleBackColor = true;
            this.btnAfsluiten.Click += new System.EventHandler(this.btnAfsluiten_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 450);
            this.Controls.Add(this.btnAfsluiten);
            this.Controls.Add(this.btnBewaarWoordenboek);
            this.Controls.Add(this.btnLaadWoordenboek);
            this.Controls.Add(this.btnWisSelectie);
            this.Controls.Add(this.lvWoordenboek);
            this.Controls.Add(this.btnVoegToeAanWoordenboek);
            this.Controls.Add(this.btnVerwijderUitWoordenboek);
            this.Controls.Add(this.txtEngels);
            this.Controls.Add(this.txtFrans);
            this.Controls.Add(this.txtNederlands);
            this.Controls.Add(this.btnWijzigInWoordenboek);
            this.Controls.Add(this.lblEngels);
            this.Controls.Add(this.lblFrans);
            this.Controls.Add(this.lblNederlands);
            this.Name = "Form1";
            this.Text = "Oefening: Woordenlijst NL-FR-EN";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNederlands;
        private System.Windows.Forms.Label lblFrans;
        private System.Windows.Forms.Label lblEngels;
        private System.Windows.Forms.Button btnWijzigInWoordenboek;
        private System.Windows.Forms.TextBox txtNederlands;
        private System.Windows.Forms.TextBox txtFrans;
        private System.Windows.Forms.TextBox txtEngels;
        private System.Windows.Forms.Button btnVerwijderUitWoordenboek;
        private System.Windows.Forms.Button btnVoegToeAanWoordenboek;
        private System.Windows.Forms.ListView lvWoordenboek;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnWisSelectie;
        private System.Windows.Forms.Button btnLaadWoordenboek;
        private System.Windows.Forms.Button btnBewaarWoordenboek;
        private System.Windows.Forms.Button btnAfsluiten;
    }
}

