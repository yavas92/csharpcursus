﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef14_1
{
    public partial class Form1 : Form
    {
        int[,] arrGetallen = new int[10, 10];

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            ArrayAfdrukken();
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblBereken_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtRij.Text) > (arrGetallen.GetUpperBound(0)) || Convert.ToInt32(txtRij.Text) < arrGetallen.GetLowerBound(0))
            {
                MessageBox.Show($"De rij index moet tussen 0 en {arrGetallen.GetUpperBound(0)} zijn!", "Ongeldige invoer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (Convert.ToInt32(txtKol.Text) > (arrGetallen.GetUpperBound(1)) || Convert.ToInt32(txtKol.Text) < arrGetallen.GetLowerBound(1))
            {
                MessageBox.Show($"De kollom index moet tussen 0 en {arrGetallen.GetUpperBound(1)} zijn!", "Ongeldige invoer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            for (int i = 0; i < Convert.ToInt32(txtRij.Text); i++)
            {
                for (int j = 0; j < Convert.ToInt32(txtKol.Text); j++)
                {
                    arrGetallen[i, j] = Convert.ToInt32(txtGetal.Text);
                }
            }
            lbArray.Items.Clear();
            ArrayAfdrukken();
            VakkenLeegmaken();
        }

        void ArrayAfdrukken()
        {
            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                StringBuilder rij = new StringBuilder();

                for (int j = 0; j <= arrGetallen.GetUpperBound(1); j++)
                {
                    rij.Append($"{arrGetallen[i, j]}".PadRight(10));
                }
                lbArray.Items.Add(rij);
            }
        }

        void VakkenLeegmaken()
        {
            txtGetal.Text = string.Empty;
            txtKol.Text = string.Empty;
            txtRij.Text = string.Empty;
        }
    }
}
