﻿namespace Oef14_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRij = new System.Windows.Forms.Label();
            this.lblKolom = new System.Windows.Forms.Label();
            this.txtRij = new System.Windows.Forms.TextBox();
            this.txtKol = new System.Windows.Forms.TextBox();
            this.lblBereken = new System.Windows.Forms.Button();
            this.lbArray = new System.Windows.Forms.ListBox();
            this.txtGetal = new System.Windows.Forms.TextBox();
            this.lblGetal = new System.Windows.Forms.Label();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblRij
            // 
            this.lblRij.AutoSize = true;
            this.lblRij.Location = new System.Drawing.Point(12, 9);
            this.lblRij.Name = "lblRij";
            this.lblRij.Size = new System.Drawing.Size(37, 13);
            this.lblRij.TabIndex = 0;
            this.lblRij.Text = "Rijen: ";
            // 
            // lblKolom
            // 
            this.lblKolom.AutoSize = true;
            this.lblKolom.Location = new System.Drawing.Point(12, 35);
            this.lblKolom.Name = "lblKolom";
            this.lblKolom.Size = new System.Drawing.Size(62, 13);
            this.lblKolom.TabIndex = 1;
            this.lblKolom.Text = "Kolommen: ";
            // 
            // txtRij
            // 
            this.txtRij.Location = new System.Drawing.Point(80, 6);
            this.txtRij.Name = "txtRij";
            this.txtRij.Size = new System.Drawing.Size(100, 20);
            this.txtRij.TabIndex = 2;
            // 
            // txtKol
            // 
            this.txtKol.Location = new System.Drawing.Point(80, 32);
            this.txtKol.Name = "txtKol";
            this.txtKol.Size = new System.Drawing.Size(100, 20);
            this.txtKol.TabIndex = 3;
            // 
            // lblBereken
            // 
            this.lblBereken.Location = new System.Drawing.Point(306, 12);
            this.lblBereken.Name = "lblBereken";
            this.lblBereken.Size = new System.Drawing.Size(97, 23);
            this.lblBereken.TabIndex = 4;
            this.lblBereken.Text = "Bereken";
            this.lblBereken.UseVisualStyleBackColor = true;
            this.lblBereken.Click += new System.EventHandler(this.lblBereken_Click);
            // 
            // lbArray
            // 
            this.lbArray.FormattingEnabled = true;
            this.lbArray.Location = new System.Drawing.Point(12, 102);
            this.lbArray.Name = "lbArray";
            this.lbArray.Size = new System.Drawing.Size(391, 173);
            this.lbArray.TabIndex = 5;
            // 
            // txtGetal
            // 
            this.txtGetal.Location = new System.Drawing.Point(80, 58);
            this.txtGetal.Name = "txtGetal";
            this.txtGetal.Size = new System.Drawing.Size(100, 20);
            this.txtGetal.TabIndex = 7;
            // 
            // lblGetal
            // 
            this.lblGetal.AutoSize = true;
            this.lblGetal.Location = new System.Drawing.Point(12, 61);
            this.lblGetal.Name = "lblGetal";
            this.lblGetal.Size = new System.Drawing.Size(38, 13);
            this.lblGetal.TabIndex = 6;
            this.lblGetal.Text = "Getal: ";
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(306, 51);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(97, 23);
            this.btnSluiten.TabIndex = 8;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 287);
            this.Controls.Add(this.btnSluiten);
            this.Controls.Add(this.txtGetal);
            this.Controls.Add(this.lblGetal);
            this.Controls.Add(this.lbArray);
            this.Controls.Add(this.lblBereken);
            this.Controls.Add(this.txtKol);
            this.Controls.Add(this.txtRij);
            this.Controls.Add(this.lblKolom);
            this.Controls.Add(this.lblRij);
            this.Name = "Form1";
            this.Text = "Oef 14.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRij;
        private System.Windows.Forms.Label lblKolom;
        private System.Windows.Forms.TextBox txtRij;
        private System.Windows.Forms.TextBox txtKol;
        private System.Windows.Forms.Button lblBereken;
        private System.Windows.Forms.ListBox lbArray;
        private System.Windows.Forms.TextBox txtGetal;
        private System.Windows.Forms.Label lblGetal;
        private System.Windows.Forms.Button btnSluiten;
    }
}

