﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yahtzee
{
    class Dice
    {
        private int _eyes;
        private Random Random;

        public int Eyes
        {
            get { return _eyes; }
            set { _eyes = value; }
        }

        public EventHandler EyesChanged;
        public EventHandler KeepChanged;


        public Dice()
        {
            Random = new Random();
        }

        public void Roll()
        {
            Eyes = Random.Next(1, 7);                                                                                                                    
        }
    }
}
