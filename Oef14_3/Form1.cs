﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef14_3
{
    public partial class Form1 : Form
    {
        int[,] arrGetallen = new int[5, 3];
        int kleinste, grootste, som=0;
        int[] kleinsteIndex, grootsteIndex;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGenereren_Click(object sender, EventArgs e)
        {
            GenereerArray();
        }
        void GenereerArray()
        {
            Random random = new Random();
            for (int rij = 0; rij <= arrGetallen.GetUpperBound(0); rij++)
            {
                for (int kol = 0; kol < arrGetallen.GetUpperBound(1); kol++)
                {
                    arrGetallen[rij, kol] = random.Next(101);
                }
                arrGetallen[rij, 2] = arrGetallen[rij, 0] + arrGetallen[rij, 1];
            }

            StringBuilder output = new StringBuilder();
            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= arrGetallen.GetUpperBound(1); j++)
                {
                    output.Append(arrGetallen[i, j].ToString().PadRight(10));
                }
                output.Append(Environment.NewLine);
            }
            txtOutput.Text = output.ToString();

            BerekenKleinsteGrootsteSom();
            SchrijfTekst();
        }

        void BerekenKleinsteGrootsteSom()
        {
            kleinste = arrGetallen[0, 0];
            grootste = arrGetallen[0, 0];

            foreach (int getal in arrGetallen)
            {
                if (getal > grootste)
                    grootste = getal;
                if (getal < kleinste)
                    kleinste = getal;
                som += getal;
            }
        }

        private void SchrijfTekst()
        {
            StringBuilder tekst = new StringBuilder();
            tekst.Append($"De som van al deze getallen is 782.");
            tekst.Append(Environment.NewLine);
            tekst.Append(Environment.NewLine);

            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= arrGetallen.GetUpperBound(1); j++)
                {
                    if (arrGetallen[i, j] == kleinste)
                    {
                        tekst.Append($"Het kleinste getal is {kleinste}. Dit staat op rij {i} in kolom {j}." + Environment.NewLine);
                    }
                }
            }

            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= arrGetallen.GetUpperBound(1); j++)
                {
                    if (arrGetallen[i, j] == grootste)
                    {
                        tekst.Append($"Het grootste getal is {grootste}. Dit staat op rij {i} in kolom {j}."+ Environment.NewLine);
                    }
                }
            }

            txtTekst.Text = tekst.ToString();
        }
    }
}
