﻿namespace Oef14_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.btnGenereren = new System.Windows.Forms.Button();
            this.txtTekst = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Enabled = false;
            this.txtOutput.Location = new System.Drawing.Point(32, 78);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(331, 138);
            this.txtOutput.TabIndex = 5;
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(151, 32);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(75, 23);
            this.btnSluiten.TabIndex = 4;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // btnGenereren
            // 
            this.btnGenereren.Location = new System.Drawing.Point(32, 32);
            this.btnGenereren.Name = "btnGenereren";
            this.btnGenereren.Size = new System.Drawing.Size(75, 23);
            this.btnGenereren.TabIndex = 3;
            this.btnGenereren.Text = "Genereren";
            this.btnGenereren.UseVisualStyleBackColor = true;
            this.btnGenereren.Click += new System.EventHandler(this.btnGenereren_Click);
            // 
            // txtTekst
            // 
            this.txtTekst.Enabled = false;
            this.txtTekst.Location = new System.Drawing.Point(32, 254);
            this.txtTekst.Multiline = true;
            this.txtTekst.Name = "txtTekst";
            this.txtTekst.Size = new System.Drawing.Size(307, 97);
            this.txtTekst.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 377);
            this.Controls.Add(this.txtTekst);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.btnSluiten);
            this.Controls.Add(this.btnGenereren);
            this.Name = "Form1";
            this.Text = "           ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button btnSluiten;
        private System.Windows.Forms.Button btnGenereren;
        private System.Windows.Forms.TextBox txtTekst;
    }
}

