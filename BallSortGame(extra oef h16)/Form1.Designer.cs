﻿namespace BallSortGame_extra_oef_h16_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ccButtonOK1 = new BallSortGame_extra_oef_h16_.ccButtonOK();
            this.SuspendLayout();
            // 
            // ccButtonOK1
            // 
            this.ccButtonOK1.Location = new System.Drawing.Point(40, 32);
            this.ccButtonOK1.Name = "ccButtonOK1";
            this.ccButtonOK1.Size = new System.Drawing.Size(75, 23);
            this.ccButtonOK1.TabIndex = 0;
            this.ccButtonOK1.Text = "ccButtonOK1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ccButtonOK1);
            this.Name = "Form1";
            this.Text = "Ball sort game";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ccButtonOK ccButtonOK1;
    }
}

