﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef21_2
{

    class Persoon
    {
        // Attributen
        private int _leeftijd;
        private string _categorie;

        // Properties
        public string Categorie
        {
            get { return _categorie; }
            set { _categorie = value; }
        }

        public int Leeftijd
        {
            get { return _leeftijd; }
            set { _leeftijd = value; }
        }

        public event EventHandler LeeftijdGewijzigdEvent;
        public event EventHandler CategorieGewijzigdEvent;
        public event EventHandler<LeeftijdGewijzigdEventArgs> LeeftijdGewijzigdArgumentenEvent;


        protected virtual void OnLeeftijdGewijzigd(EventArgs e)
        {
            EventHandler handler = LeeftijdGewijzigdEvent;
            handler?.Invoke(this, e);
        }
        protected virtual void OnCategorieGewijzigd(EventArgs e)
        {

        }
        protected virtual void OnLeeftijdGewijzigdArgumenten(EventArgs e)
        {
            EventHandler handler = LeeftijdGewijzigdEvent;
            handler?.Invoke(this, e);
        }

    }
}
