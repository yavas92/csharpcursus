﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef18_3
{
    class Cirkel : Punt
    {
        // Attributen
        private double _r;


        // Properties

        public double R
        {
            get { return _r; }
            set { _r = value; }
        }


        // Constructor
        public Cirkel() : base()
        {

        }

        public Cirkel(double x, double y, double r) : base(x, y)
        {
            R = r;
        }

        // Methods
        public override string Gegevens()
        {
            return base.Gegevens() + $"Straal: {R}" + Environment.NewLine;
        }

        public double Omtrek()
        {
            return 2 * R * Math.PI;
        }

        public virtual double Oppervlakte()
        {
            return R * R * Math.PI;
        }

    }
}

