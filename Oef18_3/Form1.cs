﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef18_3
{
    public partial class Form1 : Form
    {
        Punt punt;
        Cirkel cirkel;
        Cilinder cilinder;

        public Form1()
        {
            InitializeComponent();

            punt = new Punt(3, 4);
            cirkel = new Cirkel(3, 4, 5);
            cilinder = new Cilinder(3, 4, 1, 2);
        }

        private void btnPunt_Click(object sender, EventArgs e)
        {
            MessageBox.Show(punt.Gegevens(), "Coördinaten van het punt");
        }

        private void btnCirkel_Click(object sender, EventArgs e)
        {
            MessageBox.Show(cirkel.Gegevens(), "Coördinaten van de cirkel");
        }

        private void btnCilinder_Click(object sender, EventArgs e)
        {
            MessageBox.Show(cilinder.Gegevens(), "Gegevens van de cilinder");
        }
    }
}
