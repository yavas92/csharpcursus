﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef17_2
{
    public partial class Form1 : Form
    {
        List<WinkelkarItem> winkelkar = new List<WinkelkarItem>();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnToevoegen_Click(object sender, EventArgs e)
        {
            if (int.TryParse(txtAantal.Text, out int x) && txtBenaming.Text != "" && double.TryParse(txtPrijs.Text, out double y))
            {
                winkelkar.Add(new WinkelkarItem(txtBenaming.Text, int.Parse(txtAantal.Text), double.Parse(txtPrijs.Text)));
            }

            txtAantal.Text = "";
            txtBenaming.Text = "";
            txtPrijs.Text = "";
        }

        private void btnTonen_Click(object sender, EventArgs e)
        {
            double totaalBedrag = 0;
            lblOutput.Text = "";


            foreach (WinkelkarItem item in winkelkar)
            {
                lblOutput.Text += item;
                totaalBedrag += item.TotalePrijs;
            }
            lblOutput.Text += "De totale winkelkar bedraagt:".PadRight(35) + (Math.Round(totaalBedrag*100)/100).ToString("0.00");
        }
    }
}
