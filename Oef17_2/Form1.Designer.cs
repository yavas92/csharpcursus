﻿namespace Oef17_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAantal = new System.Windows.Forms.Label();
            this.lblBenaming = new System.Windows.Forms.Label();
            this.lblPrijs = new System.Windows.Forms.Label();
            this.txtAantal = new System.Windows.Forms.TextBox();
            this.txtBenaming = new System.Windows.Forms.TextBox();
            this.txtPrijs = new System.Windows.Forms.TextBox();
            this.btnToevoegen = new System.Windows.Forms.Button();
            this.btnTonen = new System.Windows.Forms.Button();
            this.lblOutput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblAantal
            // 
            this.lblAantal.AutoSize = true;
            this.lblAantal.Location = new System.Drawing.Point(22, 24);
            this.lblAantal.Name = "lblAantal";
            this.lblAantal.Size = new System.Drawing.Size(40, 13);
            this.lblAantal.TabIndex = 0;
            this.lblAantal.Text = "Aantal:";
            // 
            // lblBenaming
            // 
            this.lblBenaming.AutoSize = true;
            this.lblBenaming.Location = new System.Drawing.Point(22, 52);
            this.lblBenaming.Name = "lblBenaming";
            this.lblBenaming.Size = new System.Drawing.Size(57, 13);
            this.lblBenaming.TabIndex = 0;
            this.lblBenaming.Text = "Benaming:";
            // 
            // lblPrijs
            // 
            this.lblPrijs.AutoSize = true;
            this.lblPrijs.Location = new System.Drawing.Point(22, 82);
            this.lblPrijs.Name = "lblPrijs";
            this.lblPrijs.Size = new System.Drawing.Size(29, 13);
            this.lblPrijs.TabIndex = 0;
            this.lblPrijs.Text = "Prijs:";
            // 
            // txtAantal
            // 
            this.txtAantal.Location = new System.Drawing.Point(124, 21);
            this.txtAantal.Name = "txtAantal";
            this.txtAantal.Size = new System.Drawing.Size(100, 20);
            this.txtAantal.TabIndex = 1;
            // 
            // txtBenaming
            // 
            this.txtBenaming.Location = new System.Drawing.Point(124, 49);
            this.txtBenaming.Name = "txtBenaming";
            this.txtBenaming.Size = new System.Drawing.Size(100, 20);
            this.txtBenaming.TabIndex = 2;
            // 
            // txtPrijs
            // 
            this.txtPrijs.Location = new System.Drawing.Point(124, 79);
            this.txtPrijs.Name = "txtPrijs";
            this.txtPrijs.Size = new System.Drawing.Size(100, 20);
            this.txtPrijs.TabIndex = 3;
            // 
            // btnToevoegen
            // 
            this.btnToevoegen.Location = new System.Drawing.Point(25, 118);
            this.btnToevoegen.Name = "btnToevoegen";
            this.btnToevoegen.Size = new System.Drawing.Size(355, 23);
            this.btnToevoegen.TabIndex = 4;
            this.btnToevoegen.Text = "Toevoegen aan Winkelkar";
            this.btnToevoegen.UseVisualStyleBackColor = true;
            this.btnToevoegen.Click += new System.EventHandler(this.btnToevoegen_Click);
            // 
            // btnTonen
            // 
            this.btnTonen.Location = new System.Drawing.Point(25, 147);
            this.btnTonen.Name = "btnTonen";
            this.btnTonen.Size = new System.Drawing.Size(355, 23);
            this.btnTonen.TabIndex = 5;
            this.btnTonen.Text = "Winkelkar tonen";
            this.btnTonen.UseVisualStyleBackColor = true;
            this.btnTonen.Click += new System.EventHandler(this.btnTonen_Click);
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.Location = new System.Drawing.Point(25, 188);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(0, 14);
            this.lblOutput.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 358);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.btnTonen);
            this.Controls.Add(this.btnToevoegen);
            this.Controls.Add(this.txtPrijs);
            this.Controls.Add(this.txtBenaming);
            this.Controls.Add(this.txtAantal);
            this.Controls.Add(this.lblPrijs);
            this.Controls.Add(this.lblBenaming);
            this.Controls.Add(this.lblAantal);
            this.Name = "Form1";
            this.Text = "Les 17: Winkelkar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAantal;
        private System.Windows.Forms.Label lblBenaming;
        private System.Windows.Forms.Label lblPrijs;
        private System.Windows.Forms.TextBox txtAantal;
        private System.Windows.Forms.TextBox txtBenaming;
        private System.Windows.Forms.TextBox txtPrijs;
        private System.Windows.Forms.Button btnToevoegen;
        private System.Windows.Forms.Button btnTonen;
        private System.Windows.Forms.Label lblOutput;
    }
}

