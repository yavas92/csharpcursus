﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef17_2
{
    class WinkelkarItem
    {
        // Attributen
        private string _benaming;
        private double _prijs;
        private int _aantal;


        // Properties
        public string Benaming
        {
            get { return _benaming; }
            set { _benaming = value; }
        }

        public double Prijs
        {
            get { return _prijs; }
            set { _prijs = value; }
        }

        public int Aantal
        {
            get { return _aantal; }
            set { _aantal = value; }
        }

        public double TotalePrijs
        {
            get
            {
                return Aantal * Prijs;
            }
        }


        // Constructor
        public WinkelkarItem()
        {

        }

        public WinkelkarItem(string benaming, int aantal, double prijs)
        {
            Benaming = benaming;
            Aantal = aantal;
            Prijs = prijs;
        }

        // Methods
        public string FormattedTotalePrijs()
        {
            return (Math.Round(TotalePrijs * 100) / 100).ToString("0.00");
        }

        public override string ToString()
        {
            return Aantal.ToString().PadRight(5) + "*".PadRight(5) + Benaming.PadRight(20) + ":".PadRight(5) + FormattedTotalePrijs() + Environment.NewLine;
        }
    }
}
