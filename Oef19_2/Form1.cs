﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef19_2
{
    public partial class Form1 : Form
    {
        List<string> zinnen = new List<string>();
        Wezen kat, papegaai, mens;


        public Form1()
        {
            InitializeComponent();

            zinnen.Add("Hallo");
            zinnen.Add("Goede morgen");
            zinnen.Add("Hoe gaat het?");

            lbZinnen.DataSource = zinnen;
        }

        private void btnStrelen_Click(object sender, EventArgs e)
        {
            if (lbZinnen.SelectedIndex > -1)
            {
                if (rdbKat.Checked && kat != null)
                    MessageBox.Show(kat.Strelen());
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
                if (rdbPapegaai.Checked && papegaai != null)
                    MessageBox.Show(papegaai.Strelen());
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
                if (rdbMens.Checked && mens != null)
                    MessageBox.Show(mens.Strelen());
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
            }
        }

        private void btnEten_Click(object sender, EventArgs e)
        {
            if (lbZinnen.SelectedIndex > -1)
            {
                if (rdbKat.Checked && kat != null)
                    MessageBox.Show(kat.Eten());
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
                if (rdbPapegaai.Checked && papegaai != null)
                    MessageBox.Show(papegaai.Eten());
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
                if (rdbMens.Checked && mens != null)
                    MessageBox.Show(mens.Eten());
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
            }
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEquals_Click(object sender, EventArgs e)
        {
            Wezen w1 = new Mens("Mens1");
            Wezen w2 = new Mens("Mens2");
            Wezen w3 = new Kat("Mens1");
            Wezen w4 = new Mens("Mens1");

            MessageBox.Show("w1 (mens) - Naam: Mens1" + Environment.NewLine + "w2 (mens) - Naam: Mens2" + Environment.NewLine + "Equals: " + w1.Equals(w2));

            MessageBox.Show("w1 (mens) - Naam: Mens1" + Environment.NewLine + "w3 (kat) - Naam: Mens1" + Environment.NewLine + "Equals: " + w1.Equals(w3));

            MessageBox.Show("w1 (mens) - Naam: Mens1" + Environment.NewLine + "w4 (mens) - Naam: Mens1" + Environment.NewLine + "Equals: " + w1.Equals(w4));

        }

        private void btnAanmaken_Click(object sender, EventArgs e)
        {
            if (txtNaam.Text != "")
            {
                if (rdbKat.Checked)
                    kat = new Kat(txtNaam.Text);

                if (rdbPapegaai.Checked)
                    papegaai = new Papegaai(txtNaam.Text);
                if (rdbMens.Checked)
                    mens = new Mens(txtNaam.Text);
            }

            txtNaam.Text = "";
            rdbKat.Checked = true;
        }

        private void btnPraten_Click(object sender, EventArgs e)
        {

            if (lbZinnen.SelectedIndex > -1)
            {
                if (rdbKat.Checked && kat != null)
                    MessageBox.Show(kat.Praten(lbZinnen.SelectedItem.ToString()));
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
                if (rdbPapegaai.Checked && papegaai!=null)
                    MessageBox.Show(papegaai.Praten(lbZinnen.SelectedItem.ToString()));
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
                if (rdbMens.Checked && mens != null)
                    MessageBox.Show(mens.Praten(lbZinnen.SelectedItem.ToString()));
                else
                    MessageBox.Show("Er is geen type van dit wezen aangemaakt.");
            }
        }
    }
}
