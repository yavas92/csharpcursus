﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef19_2
{
    class Kat : Wezen
    {
        private int _teller = 1;
        public Kat(string naam) : base(naam)
        {

        }

        public override string Praten(string zin)
        {
            if (_teller == 3)
            {
                _teller = 1;
                return "Miauw.";
            }
            _teller++;

            return "";
        }

        public override string Strelen()
        {
            return "RrrRrr.";
        }
    }
}
