﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef17_3
{
    class Kanaal
    {
        // Attributen
        private int _nummer;
        private string _naam;


        // Properties

        public int Nummer
        {
            get { return _nummer; }
            set
            {
                if (value > 0)
                    _nummer = value;
            }
        }


        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }

        // Constructor
        public Kanaal()
        {

        }

        public Kanaal(int nummer, string naam)
        {
            Nummer = nummer;
            Naam = naam;
        }

        // Methods



    }
}
