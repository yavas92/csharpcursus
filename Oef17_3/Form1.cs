﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef17_3
{
    public partial class Form1 : Form
    {
        TV tv = new TV();
        List<Kanaal> zenders = new List<Kanaal>();


        public Form1()
        {
            InitializeComponent();
        }

        private void btnVermeerder_Click(object sender, EventArgs e)
        {
            tv.VermeerderKanaal();
            tv.VermeerderVolume();
            UpdateOutput();
        }

        void UpdateOutput()
        {
            txtOutput.Text = tv.ToonGegevens();
        }

        private void btnVerminder_Click(object sender, EventArgs e)
        {
            tv.VerminderKanaal();
            tv.VerminderVolume();
            UpdateOutput();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateOutput();
            zenders.Add(new Kanaal(1, "VRT"));
            zenders.Add(new Kanaal(2, "CANVAS"));
            zenders.Add(new Kanaal(3, "VTM"));
            zenders.Add(new Kanaal(4, "VT4"));
            zenders.Add(new Kanaal(5, "2BE"));

            foreach (Kanaal kanaal in zenders)
            {
                cbZenderNamen.Items.Add(kanaal.Naam);
            }
        }

        private void cbZenderNamen_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblNummer.Text = zenders[cbZenderNamen.SelectedIndex].Nummer.ToString();
        }
    }
}
