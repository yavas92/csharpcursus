﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef17_3
{
    class TV
    {
        // Attributen
        private int _kanaal;
        private int _volume;

        // Properties
        public int Kanaal
        {
            get { return _kanaal; }
            set
            {
                if (value <= 30 && value>=0)
                    _kanaal = value;
            }
        }

        public int Volume
        {
            get { return _volume; }
            set
            {
                if (value <= 10 && value>=0)
                    _volume = value;
            }
        }

        // Constructors
        public TV()
        {

        }

        // Methods
        public string ToonGegevens()
        {
            return $"kanaal: {Kanaal} - volume: {Volume}";
        }

        public void VermeerderKanaal()
        {
            Kanaal++;
        }

        public void VerminderKanaal()
        {
            Kanaal--;
        }

        public void VermeerderVolume()
        {
            Volume++;
        }

        public void VerminderVolume()
        {
            Volume--;
        }
    }
}
