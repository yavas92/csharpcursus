﻿namespace Oef17_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutput = new System.Windows.Forms.Label();
            this.btnVermeerder = new System.Windows.Forms.Button();
            this.btnVerminder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbZenderNamen = new System.Windows.Forms.ComboBox();
            this.lblNummer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.AutoSize = true;
            this.txtOutput.Font = new System.Drawing.Font("Courier New", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutput.Location = new System.Drawing.Point(37, 33);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(62, 18);
            this.txtOutput.TabIndex = 0;
            this.txtOutput.Text = "label1";
            // 
            // btnVermeerder
            // 
            this.btnVermeerder.Location = new System.Drawing.Point(40, 78);
            this.btnVermeerder.Name = "btnVermeerder";
            this.btnVermeerder.Size = new System.Drawing.Size(196, 50);
            this.btnVermeerder.TabIndex = 1;
            this.btnVermeerder.Text = "Vermeerder";
            this.btnVermeerder.UseVisualStyleBackColor = true;
            this.btnVermeerder.Click += new System.EventHandler(this.btnVermeerder_Click);
            // 
            // btnVerminder
            // 
            this.btnVerminder.Location = new System.Drawing.Point(40, 134);
            this.btnVerminder.Name = "btnVerminder";
            this.btnVerminder.Size = new System.Drawing.Size(196, 50);
            this.btnVerminder.TabIndex = 2;
            this.btnVerminder.Text = "Verminder";
            this.btnVerminder.UseVisualStyleBackColor = true;
            this.btnVerminder.Click += new System.EventHandler(this.btnVerminder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Naam van het kanaal:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 269);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nummer van het kanaal:";
            // 
            // cbZenderNamen
            // 
            this.cbZenderNamen.FormattingEnabled = true;
            this.cbZenderNamen.Location = new System.Drawing.Point(155, 212);
            this.cbZenderNamen.Name = "cbZenderNamen";
            this.cbZenderNamen.Size = new System.Drawing.Size(81, 21);
            this.cbZenderNamen.TabIndex = 5;
            this.cbZenderNamen.SelectedIndexChanged += new System.EventHandler(this.cbZenderNamen_SelectedIndexChanged);
            // 
            // lblNummer
            // 
            this.lblNummer.AutoSize = true;
            this.lblNummer.Location = new System.Drawing.Point(166, 269);
            this.lblNummer.Name = "lblNummer";
            this.lblNummer.Size = new System.Drawing.Size(0, 13);
            this.lblNummer.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 335);
            this.Controls.Add(this.lblNummer);
            this.Controls.Add(this.cbZenderNamen);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnVerminder);
            this.Controls.Add(this.btnVermeerder);
            this.Controls.Add(this.txtOutput);
            this.Name = "Form1";
            this.Text = "Oef 17.3 - TV (uitbreiding 15.2)";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtOutput;
        private System.Windows.Forms.Button btnVermeerder;
        private System.Windows.Forms.Button btnVerminder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbZenderNamen;
        private System.Windows.Forms.Label lblNummer;
    }
}

