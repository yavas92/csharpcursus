﻿namespace Oef16_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLezenStudent = new System.Windows.Forms.Button();
            this.btnLezenPunten = new System.Windows.Forms.Button();
            this.btnGeslaagd = new System.Windows.Forms.Button();
            this.btnWis = new System.Windows.Forms.Button();
            this.lbOutput = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnLezenStudent
            // 
            this.btnLezenStudent.Location = new System.Drawing.Point(453, 158);
            this.btnLezenStudent.Name = "btnLezenStudent";
            this.btnLezenStudent.Size = new System.Drawing.Size(160, 41);
            this.btnLezenStudent.TabIndex = 1;
            this.btnLezenStudent.Text = "Lezen Student.txt + Overzicht";
            this.btnLezenStudent.UseVisualStyleBackColor = true;
            this.btnLezenStudent.Click += new System.EventHandler(this.btnLezenStudent_Click);
            // 
            // btnLezenPunten
            // 
            this.btnLezenPunten.Location = new System.Drawing.Point(453, 205);
            this.btnLezenPunten.Name = "btnLezenPunten";
            this.btnLezenPunten.Size = new System.Drawing.Size(160, 41);
            this.btnLezenPunten.TabIndex = 1;
            this.btnLezenPunten.Text = "Lezen Punten.txt + Overzicht";
            this.btnLezenPunten.UseVisualStyleBackColor = true;
            this.btnLezenPunten.Click += new System.EventHandler(this.btnLezenPunten_Click);
            // 
            // btnGeslaagd
            // 
            this.btnGeslaagd.Location = new System.Drawing.Point(453, 252);
            this.btnGeslaagd.Name = "btnGeslaagd";
            this.btnGeslaagd.Size = new System.Drawing.Size(160, 41);
            this.btnGeslaagd.TabIndex = 1;
            this.btnGeslaagd.Text = "Geslaagd / Niet geslaagd";
            this.btnGeslaagd.UseVisualStyleBackColor = true;
            this.btnGeslaagd.Click += new System.EventHandler(this.btnGeslaagd_Click);
            // 
            // btnWis
            // 
            this.btnWis.Location = new System.Drawing.Point(453, 299);
            this.btnWis.Name = "btnWis";
            this.btnWis.Size = new System.Drawing.Size(160, 41);
            this.btnWis.TabIndex = 1;
            this.btnWis.Text = "Wis Overzicht";
            this.btnWis.UseVisualStyleBackColor = true;
            this.btnWis.Click += new System.EventHandler(this.btnWis_Click);
            // 
            // lbOutput
            // 
            this.lbOutput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOutput.FormattingEnabled = true;
            this.lbOutput.ItemHeight = 14;
            this.lbOutput.Location = new System.Drawing.Point(13, 13);
            this.lbOutput.Name = "lbOutput";
            this.lbOutput.Size = new System.Drawing.Size(408, 508);
            this.lbOutput.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 529);
            this.Controls.Add(this.lbOutput);
            this.Controls.Add(this.btnWis);
            this.Controls.Add(this.btnGeslaagd);
            this.Controls.Add(this.btnLezenPunten);
            this.Controls.Add(this.btnLezenStudent);
            this.Name = "Form1";
            this.Text = "Oefening met List";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLezenStudent;
        private System.Windows.Forms.Button btnLezenPunten;
        private System.Windows.Forms.Button btnGeslaagd;
        private System.Windows.Forms.Button btnWis;
        private System.Windows.Forms.ListBox lbOutput;
    }
}

