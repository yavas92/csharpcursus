﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef16_1
{
    public partial class Form1 : Form
    {
        List<string> studenten = new List<string>();
        List<int> punten = new List<int>();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLezenStudent_Click(object sender, EventArgs e)
        {
            string bestand = @"C:\Users\Abdullah\Documents\Visual Studio 2019\Projects\CevoraCursus\Oef16_1\Studenten.txt";

            if (studenten.Count == 0)
            {
                if (File.Exists(bestand))
                {
                    using (StreamReader reader = new StreamReader(bestand))
                    {
                        while (!reader.EndOfStream)
                        {
                            studenten.Add(reader.ReadLine());
                        }
                    }
                }
                else
                    MessageBox.Show("Bestand niet gevonden!");
            }
            foreach (string student in studenten)
            {
                lbOutput.Items.Add(student);
            }
        }

        private void btnLezenPunten_Click(object sender, EventArgs e)
        {
            string bestand = @"C:\Users\Abdullah\Documents\Visual Studio 2019\Projects\CevoraCursus\Oef16_1\Punten.txt";

            if (punten.Count == 0)
            {
                if (File.Exists(bestand) && punten.Count == 0)
                {
                    using (StreamReader reader = new StreamReader(bestand))
                    {
                        while (!reader.EndOfStream)
                        {
                            punten.Add(Convert.ToInt32(reader.ReadLine()));
                        }
                    }
                }
                else
                    MessageBox.Show("Bestand niet gevonden!");
            }

            foreach (int punt in punten)
            {
                lbOutput.Items.Add(punt);
            }
        }

        private void btnGeslaagd_Click(object sender, EventArgs e)
        {
            string item;
            int aantalGeslaagd = 0, aantalNietGeslaagd = 0;
            for (int i = 0; i < studenten.Count; i++)
            {
                item = studenten[i].PadRight(30) + punten[i].ToString().PadRight(10);
                if (punten[i] > 50)
                {
                    item += "Geslaagd".PadRight(15);
                    aantalGeslaagd++;
                }
                else
                {
                    item += "Niet geslaagd".PadRight(15);
                    aantalNietGeslaagd++;
                }
                lbOutput.Items.Add(item);

            }

            lbOutput.Items.Add($"Aantal geslaagd: {aantalGeslaagd}");
            lbOutput.Items.Add($"Aantal niet-geslaagd: {aantalNietGeslaagd}");

        }

        private void btnWis_Click(object sender, EventArgs e)
        {
            lbOutput.Items.Clear();
        }
    }
}
