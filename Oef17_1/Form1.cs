﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef17_1
{
    public partial class Form1 : Form
    {
        List<Cursist> cursisten = new List<Cursist>();
        public Form1()
        {
            InitializeComponent();


        }

        private void btnToevoegen_Click(object sender, EventArgs e)
        {
            if (txtVoornaam.Text != "" && txtFamilienaam.Text != "")
            {
                cursisten.Add(new Cursist(txtFamilienaam.Text, txtVoornaam.Text));
            }
            else
            {
                MessageBox.Show("Voornaam en familienaam moeten ingevuld worden.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            ResetListbox();
            txtFamilienaam.Text = "";
            txtVoornaam.Text = "";
        }


        private void ResetListbox()
        {
            lbOutput.Items.Clear();
            foreach (Cursist cursist in cursisten)
            {
                lbOutput.Items.Add(cursist.Naam);
            }
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnVerwijderen_Click(object sender, EventArgs e)
        {
            if (lbOutput.SelectedIndex != -1)
            {
                if (MessageBox.Show($"Wil je {lbOutput.SelectedItem} verwijderen?", "Verwijderen", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    cursisten.RemoveAt(lbOutput.SelectedIndex);
                    ResetListbox();
                }
            }
        }

        private void lbOutput_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtVoornaam.Text = cursisten[lbOutput.SelectedIndex].Voornaam;
            txtFamilienaam.Text = cursisten[lbOutput.SelectedIndex].Familienaam;
        }
    }
}
