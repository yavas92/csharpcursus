﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef17_1
{
    class Cursist
    {
        // Attributen
        private string _familienaam;
        private string _voornaam;

        // Properties
        public string Familienaam
        {
            get { return _familienaam; }
            set { _familienaam = value; }
        }

        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }

        public string Naam
        {
            get
            {
                return $"{Voornaam} {Familienaam}";
            }
        }

        // Constructors
        public Cursist()
        {

        }

        public Cursist(string familienaam, string voornaam)
        {
            Familienaam = familienaam;
            Voornaam = voornaam;
        }

        // Methods

    }
}
