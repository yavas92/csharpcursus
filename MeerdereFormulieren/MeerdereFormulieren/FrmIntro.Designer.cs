﻿namespace MeerdereFormulieren
{
    partial class FrmIntro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTekening2 = new System.Windows.Forms.Button();
            this.btnTekening = new System.Windows.Forms.Button();
            this.btnVraag = new System.Windows.Forms.Button();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnTekening2
            // 
            this.btnTekening2.Location = new System.Drawing.Point(251, 119);
            this.btnTekening2.Margin = new System.Windows.Forms.Padding(4);
            this.btnTekening2.Name = "btnTekening2";
            this.btnTekening2.Size = new System.Drawing.Size(128, 39);
            this.btnTekening2.TabIndex = 15;
            this.btnTekening2.Text = "Tekening2";
            this.btnTekening2.Click += new System.EventHandler(this.btnTekening2_Click);
            // 
            // btnTekening
            // 
            this.btnTekening.Location = new System.Drawing.Point(251, 70);
            this.btnTekening.Margin = new System.Windows.Forms.Padding(4);
            this.btnTekening.Name = "btnTekening";
            this.btnTekening.Size = new System.Drawing.Size(128, 39);
            this.btnTekening.TabIndex = 14;
            this.btnTekening.Text = "Tekening";
            this.btnTekening.Click += new System.EventHandler(this.btnTekening_Click);
            // 
            // btnVraag
            // 
            this.btnVraag.Location = new System.Drawing.Point(251, 168);
            this.btnVraag.Margin = new System.Windows.Forms.Padding(4);
            this.btnVraag.Name = "btnVraag";
            this.btnVraag.Size = new System.Drawing.Size(128, 39);
            this.btnVraag.TabIndex = 13;
            this.btnVraag.Text = "Vraag";
            this.btnVraag.Click += new System.EventHandler(this.btnVraag_Click);
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(38, 40);
            this.txtNaam.Margin = new System.Windows.Forms.Padding(4);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(191, 22);
            this.txtNaam.TabIndex = 12;
            // 
            // FrmIntro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 311);
            this.Controls.Add(this.btnTekening2);
            this.Controls.Add(this.btnTekening);
            this.Controls.Add(this.btnVraag);
            this.Controls.Add(this.txtNaam);
            this.Name = "FrmIntro";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnTekening2;
        internal System.Windows.Forms.Button btnTekening;
        internal System.Windows.Forms.Button btnVraag;
        internal System.Windows.Forms.TextBox txtNaam;
    }
}

