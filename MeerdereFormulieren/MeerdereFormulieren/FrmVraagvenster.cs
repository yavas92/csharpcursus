﻿using System;
using System.Windows.Forms;

namespace MeerdereFormulieren
{
    public partial class FrmVraagvenster : Form
    {
        public FrmVraagvenster()
        {
            InitializeComponent();
        }

        private void btnAccepteer_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnAnnuleer_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void FrmVraagvenster_Load(object sender, EventArgs e)
        {
            //Kan je zowel hier in code doen als in de properties van je formulier:
            this.AcceptButton = btnAccepteer;
            this.CancelButton = btnAnnuleer;
        }

        //Propertie die eigen is aan FrmVraagvenster:
        public string InvoerTekst
        {
            get { return txtInvoer.Text; }
            set { txtInvoer.Text = value; }
        }

    }
}
