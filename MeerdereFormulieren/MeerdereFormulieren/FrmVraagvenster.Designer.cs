﻿namespace MeerdereFormulieren
{
    partial class FrmVraagvenster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnnuleer = new System.Windows.Forms.Button();
            this.btnAccepteer = new System.Windows.Forms.Button();
            this.txtInvoer = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAnnuleer
            // 
            this.btnAnnuleer.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnnuleer.Location = new System.Drawing.Point(281, 62);
            this.btnAnnuleer.Margin = new System.Windows.Forms.Padding(4);
            this.btnAnnuleer.Name = "btnAnnuleer";
            this.btnAnnuleer.Size = new System.Drawing.Size(96, 49);
            this.btnAnnuleer.TabIndex = 11;
            this.btnAnnuleer.Text = "Annuleer";
            this.btnAnnuleer.Click += new System.EventHandler(this.btnAnnuleer_Click);
            // 
            // btnAccepteer
            // 
            this.btnAccepteer.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAccepteer.Location = new System.Drawing.Point(36, 62);
            this.btnAccepteer.Margin = new System.Windows.Forms.Padding(4);
            this.btnAccepteer.Name = "btnAccepteer";
            this.btnAccepteer.Size = new System.Drawing.Size(96, 49);
            this.btnAccepteer.TabIndex = 10;
            this.btnAccepteer.Text = "Accepteer";
            this.btnAccepteer.Click += new System.EventHandler(this.btnAccepteer_Click);
            // 
            // txtInvoer
            // 
            this.txtInvoer.Location = new System.Drawing.Point(25, 23);
            this.txtInvoer.Margin = new System.Windows.Forms.Padding(4);
            this.txtInvoer.Name = "txtInvoer";
            this.txtInvoer.Size = new System.Drawing.Size(361, 22);
            this.txtInvoer.TabIndex = 9;
            // 
            // FrmVraagvenster
            // 
            this.AcceptButton = this.btnAccepteer;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 180);
            this.Controls.Add(this.btnAnnuleer);
            this.Controls.Add(this.btnAccepteer);
            this.Controls.Add(this.txtInvoer);
            this.Name = "FrmVraagvenster";
            this.Text = "FrmVraagvenster";
            this.Load += new System.EventHandler(this.FrmVraagvenster_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnAnnuleer;
        internal System.Windows.Forms.Button btnAccepteer;
        internal System.Windows.Forms.TextBox txtInvoer;
    }
}