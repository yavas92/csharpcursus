﻿using System;
using System.Windows.Forms;

namespace MeerdereFormulieren
{
    public partial class FrmIntro : Form
    {
        public FrmIntro()
        {
            InitializeComponent();
        }

        private void btnTekening_Click(object sender, EventArgs e)
        {
            FrmTekening tekeningvenster1 = new FrmTekening(); //object maken van sjabloon, hierdoor krijg je telkens een nieuw venster
            tekeningvenster1.Show(); // zonder dit is het aangemaakt, maar nog niet zichtbaar
            //na this.close krijg je eigenlijk tekeningvenster1 = null; Het wordt weggegooid.
        }

        private void btnTekening2_Click(object sender, EventArgs e)
        {
            FrmTekening tekeningvenster2 = new FrmTekening();
            tekeningvenster2.Text += " OwnedForm"; // titel aanpassen
            this.AddOwnedForm(tekeningvenster2); // als ik FrmIntro minimaliseer, dan ook het nieuwe formulier
            tekeningvenster2.Show();
        }

        private void btnVraag_Click(object sender, EventArgs e)
        {
            // show dialog = wacht op een antwoord
            FrmVraagvenster vraagvenster = new FrmVraagvenster();
            vraagvenster.InvoerTekst = "Zet hier je naam";
            if (vraagvenster.ShowDialog() == DialogResult.OK)
            {
                txtNaam.Text = vraagvenster.InvoerTekst; // instellen in properties (public)
            }
            else
            {
                txtNaam.Text = "invoer geannuleerd";
            }
        }
    }
}
