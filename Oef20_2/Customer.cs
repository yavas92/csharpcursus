﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef20_2
{
    class Customer
    {
        // Variabelen
        private string _address;
        private string _city;
        private int _customerNumber;
        private string _name;
        private string _state;
        private string _zipcode;

        //Properties
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public int CustomerNumber
        {
            get { return _customerNumber; }
            set { _customerNumber = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string Zipcode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        // Constructor
        public Customer() { }
        public Customer(int customerNumber, string name, string address, string city, string state, string zipcode)
        {
            CustomerNumber = customerNumber;
            Name = name;
            Address = address;
            City = city;
            State = state;
            Zipcode = zipcode;
        }

        // Methods
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return $"{Name}" + Environment.NewLine +
                $"{Address}" + Environment.NewLine +
                $"{City}, {State} {Zipcode}";
        }
    }
}
