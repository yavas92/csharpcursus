﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef20_2
{
    public partial class Form1 : Form
    {
        List<Customer> customers = new List<Customer>();

        public Form1()
        {
            InitializeComponent();
            GetCustomers();
        }

        private Customer GetCustomerByNumber(int customerNumber)
        {
            foreach (Customer customer in customers)
            {
                if (customer.CustomerNumber == customerNumber)
                    return customer;
            }

            throw new NoSuchCustomerException(customerNumber);
        }

        private void GetCustomers()
        {
            string path = @"C:\Users\Abdullah\Documents\Visual Studio 2019\Projects\CevoraCursus\Oef20_2\Les 20 - Customers.txt";
            string gegeven;
            string[] gegevenSplit;

            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    gegeven = sr.ReadLine();
                    gegevenSplit = gegeven.Split(';');
                    customers.Add(new Customer(int.Parse(gegevenSplit[0]), gegevenSplit[1], gegevenSplit[2], gegevenSplit[3], gegevenSplit[4], gegevenSplit[5]));
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnZoek_Click(object sender, EventArgs e)
        {
            try
            {
                lblCustomerDetails.Text = GetCustomerByNumber(int.Parse(txtCustomerNumber.Text)).ToString();

            }
            catch(NoSuchCustomerException nsce)
            {
                lblCustomerDetails.Text = $"The customer {txtCustomerNumber.Text} does not exist.";
            }
        }
    }
}
