﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef20_2
{
    class NoSuchCustomerException : Exception
    {
        public NoSuchCustomerException(int number):base($"The customer number {number} does not exist.")
        {

        }
    }
}
