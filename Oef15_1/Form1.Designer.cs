﻿namespace Oef15_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLezen = new System.Windows.Forms.Button();
            this.btnVerhoog = new System.Windows.Forms.Button();
            this.btnVerminder = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnEinde = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLezen
            // 
            this.btnLezen.Location = new System.Drawing.Point(12, 12);
            this.btnLezen.Name = "btnLezen";
            this.btnLezen.Size = new System.Drawing.Size(235, 23);
            this.btnLezen.TabIndex = 0;
            this.btnLezen.Text = "Lezen waarde teller";
            this.btnLezen.UseVisualStyleBackColor = true;
            this.btnLezen.Click += new System.EventHandler(this.btnLezen_Click);
            // 
            // btnVerhoog
            // 
            this.btnVerhoog.Location = new System.Drawing.Point(12, 41);
            this.btnVerhoog.Name = "btnVerhoog";
            this.btnVerhoog.Size = new System.Drawing.Size(235, 23);
            this.btnVerhoog.TabIndex = 1;
            this.btnVerhoog.Text = "Verhoog teller met 1";
            this.btnVerhoog.UseVisualStyleBackColor = true;
            this.btnVerhoog.Click += new System.EventHandler(this.btnVerhoog_Click);
            // 
            // btnVerminder
            // 
            this.btnVerminder.Location = new System.Drawing.Point(12, 70);
            this.btnVerminder.Name = "btnVerminder";
            this.btnVerminder.Size = new System.Drawing.Size(235, 23);
            this.btnVerminder.TabIndex = 2;
            this.btnVerminder.Text = "Verminder teller met 1";
            this.btnVerminder.UseVisualStyleBackColor = true;
            this.btnVerminder.Click += new System.EventHandler(this.btnVerminder_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(12, 99);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(235, 23);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Reset tellen naar 0";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnEinde
            // 
            this.btnEinde.Location = new System.Drawing.Point(12, 128);
            this.btnEinde.Name = "btnEinde";
            this.btnEinde.Size = new System.Drawing.Size(235, 23);
            this.btnEinde.TabIndex = 4;
            this.btnEinde.Text = "Einde";
            this.btnEinde.UseVisualStyleBackColor = true;
            this.btnEinde.Click += new System.EventHandler(this.btnEinde_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 172);
            this.Controls.Add(this.btnEinde);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnVerminder);
            this.Controls.Add(this.btnVerhoog);
            this.Controls.Add(this.btnLezen);
            this.Name = "Form1";
            this.Text = "Teller";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLezen;
        private System.Windows.Forms.Button btnVerhoog;
        private System.Windows.Forms.Button btnVerminder;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnEinde;
    }
}

