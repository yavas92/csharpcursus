﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_1
{
    public partial class Form1 : Form
    {
        Teller teller = new Teller();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLezen_Click(object sender, EventArgs e)
        {
            teller.LeesUit();
        }

        private void btnVerhoog_Click(object sender, EventArgs e)
        {
            teller.Verhoog();
        }

        private void btnVerminder_Click(object sender, EventArgs e)
        {
            teller.Verlaag();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            teller.Resetten();
        }

        private void btnEinde_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
