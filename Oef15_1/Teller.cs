﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_1
{
    class Teller
    {
        // Variables
        private int _tel;

        // Properties
        public int Tel
        {
            get { return _tel; }
        }

        // Constructors (geen in dit geval?)
        public Teller()
        {
            _tel = 0;
        }
        // Methods
        public void Resetten()
        {
            _tel = 0;
        }

        public void Verhoog()
        {
            _tel++;
        }

        public void Verlaag()
        {
            _tel--;
        }

        public void LeesUit()
        {
            MessageBox.Show(Tel.ToString(), "Waarde teller", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
