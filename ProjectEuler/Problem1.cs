﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectEuler
{
    class Problem1
    {
        public static void Run()
        {
            int sum = 0;
            int maxNumber = 1000;

            Console.WriteLine($"Natural numbers below {maxNumber} that are multiples of 3 or 5: ");
            for (int i = 0; i < maxNumber; i++)
            {
                if (i % 3 == 0 || i%5==0)
                {
                    Console.Write($"{i} ");
                    sum += i;
                }
            }
            Console.WriteLine(Environment.NewLine + $"The sum of these multiples is: {sum}");
        }
    }
}
