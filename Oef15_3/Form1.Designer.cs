﻿namespace Oef15_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNaam = new System.Windows.Forms.Label();
            this.lblWiskunde = new System.Windows.Forms.Label();
            this.lblInformatica = new System.Windows.Forms.Label();
            this.lblRapport = new System.Windows.Forms.Label();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.txtWiskunde = new System.Windows.Forms.TextBox();
            this.txtInformatica = new System.Windows.Forms.TextBox();
            this.txtRapport = new System.Windows.Forms.TextBox();
            this.btnToonRapport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNaam
            // 
            this.lblNaam.AutoSize = true;
            this.lblNaam.Location = new System.Drawing.Point(22, 19);
            this.lblNaam.Name = "lblNaam";
            this.lblNaam.Size = new System.Drawing.Size(38, 13);
            this.lblNaam.TabIndex = 0;
            this.lblNaam.Text = "Naam:";
            // 
            // lblWiskunde
            // 
            this.lblWiskunde.AutoSize = true;
            this.lblWiskunde.Location = new System.Drawing.Point(22, 45);
            this.lblWiskunde.Name = "lblWiskunde";
            this.lblWiskunde.Size = new System.Drawing.Size(58, 13);
            this.lblWiskunde.TabIndex = 1;
            this.lblWiskunde.Text = "Wiskunde:";
            // 
            // lblInformatica
            // 
            this.lblInformatica.AutoSize = true;
            this.lblInformatica.Location = new System.Drawing.Point(22, 71);
            this.lblInformatica.Name = "lblInformatica";
            this.lblInformatica.Size = new System.Drawing.Size(62, 13);
            this.lblInformatica.TabIndex = 1;
            this.lblInformatica.Text = "Informatica:";
            // 
            // lblRapport
            // 
            this.lblRapport.AutoSize = true;
            this.lblRapport.Location = new System.Drawing.Point(22, 132);
            this.lblRapport.Name = "lblRapport";
            this.lblRapport.Size = new System.Drawing.Size(48, 13);
            this.lblRapport.TabIndex = 1;
            this.lblRapport.Text = "Rapport:";
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(95, 16);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(100, 20);
            this.txtNaam.TabIndex = 2;
            // 
            // txtWiskunde
            // 
            this.txtWiskunde.Location = new System.Drawing.Point(95, 42);
            this.txtWiskunde.Name = "txtWiskunde";
            this.txtWiskunde.Size = new System.Drawing.Size(100, 20);
            this.txtWiskunde.TabIndex = 2;
            // 
            // txtInformatica
            // 
            this.txtInformatica.Location = new System.Drawing.Point(95, 68);
            this.txtInformatica.Name = "txtInformatica";
            this.txtInformatica.Size = new System.Drawing.Size(100, 20);
            this.txtInformatica.TabIndex = 2;
            // 
            // txtRapport
            // 
            this.txtRapport.Location = new System.Drawing.Point(95, 132);
            this.txtRapport.Multiline = true;
            this.txtRapport.Name = "txtRapport";
            this.txtRapport.Size = new System.Drawing.Size(343, 60);
            this.txtRapport.TabIndex = 2;
            // 
            // btnToonRapport
            // 
            this.btnToonRapport.Location = new System.Drawing.Point(264, 19);
            this.btnToonRapport.Name = "btnToonRapport";
            this.btnToonRapport.Size = new System.Drawing.Size(174, 69);
            this.btnToonRapport.TabIndex = 3;
            this.btnToonRapport.Text = "Toon Rapport";
            this.btnToonRapport.UseVisualStyleBackColor = true;
            this.btnToonRapport.Click += new System.EventHandler(this.btnToonRapport_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 212);
            this.Controls.Add(this.btnToonRapport);
            this.Controls.Add(this.txtRapport);
            this.Controls.Add(this.txtInformatica);
            this.Controls.Add(this.txtWiskunde);
            this.Controls.Add(this.txtNaam);
            this.Controls.Add(this.lblRapport);
            this.Controls.Add(this.lblInformatica);
            this.Controls.Add(this.lblWiskunde);
            this.Controls.Add(this.lblNaam);
            this.Name = "Form1";
            this.Text = "Oef 15.3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNaam;
        private System.Windows.Forms.Label lblWiskunde;
        private System.Windows.Forms.Label lblInformatica;
        private System.Windows.Forms.Label lblRapport;
        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.TextBox txtWiskunde;
        private System.Windows.Forms.TextBox txtInformatica;
        private System.Windows.Forms.TextBox txtRapport;
        private System.Windows.Forms.Button btnToonRapport;
    }
}

