﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef15_3
{
    class Student
    {
        // Variabelen
        private string _naam;
        private double _wiskunde;
        private double _informatica;

        // Properties

        public string Naam
        {
            get { return _naam; }
            set
            {
                if (!int.TryParse(value, out int result))
                    _naam = value.ToUpper();
            }
        }

        public double Wiskunde
        {
            get { return _wiskunde; }
            set
            {
                if (0 <= value && value <= 20)
                    _wiskunde = value;
            }
        }

        public double Informatica
        {
            get { return _informatica; }
            set
            {
                if (0 <= value && value <= 20)
                    _informatica = value;
            }
        }

        // Constructor
        public Student()
        {

        }

        // Methods
        public string ToonGegevens()
        {
            return $"{Naam} heeft {Wiskunde} voor wiskunde en {Informatica} voor informatica behaald.";
        }
    }
}
