﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_3
{
    public partial class Form1 : Form
    {
        Student student = new Student();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnToonRapport_Click(object sender, EventArgs e)
        {
            student.Naam = txtNaam.Text;
            student.Wiskunde = Convert.ToDouble(txtWiskunde.Text);
            student.Informatica = Convert.ToDouble(txtInformatica.Text);

            txtRapport.Text = student.ToonGegevens();
        }
    }
}
