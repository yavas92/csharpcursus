﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CevoraCursus
{
    class Oefening8_1
    {
        public static void Run()
        {
            string[] namen = new string[20] { "Arthur", "Noah", "Adam", "Louis", "Liam", "Lucas", "Jules", "Victor", "Gabriel", "Mohamed", "Emma", "Olivia", "Louise", "Mila", "Alice", "Juliette", "Elena", "Marie", "Sofia", "Lina" };
            int aantalStudenten, aantalVakken = 4, teller = 0;
            string[] studenten = new string[] { };
            int[] pNederlands, pWiskunde, pGeschiedenis, pMuziek;
            double[] gemiddelde;
            string inhoud = null, bestandNaam;
            Random random = new Random();

            Console.WriteLine("Hoeveel studenten moet u ingeven?");
            aantalStudenten = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            // Instancieren van de arrays
            pNederlands = new int[aantalStudenten];
            pWiskunde = new int[aantalStudenten];
            pGeschiedenis = new int[aantalStudenten];
            pMuziek = new int[aantalStudenten];
            gemiddelde = new double[aantalStudenten];
            studenten = new string[aantalStudenten];

            namen.OrderBy(x => random.Next()).ToArray();

            do
            {
                studenten[teller] = namen[teller];
                pNederlands[teller] = random.Next(101);
                pWiskunde[teller] = random.Next(101);
                pGeschiedenis[teller] = random.Next(101);
                pMuziek[teller] = random.Next(101);
                gemiddelde[teller] = (pNederlands[teller] + pWiskunde[teller] + pGeschiedenis[teller] + pMuziek[teller]) / aantalVakken;

            } while (++teller < aantalStudenten);

            inhoud += "Nederlands".PadLeft(40) + "Wiskunde".PadLeft(20) + "Geschiedenis".PadLeft(20) + "Muziek".PadLeft(20) + Environment.NewLine + Environment.NewLine;


            for (int i = 0; i < aantalStudenten; i++)
            {
                inhoud += studenten[i].PadLeft(20) + pNederlands[i].ToString().PadLeft(20) + pWiskunde[i].ToString().PadLeft(20) + pGeschiedenis[i].ToString().PadLeft(20) + pMuziek[i].ToString().PadLeft(20) + Environment.NewLine;
            }

            inhoud += Environment.NewLine + Environment.NewLine + "Per vak is..." + Environment.NewLine;

            inhoud += "de hoogste score:".PadLeft(20) + pNederlands.Max().ToString().PadLeft(20) + pWiskunde.Max().ToString().PadLeft(20) + pGeschiedenis.Max().ToString().PadLeft(20) + pMuziek.Max().ToString().PadLeft(20) + Environment.NewLine;

            inhoud += "de laagste score:".PadLeft(20) + pNederlands.Min().ToString().PadLeft(20) + pWiskunde.Min().ToString().PadLeft(20) + pGeschiedenis.Min().ToString().PadLeft(20) + pMuziek.Min().ToString().PadLeft(20) + Environment.NewLine;

            inhoud += "de gemiddelde:".PadLeft(20) + pNederlands.Average().ToString().PadLeft(20) + pWiskunde.Average().ToString().PadLeft(20) + pGeschiedenis.Average().ToString().PadLeft(20) + pMuziek.Average().ToString().PadLeft(20) + Environment.NewLine;

            // We gaan de arrays hier sorteren; om de mediaan te selecteren; aangezien de punten al afgedrukt zijn
            Array.Sort(pNederlands);
            Array.Sort(pWiskunde);
            Array.Sort(pGeschiedenis);
            Array.Sort(pMuziek);
            Array.Sort(gemiddelde);

            inhoud += "de mediaan:".PadLeft(20) + pNederlands[pNederlands.Length / 2 - 1].ToString().PadLeft(20) + pWiskunde[pWiskunde.Length / 2 - 1].ToString().PadLeft(20) + pGeschiedenis[pGeschiedenis.Length / 2 - 1].ToString().PadLeft(20) + pMuziek[pMuziek.Length / 2 - 1].ToString().PadLeft(20) + Environment.NewLine + Environment.NewLine + Environment.NewLine;

            inhoud += "Van de klas is ...".PadLeft(20) + Environment.NewLine;
            inhoud += "de hoogste score:".PadLeft(20) + gemiddelde.Max().ToString().PadLeft(20) + Environment.NewLine;
            inhoud += "de laagste score:".PadLeft(20) + gemiddelde.Min().ToString().PadLeft(20) + Environment.NewLine;
            inhoud += "het gemiddelde:".PadLeft(20) + gemiddelde.Average().ToString().PadLeft(20) + Environment.NewLine;
            inhoud += "de mediaan:".PadLeft(20) + gemiddelde[gemiddelde.Length / 2 - 1].ToString().PadLeft(20) + Environment.NewLine;

            Console.WriteLine(inhoud);

            if (File.Exists(@"C:\Users\Abdullah\Desktop\GegevensSchooljaar2020.txt"))
            {
                Console.Write("Deze bestandsnaam bestaat al.");
            }
            else
            {
                using (StreamWriter writer = new StreamWriter(@"C:\Users\Abdullah\Desktop\GegevensSchooljaar2020.txt"))
                {
                    Console.WriteLine("Uw bestand \"GegevensSchooljaar2020.txt\" is aangemaakt.");
                    writer.Write(inhoud);
                    Console.WriteLine($"Rapport is weggeschreven naar het bestand.");
                }
            }
        }
    }
    class Oefening8_2
    {
        public static void Run()
        {
            string inhoud;
            if (File.Exists(@"C:\Users\Abdullah\Desktop\GegevensSchooljaar2020.txt"))
            {
                StreamReader streamReader = new StreamReader(@"C:\Users\Abdullah\Desktop\GegevensSchooljaar2020.txt");
                inhoud = streamReader.ReadToEnd();
                streamReader.Close();
                Console.WriteLine(inhoud);
            } else
            {
                Console.WriteLine("Bestand bestaat niet.");
            }
        }
    }
}
