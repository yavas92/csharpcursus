﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace CevoraCursus
{
    class Oefening4_1_1
    {
        public static void Run()
        {
            string jaNee;
            Console.WriteLine("De rookmelder heeft rook gedetecteerd in de wasplaats. Is er iemand in huis?");
            jaNee = Console.ReadLine();

            if (jaNee == "ja")
            {
                Console.WriteLine("Heeft u de situatie onder controle?");
                jaNee = Console.ReadLine();
                if (jaNee == "ja")
                    Console.WriteLine("Het alarm is uitgeschakeld.");
                else
                    Console.WriteLine("De brandweer wordt verwittigd.");
            }
            else
            {
                Console.WriteLine("Zal ik de brandweer voor u verwittigen?");
                jaNee = Console.ReadLine();
                if (jaNee == "ja")
                    Console.WriteLine("De brandweer wordt verwittigd.");
                else
                    Console.WriteLine("Het alarm is uitgeschakeld.");
            }
        }
    }
    class Oefening4_1_2
    {
        public static void Run()
        {
            string jaNee;
            int dagen;
            Console.WriteLine("De krant heeft een nieuwe zomeractie die 3 maanden geldig zal zijn. Als je binnen de 7 dagen ingaat op deze actie, zal je de krant ontvangen in de zomer met 25% korting.");
            Console.WriteLine("Wil jij je inschrijven voor de zomeractie?");
            jaNee = Console.ReadLine();

            if (jaNee == "ja")
            {
                Console.WriteLine("Hoeveel dagen zijn er reeds voorbij gegaan?");
                dagen = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"U heeft zich ingeschreven voor een zomerabonnement. Dit was {(dagen <= 7 ? "wel" : "niet")} binnen de tijd. U hoeft {(dagen <= 7 ? 20 * 0.75 : 20)} euro te betalen om de bestelling af te ronden.");
            }
            else
            {
                Console.WriteLine("Jammer dat je geen bestelling wilt doen. Toch bedankt.");
            }
        }
    }
    class Oefening4_1_3
    {
        public static void Run()
        {
            string[] jaNee = new string[2];
            Console.WriteLine("Heb je vandaag iets gedaan waar je trots op bent?");
            jaNee[0] = Console.ReadLine();
            Console.WriteLine("Heb je vandaag met iets kunnen lachen?");
            jaNee[1] = Console.ReadLine();
        }
    }
    class Oefening4_1_4
    {
        public static void Run()
        {
            string[] jaNee = new string[4];
            int aantalJa = 0;

            Console.WriteLine("Heb je vandaag iets gedaan waar je trots op bent?");
            jaNee[0] = Console.ReadLine();

            Console.WriteLine("Heb je vandaag met iets kunnen lachen?");
            jaNee[1] = Console.ReadLine();

            Console.WriteLine("Heeft het zonnetje vandaag geschenen?");
            jaNee[2] = Console.ReadLine();

            Console.WriteLine("Kijk je uit naar morgen ?");
            jaNee[3] = Console.ReadLine();

            for (int i = 0; i < jaNee.Length; i++)
            {
                if (jaNee[i] == "ja")
                    aantalJa++;
            }

            if (aantalJa >= 3)
                Console.WriteLine("Super! Positieve mensen laden ons op.");
            else
                Console.WriteLine("Er is niets mis met een dagje down. Vraag gerust aan je docent om een mopje te vertellen.");
        }
    }
    class Oefening4_1_5
    {
        public static void Run()
        {
            string naam = "Abdullah";
            int[] punten = new int[4];
            int gemiddelde = 0;
            Random random = new Random();

            for (int i = 0; i < punten.Length; i++)
            {
                punten[i] = random.Next(0, 100);
                gemiddelde += punten[i];
            }
            gemiddelde /= punten.Length;

            Console.WriteLine($"Rapport van {naam}");
            Console.WriteLine("-------------------------");
            Console.WriteLine($"Nederlands: {punten[0]} / 100");
            Console.WriteLine($"Frans: {punten[1]} / 100");
            Console.WriteLine($"Engels: {punten[2]} / 100");
            Console.WriteLine($"Wiskunde: {punten[3]} / 100");
            Console.WriteLine();

            if (gemiddelde > 49)
                Console.WriteLine("Proficiat. Je bent geslaagd.");
            else
                Console.WriteLine("Je bent niet geslaagd.");
        }
    }
    class Oefening4_1_6
    {
        public static void Run()
        {
            // geen opgave
        }
    }
    class Oefening4_1_7
    {
        public static void Run()
        {
            string jaNee;
            string[] tekst = new string[4] {
                "Heeft u de situatie onder controle?",
                "Zal ik de brandweer voor u verwittigen?",
                "Het alarm wordt uitgeschakeld.",
                "De brandweer zal verwittigd worden."};

            Console.WriteLine("De rookmelder heeft rook gedetecteerd in de wasplaats. Is er iemand in huis?");
            jaNee = Console.ReadLine();

            if (jaNee == "ja")
            {
                Console.WriteLine(tekst[0]);
                jaNee = Console.ReadLine();
                if (jaNee == "ja")
                    Console.WriteLine(tekst[2]);
                else
                    Console.WriteLine(tekst[3]);
            }
            else
            {
                Console.WriteLine(tekst[1]);
                jaNee = Console.ReadLine();
                if (jaNee == "ja")
                    Console.WriteLine(tekst[3]);
                else
                    Console.WriteLine(tekst[2]);
            }
        }
    }

    class Oefening4_2_1
    {
        public static void Run()
        {
            Console.WriteLine("Goedemorgen. Dit is je huisrobot: Welke dag is het?");
            switch (Console.ReadLine().ToLower())
            {
                case "maandag":
                    Console.WriteLine("Eerste werkdag van je week. Succes!");
                    break;
                case "dinsdag":
                    Console.WriteLine("Je hebt afgesproken met je neef om te gaan eten.");
                    break;
                case "woensdag":
                    Console.WriteLine("De week gaat snel. Vandaag heb je training!");
                    break;
                case "donderdag":
                    Console.WriteLine("We zijn dichter bij de weekend! Vergeet niet dat je een afspraak hebt bij de tandarts.");
                    break;
                case "vrijdag":
                    Console.WriteLine("Laatste werkdag, rustig afbollen tot 16u. :D");
                    break;
                case "zaterdag":
                    Console.WriteLine("Zonnig weer, veel plezier met je picknick!");
                    break;
                case "zondag":
                    Console.WriteLine("Vandaag staat op de planning: Pizza en Netflix!");
                    break;
                default:
                    Console.WriteLine("Je hebt geen dag ingegeven.");
                    break;
            }
        }
    }
    class Oefening4_2_2
    {
        public static void Run()
        {
            string maand;
            Console.WriteLine("Welke maand is het?");
            maand = Console.ReadLine().ToLower();

            if (maand == "januari")
                Console.WriteLine("Het is de maand januari. Nieuwjaar is net voorbij.");
            if (maand == "februari")
                Console.WriteLine("Het is de maand februari. De winter is bijna voorbij.");
            if (maand == "maart")
                Console.WriteLine("Het is de maand maart. De lente zal hier starten.");
            if (maand == "april")
                Console.WriteLine("Het is de maand april.");
            if (maand == "mei")
                Console.WriteLine("Het is de maand mei.");
            if (maand == "juni")
                Console.WriteLine("Het is de maand juni.");
            if (maand == "juli")
                Console.WriteLine("Het is de maand juli.");
            if (maand == "augustus")
                Console.WriteLine("Het is de maand augustus.");
            if (maand == "september")
                Console.WriteLine("Het is de maand september.");
            if (maand == "oktober")
                Console.WriteLine("Het is de maand oktober.");
            if (maand == "november")
                Console.WriteLine("Het is de maand november.");
            if (maand == "december")
                Console.WriteLine("Het is de maand december.");

            Console.WriteLine();
            switch (maand)
            {
                case "januari":
                    Console.WriteLine("Het is de maand januari. Nieuwjaar is net voorbij.");
                    break;
                case "februari":
                    Console.WriteLine("Het is de maand februari. De winter is bijna voorbij.");
                    break;
                case "maart":
                    Console.WriteLine("Het is de maand maart. De lente zal hier starten.");
                    break;
                case "april":
                    Console.WriteLine("Het is de maand april.");
                    break;
                case "mei":
                    Console.WriteLine("Het is de maand mei.");
                    break;
                case "juni":
                    Console.WriteLine("Het is de maand juni.");
                    break;
                case "juli":
                    Console.WriteLine("Het is de maand juli.");
                    break;
                case "augustus":
                    Console.WriteLine("Het is de maand augustus.");
                    break;
                case "september":
                    Console.WriteLine("Het is de maand september.");
                    break;
                case "oktober":
                    Console.WriteLine("Het is de maand oktober.");
                    break;
                case "november":
                    Console.WriteLine("Het is de maand november.");
                    break;
                case "december":
                    Console.WriteLine("Het is de maand december.");
                    break;
                default:
                    Console.WriteLine("Dit is geen maand!");
                    break;
            }
        }
    }

    class Oefening4_3_1
    {
        public static void Run()
        {
            int length = 5, getal, som = 0;
            for (int i = 0; i < length; i++)
            {
                Console.WriteLine($"Geef het {i + 1}e getal in: ");
                getal = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"Je hebt het getal {getal} ingegeven.");
                som += getal;
            }
            Console.WriteLine($"De som van alle ingevoerde getallen bedraagt {som}.");
        }
    }
    class Oefening4_3_2
    {
        public static void Run()
        {
            int a = 12, b = 4, c = 2, d;
            Console.WriteLine("Geef een geheel getal in tussen 1 en 4: ");
            d = Convert.ToInt32(Console.ReadLine());

            switch (d)
            {
                case 1:
                    Console.WriteLine(a + c);
                    break;
                case 2:
                    Console.WriteLine(a - b);
                    break;
                case 3:
                    Console.WriteLine(b * b * b);
                    break;
                case 4:
                    Console.WriteLine(a * b * c);
                    break;
            }
        }
    }
    class Oefening4_3_3 // Overgeslaan
    {
        public static void Run()
        {
            double percentage, geld;
            int jaar;

            Console.WriteLine("Wat is het aflossingspercentage?");
            percentage = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoeveel geld wil je lenen?");
            geld = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoeveel jaren wil je aflossen?");
            jaar = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine($"Je hebt {geld} geleend aan {percentage}% voor {jaar} aantal jaren.");

            for (int i = 1; i <= jaar; i++)
            {
                Console.WriteLine($"In jaar {i + 1} gaat er {1} euro van je geleende bedrag. Je hoeft nu nog {1} euro af te lossen over {jaar - i} aantal jaar.");
            }

            // AFWERKEN
        }
    }
    class Oefening4_3_4
    {
        public static void Run()
        {
            double pFrisdrank = 2, pBier = 2.5, pCava = 4.95, pAndere = 1.75;
            double[] tafelPrijs = new double[3] { 0, 0, 0 };

            for (int i = 0; i < tafelPrijs.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine($"Drank tafel {i + 1} persoon {j + 1}: ");
                    switch (Console.ReadLine().ToLower())
                    {
                        case "frisdrank":
                            tafelPrijs[i] += pFrisdrank;
                            break;
                        case "bier":
                            tafelPrijs[i] += pBier;
                            break;
                        case "cava":
                            tafelPrijs[i] += pCava;
                            break;
                        default:
                            tafelPrijs[i] += pAndere;
                            break;
                    }
                }
            }
            Console.WriteLine();

            for (int i = 0; i < tafelPrijs.Length; i++)
            {
                Console.WriteLine($"De totale prijs van tafel {i + 1} bedraagt {tafelPrijs[i]} euro.");
            }
        }
    }
    class Oefening4_3_5
    {
        public static void Run()
        {
            int fib1 = 0, fib2 = 1;
            Console.Write($"{fib1} {fib2} ");
            for (int i = 2; i < 10; i++)
            {
                int getal = fib1;
                fib1 = fib2;
                fib2 = getal + fib1;
                Console.Write($"{fib2} ");
            }
        }
    }

    class Oefening4_4_1
    {
        public static void Run()
        {
            int aantalMunten = 0;

            Console.WriteLine("Is je spaarpot leeg?");
            while (Console.ReadLine().ToLower() == "nee")
            {
                aantalMunten++;
                Console.WriteLine("Is je spaarpot leeg?");
            }
            Console.WriteLine($"Je had {aantalMunten} aantal munten in je spaarpot zitten.");
        }
    }
    class Oefening4_4_2
    {
        public static void Run()
        {
            int aantalMunten = 1;

            Console.WriteLine("Is dit een vreemd munt?");
            while (Console.ReadLine().ToLower() == "nee")
            {
                aantalMunten++;
                Console.WriteLine("Is dit een vreemd munt?");
            }
            Console.WriteLine($"Je hebt {aantalMunten} aantal munten moeten nemen tot je de vreemde munt bent tegengekomen.");
        }
    }
    class Oefening4_4_3
    {
        public static void Run()
        {

        }
    }
    class Oefening4_4_4
    {
        public static void Run()
        {
            Random random = new Random();
            int getal = random.Next(10), raadgetal, teller = 1;

            Console.WriteLine("Raad het getal tussen 0 en 10: ");
            raadgetal = Convert.ToInt32(Console.ReadLine());

            while (raadgetal != getal)
            {
                if (raadgetal < getal)
                    Console.WriteLine("Jammer. Je mag wat hoger raden.");
                else
                    Console.WriteLine("Jammer. Je mag wat lager raden.");
                raadgetal = Convert.ToInt32(Console.ReadLine());
                teller++;
            }
            Console.WriteLine($"Proficiat! Je had het juist in {teller} keer.");
        }
    }
    class Oefening4_4_5
    {
        public static void Run()
        {
            int[] getallen;
            int aantal, teller = 0, productVijfde = 0, somNegatief = 0;

            Console.WriteLine("Hoeveel getallen wil je inlezen?");
            aantal = Convert.ToInt32(Console.ReadLine());
            getallen = new int[aantal];

            while (teller < aantal)
            {
                getallen[teller] = Convert.ToInt32(Console.ReadLine());
                if ((teller + 1) % 5 == 0)
                {
                    if (productVijfde == 0)
                        productVijfde = getallen[teller];
                    else
                        productVijfde *= getallen[teller];
                }
                if (getallen[teller] < 0)
                    somNegatief += getallen[teller];
                teller++;
            }

            Console.WriteLine();
            Console.Write("Getallen deelbaar door 2 met restwaarde 0: ");
            for (int i = 0; i < aantal; i++)
            {
                {
                    if (getallen[i] % 2 == 0)
                    {
                        Console.Write(getallen[i] + " ");
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Het product van elk 5de getal: {productVijfde}");
            Console.WriteLine($"De som van alle negatieve getallen: {somNegatief}");
        }
    }
    class Oefening4_4_6 // Overgeslaan
    {
        public static void Run()
        {

        }
    }

    class Oefening4_5_1
    {
        public static void Run()
        {
            int fib1 = 0, fib2 = 1;
            Console.Write($"{fib1} {fib2} ");

            do
            {
                int getal = fib1;
                fib1 = fib2;
                fib2 = fib1 + getal;
                Console.Write(fib2 + " ");
            } while ((fib1 + fib2) < 1000);
            Console.WriteLine();
        }
    }
    class Oefening4_5_2
    {
        public static void Run()
        {
            int aantalStudenten, aantalVakken = 4, teller = 0;
            string[] studenten;
            double[,] punten;
            double[] gemiddelde;
            double klasgemiddelde = 0;

            Console.WriteLine("Hoeveel studenten moet u ingeven?");
            aantalStudenten = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            // Instancieren van de arrays
            punten = new double[aantalStudenten, aantalVakken];
            gemiddelde = new double[aantalStudenten];
            studenten = new string[aantalStudenten];

            do
            {
                Console.Write("Naam student: ");
                studenten[teller] = Console.ReadLine();
                Console.WriteLine();

                Console.WriteLine($"Geef de punten in van {studenten[teller]} (%):");
                for (int i = 0; i < aantalVakken; i++)
                {
                    switch (i)
                    {
                        case 0:
                            Console.Write("Nederlands: ");
                            break;
                        case 1:
                            Console.Write("Frans: ");
                            break;
                        case 2:
                            Console.Write("Engels: ");
                            break;
                        case 3:
                            Console.Write("Wiskunde: ");
                            break;
                        default:
                            break;
                    }
                    punten[teller, i] = Convert.ToInt32(Console.ReadLine()); // Leest punt in per vak
                    gemiddelde[teller] += punten[teller, i]; // Telt de punten per vak op zodat we de gemiddelde kunnen berekenen
                }
                gemiddelde[teller] /= aantalVakken; // Deelt de laatste index door het aantal vakken, zo bekomen we de gemiddelde
                klasgemiddelde += gemiddelde[teller]; // Telt de gemiddelden van de studenten op

                Console.WriteLine();
            } while (++teller < aantalStudenten);

            klasgemiddelde /= aantalStudenten;



            teller = 0;

            do
            {
                Console.WriteLine($"Punten van {studenten[teller]} :");

                for (int i = 0; i < aantalVakken; i++)
                {
                    switch (i)
                    {
                        case 0:
                            Console.Write("Nederlands: ");
                            break;
                        case 1:
                            Console.Write("Frans: ");
                            break;
                        case 2:
                            Console.Write("Engels: ");
                            break;
                        case 3:
                            Console.Write("Wiskunde: ");
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine($"{punten[teller, i]}%");
                }
                Console.WriteLine($"Gemiddelde: {gemiddelde[teller]}%");
                Console.WriteLine();
            } while (++teller < aantalStudenten);

            Console.WriteLine($"Klasgemiddelde: {klasgemiddelde}%");

            Array.Sort(gemiddelde); // Array sorteren voor de mediaan te selecteren
            Console.WriteLine($"Mediaan: {gemiddelde[gemiddelde.Length/2-1]}%");
        }
    }
    class Oefening4_5_3 // Overgeslaan
    {
        public static void Run()
        {

        }
    }
}
