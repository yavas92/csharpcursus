﻿using System;

namespace CevoraCursus
{
    class Program
    {
        static void Main(string[] args)
        {
            RunGame.Run();

            Console.ReadKey();
        }
    }
}
