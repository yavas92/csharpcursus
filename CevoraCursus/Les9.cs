﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CevoraCursus
{
    class Oefening9_1
    {
        public static void Run()
        {
            Console.WriteLine("Voer een getal in tussen 1 en 10: ");
            Console.WriteLine(Foutmelding(Console.ReadLine()));
        }

        static string Foutmelding(string input)
        {
            string output = "Uw invoer is niet correct!";
            int getal;
            if (int.TryParse(input, out getal))
            {
                if (getal >= 1 && getal <= 10)
                    output = "U heeft getal " + getal + " ingevoerd.";
            }
            return output;
        }
    }
    class Oefening9_2
    {
        public static void Run()
        {
            string resultaat = "";
            int som = 0, product = 0, quotient = 0, verschil = 0;

            resultaat = "De volgende optellingen hebben als som: ";
            resultaat += TelOp(5, 10);
            resultaat += TelOp(7, 0);
            resultaat += TelOp(1, 4);
            resultaat += TelOp(6, 3);
            resultaat += Environment.NewLine + "De volgende vermenigvuldigingen hebben als product: ";
            resultaat += Vermenigvuldig(2, 3);
            resultaat += Vermenigvuldig(6, 3);
            resultaat += Environment.NewLine + "De volgende delingen hebben als quotiënt: ";
            resultaat += Deel(8, 2);
            resultaat += Deel(21, 3);
            resultaat += Environment.NewLine + "De volgende aftrekkingen hebben als verschil: ";
            resultaat += TrekAf(20, 8);
            resultaat += TrekAf(3, 6);
            resultaat += Environment.NewLine + "Dit zijn de berekeningen adhv BerekenAlles():";
            BerekenAlles(1, 2, 3, 4, 5, 6, 7, 8, ref som, ref product, ref quotient, ref verschil);
            resultaat += Environment.NewLine + $"Som: {som}";
            resultaat += Environment.NewLine + $"Product: {product}";
            resultaat += Environment.NewLine + $"Quotient: {quotient}";
            resultaat += Environment.NewLine + $"Verschil: {verschil}";

            Console.WriteLine(resultaat);
        }

        private static string TrekAf(int getal1, int getal2)
        {
            return $"{Environment.NewLine}{getal1} + {getal2} = {getal1 - getal2}";
        }

        private static string Deel(int getal1, int getal2)
        {
            return $"{Environment.NewLine}{getal1} / {getal2} = {getal1 / getal2}";
        }

        private static string Vermenigvuldig(int getal1, int getal2)
        {
            return $"{Environment.NewLine}{getal1} x {getal2} = {getal1 * getal2}";
        }

        private static string TelOp(int getal1, int getal2)
        {
            return $"{Environment.NewLine}{getal1} + {getal2} = {getal1 + getal2}";
        }

        private static void BerekenAlles(int getal1, int getal2, int getal3, int getal4, int getal5, int getal6, int getal7, int getal8, ref int som, ref int product, ref int quotient, ref int verschil)
        {
            som = getal1 + getal2;
            product = getal3 * getal4;
            quotient = getal5 / getal6;
            verschil = getal7 - getal8;
        }
    }
    class Oefening9_3
    {
        public static void Run()
        {
            int getal;

            Console.WriteLine("Faculteitberekening:");
            getal = Convert.ToInt32(Console.ReadLine());

            Faculteit(getal);
        }

        private static void Faculteit(int getal)
        {
            string output = "";
            int faculteit = getal;
            output += getal.ToString().PadRight(5) + "= " + getal.ToString();

            for (int i = getal - 1; i > 0; i--)
            {
                faculteit *= i;
                output += $" * {i}";
            }
            output += Environment.NewLine + "".PadRight(5) + "= " + faculteit;
            Console.WriteLine(output);
        }
    }
    class Oefening9_4
    {
        public static void Run()
        {
            string btwNummer;
            Console.WriteLine("Geef uw BTW-nummer in: ");
            btwNummer = Console.ReadLine();

            if(ControleBTWNummer(btwNummer))
                Console.WriteLine("Geldige BTW-nummer.");
            else
                Console.WriteLine("Ongeldige BTW-nummer!");
        }

        private static bool ControleBTWNummer(string btwNummer)
        {
            btwNummer = btwNummer.Trim(new char[] { ' ', '.', '-' });
            // knipt spaties, puntjes en koppeltekens weg
            int eersteZevenCijfers = Convert.ToInt32(btwNummer.Substring(3, 7));
            int laatsteTweeCijfers = Convert.ToInt32(btwNummer.Substring(10, 2));

            return 97 - (eersteZevenCijfers % 97) == laatsteTweeCijfers;
        }
    }
}
