﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CevoraCursus
{

    class Oefening7_1_1
    {
        public static void Run()
        {

        }
    }
    class Oefening7_1_2
    {
        public static void Run()
        {

        }
    }
    class Oefening7_1_3
    {
        public static void Run()
        {

        }
    }
    class Oefening7_1_4
    {
        public static void Run()
        {
            string naam;
            DateTime geboortedatum;

            Console.WriteLine("Wat is je naam?");
            naam = Console.ReadLine();
            Console.WriteLine("Wat is je geboortedatum? (vb: 20-01-1992)");
            while (!DateTime.TryParse(Console.ReadLine(), out geboortedatum))
            {
                Console.WriteLine("Geboortedatum is niet correct ingegeven. Probeer opnieuw!");
            }
            Console.WriteLine(Environment.NewLine + "Hallo " + naam + ", u bent " + (DateTime.Now - geboortedatum).Days + " dagen aan het rondlopen op planeet Aarde.");
        }
    }
    class Oefening7_1_5 // Niet afgewerkt
    {
        public static void Run()
        {
            string naam;
            DateTime geboortedatum;
            DateTime[] arrVandaagPlusZesDagen = new DateTime[7];
            string[] arrAfspraken = new string[7];

            Console.WriteLine("Wat is je naam?");
            naam = Console.ReadLine();
            Console.WriteLine("Wat is je geboortedatum? (vb: 20-01-1992)");
            while (!DateTime.TryParse(Console.ReadLine(), out geboortedatum))
            {
                Console.WriteLine("Geboortedatum is niet correct ingegeven. Probeer opnieuw!");
            }

            for (int i = 0; i < arrVandaagPlusZesDagen.Length; i++)
            {
                arrVandaagPlusZesDagen[i] = DateTime.Now.AddDays(i);
                Console.WriteLine(arrVandaagPlusZesDagen[i]);
            }

            Console.WriteLine("Wil je een afspraak inplannen?");
            while (Console.ReadLine().ToLower() == "ja")
            {
                Console.WriteLine("Voor welke dag wil je een afspraak maken?");
                switch (Console.ReadLine())
                {
                    case "maandag": 
                    case "dinsdag":
                    case "woensdag":
                    case "donderdag":
                    case "vrijdag":
                    case "zaterdag":
                    case "zondag":
                    default: break;
                }
                Console.WriteLine();
            }

        }
    }

    class Oefening7_2_1
    {
        public static void Run()
        {
            string woord, zin, enkeleZinnen;

            Console.WriteLine("Geef een woord in:");
            woord = Console.ReadLine();
            Console.WriteLine("Geef een zin in:");
            zin = Console.ReadLine();
            Console.WriteLine("Geef enkele zinnen in:");
            enkeleZinnen = Console.ReadLine();

            woord = woord.ToUpper();
            zin = zin.ToUpper();
            enkeleZinnen = enkeleZinnen.ToUpper();

            Console.WriteLine(woord + Environment.NewLine + zin + Environment.NewLine + enkeleZinnen);
        }
    }
    class Oefening7_2_2
    {
        public static void Run()
        {
            string woord, zin, enkeleZinnen;
            string tweedeWoord, tweedeZin;

            Console.WriteLine("Geef een woord in:");
            woord = Console.ReadLine();
            Console.WriteLine("Geef een zin in:");
            zin = Console.ReadLine();
            Console.WriteLine("Geef enkele zinnen in:");
            enkeleZinnen = Console.ReadLine();

            tweedeWoord = zin.Split(' ')[1];
            Console.WriteLine($"Tweede woord: {tweedeWoord}");
            tweedeZin = enkeleZinnen.Substring(enkeleZinnen.IndexOf('.') + 2);
            Console.WriteLine("Dit is de tweede zin: " + tweedeZin.Substring(0, tweedeZin.IndexOf('.') + 1));
        }
    }
    class Oefening7_2_3
    {
        public static void Run()
        {
            string woord, zin, enkeleZinnen;
            string tweedeWoord = null, tweedeZin = null;

            Console.WriteLine("Geef een woord in:");
            woord = Console.ReadLine();
            Console.WriteLine("Geef een zin in:");
            zin = Console.ReadLine();
            Console.WriteLine("Geef enkele zinnen in:");
            enkeleZinnen = Console.ReadLine();
            if (zin.Contains(' '))
                tweedeWoord = zin.Split(' ')[1];

            Console.WriteLine($"Tweede woord: {tweedeWoord}");
            tweedeZin = enkeleZinnen.Substring(enkeleZinnen.IndexOf('.') + 2);
            Console.WriteLine("Dit is de tweede zin: " + tweedeZin.Substring(0, tweedeZin.IndexOf('.') + 1));

            if (!(woord.Contains('.') && woord.Contains('@')))
                Console.WriteLine("Woord bevat geen emailadres!");
        }
    }
    class Oefening7_2_4
    {
        public static void Run()
        {

        }
    }
    class Oefening7_2_5
    {
        public static void Run()
        {

        }
    }
}
