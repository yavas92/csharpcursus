﻿using System;
using System.Collections.Generic;
using System.Text;


namespace CevoraCursus
{
    class Oefening2_1
    {
        public static void Run()
        {
            int a, b, c, d;
            a = 5 + 7;
            b = 14 - 5;
            c = 2 * 3;
            d = 8 / 4;

            Console.WriteLine($"{a}{b}{c}{d}");
            Console.WriteLine();
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);
        }
    }

    class Oefening2_2
    {
        public static void Run(double value = 18.24, int round = 100)
        {
            double e = Math.Round(value % 5 * round) / round;
            Console.WriteLine(e);
            e++;
            Console.WriteLine(e);
            e--;
            Console.WriteLine(e);
            e++;
            Console.WriteLine(e);
        }
    }


    class Oefening2_3
    {
        public static void Run(double value = 4.36)
        {
            int a = 4;
            double e = value;

            a = (int)e;
            e = (double)a;

            Console.WriteLine(a);
            Console.WriteLine(e);
        }
    }

    class Oefening2_4
    {
        public static void Run()
        {
            Console.WriteLine("Bij oefening 1 hebben we gewerkt met +, -, * en / en het datatype int.");
            Console.WriteLine("Bij oefening 2 hebben we gewerkt met  %, werd er één opgeteld en één afgetrokken en kozen we voor het datatype double.");
            Console.WriteLine("Bij oefening 3 kwamen we te weten dat het datatype double niet volledig past in het datatype int  en dat het datatype int wel volledig past in het datatype double.");
            Console.WriteLine("Bij oefening 4 werkten we dan weer met het datatype string en hebben op deze manier onze les herhaald. Dat vind ik TOP.");
        }
    }

    class Oefening2_5
    {
        public static void Run()
        {
            Oefening2_1.Run();
            Console.WriteLine();
            Oefening2_2.Run(3.6482, 1000);
            Console.WriteLine();
            Oefening2_3.Run(5);
            Console.WriteLine();
            Oefening2_4.Run();
        }
    }
}
