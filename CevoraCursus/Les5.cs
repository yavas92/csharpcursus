﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CevoraCursus
{
    class Oefening5_1_1
    {
        public static void Run()
        {
            const double c = 299792458;
            double e, m;
            Console.WriteLine("Geef de massa in: ");
            m = Convert.ToDouble(Console.ReadLine());
            e = m * Math.Pow(c, 2);
            Console.WriteLine("E = m * c²");
            Console.WriteLine($"E = {Math.Round(e,2)}");

        }
    }
    class Oefening5_1_2
    {
        public static void Run()
        {

        }
    }
    class Oefening5_1_3
    {
        public static void Run()
        {

        }
    }
    class Oefening5_1_4
    {
        public static void Run()
        {

        }
    }
    class Oefening5_1_5
    {
        public static void Run()
        {

        }
    }
    class Oefening5_1_6
    {
        public static void Run()
        {

        }
    }
    class Oefening5_1_7
    {
        public static void Run()
        {

        }
    }

    class Oefening5_2_1
    {
        public static void Run()
        {

        }
    }
    class Oefening5_2_2
    {
        public static void Run()
        {

        }
    }
    class Oefening5_2_3
    {
        public static void Run()
        {

        }
    }
    class Oefening5_2_4
    {
        public static void Run()
        {

        }
    }
    class Oefening5_2_5
    {
        public static void Run()
        {

        }
    }
    class Oefening5_2_6
    {
        public static void Run()
        {

        }
    }
}
