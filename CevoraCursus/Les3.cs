﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace CevoraCursus
{
    class Oefening3_1
    {
        public static void Run()
        {
            double prijsFrisdrank = 2.35;
            int aantal;

            Console.WriteLine("Hoeveel frisdank wil je bestellen?");
            aantal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Bij het bestellen van {aantal} frisdrank zal je {Math.Round(prijsFrisdrank * aantal * 1.21 * 100) / 100} euro moeten betalen incl. BTW.");
        }
    }

    class Oefening3_2
    {
        public static void Run()
        {
            double gradenCelsius, gradenFahrenheit;

            Console.WriteLine("Hoeveel graden is het buiten? (in °C)");
            gradenCelsius = Convert.ToDouble(Console.ReadLine());
            gradenFahrenheit = gradenCelsius * 9 / 5 + 32;
            Console.WriteLine($"{gradenFahrenheit} aantal graden Fahrenheit is gelijk aan {gradenCelsius} graden Celsius.");
            //Console.WriteLine((gradenFahrenheit-32)/9*5 + " " + gradenCelsius);
        }
    }

    class Oefening3_3
    {
        public static void Run()
        {
            string naam, voornaam, straatnaam, postcode, hobbys;

            Console.WriteLine("Wat is uw naam?");
            naam = Console.ReadLine();
            Console.WriteLine("Wat is uw voornaam?");
            voornaam = Console.ReadLine();
            Console.WriteLine("Wat is uw straatnaam?");
            straatnaam = Console.ReadLine();
            Console.WriteLine("Wat is uw postcode?");
            postcode = Console.ReadLine();
            Console.WriteLine("Wat zijn uw hobby's?");
            hobbys = Console.ReadLine();

            Console.WriteLine($"\nDit zijn de gegevens van {naam} {voornaam}: \nStraatnaam: {straatnaam} \nPostcode: {postcode} \nHobby's: {hobbys} ");
        }
    }

    class Oefening3_4
    {
        public static void Run()
        {
            int aantal;
            double inkoopprijs, winstmarge;

            Console.WriteLine("Hoeveel frisdrank wil je inkopen?");
            aantal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wat is de inkoopprijs per frisdrank?");
            inkoopprijs = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoeveel winstmarge wil je nemen? (x %)");
            winstmarge = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine($"Als je {aantal} aantal frisdrank inkoopt, aan een inkoopprijs van {inkoopprijs} euro, en je wilt een winstmarge van {winstmarge}%. Dan zal je frisdrank verkopen aan {inkoopprijs * (100 + winstmarge) / 100} euro.");
        }
    }

    class Oefening3_5
    {
        public static void Run()
        {
            int aantalVolwassenen, aantalKinderen;

            Console.WriteLine("Welkom bij Pretpark C#! \nInkomprijzen: \nVolwassenen: 35 euro \tKinderen: 25 euro\n");
            Console.WriteLine("Met hoeveel volwassenen zijn jullie?");
            aantalVolwassenen = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Met hoeveel kinderen zijn jullie?");
            aantalKinderen = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"De totaalprijs van jullie inkom bedraag {aantalKinderen * 25 + aantalVolwassenen * 35} euro.");
        }
    }

    class Oefening3_6
    {
        public static void Run()
        {
            double pFrisdrank, pBier, pSterkeDrank;
            int tafelNummer, aantalFrisdrank, aantalBier, aantalSterkDrank;

            Console.WriteLine("Geef de prijs in van frisdrank: ");
            pFrisdrank = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef de prijs in van bier: ");
            pBier = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef de prijs in van sterke drank: ");
            pSterkeDrank = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Geef de bestelling hier");
            Console.WriteLine("Tafel nummer: ");
            tafelNummer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal frisdrank: ");
            aantalFrisdrank = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal bier: ");
            aantalBier = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal sterk drank: ");
            aantalSterkDrank = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine($"Tafel {tafelNummer} heeft {aantalFrisdrank} frisdrank, {aantalBier} bier en {aantalSterkDrank} sterke drank besteld vanavond. Zij zullen een totaal van {pFrisdrank * aantalFrisdrank + pBier * aantalBier + pSterkeDrank * aantalSterkDrank} euro moeten betalen.");
        }
    }

    class Oefening3_7
    {
        public static void Run()
        {
            int[] getallen = new int[3];

            for (int i = 0; i < getallen.Length; i++)
            {
                Console.WriteLine($"Geef {i + 1}e getal in: ");
                getallen[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine();

            Console.WriteLine($"{getallen[0]} - {getallen[1]} - {getallen[2]}");
            Console.WriteLine($"{getallen[0]} - {getallen[2]} - {getallen[1]}");
            Console.WriteLine($"{getallen[1]} - {getallen[0]} - {getallen[2]}");
            Console.WriteLine($"{getallen[1]} - {getallen[2]} - {getallen[0]}");
            Console.WriteLine($"{getallen[2]} - {getallen[0]} - {getallen[1]}");
            Console.WriteLine($"{getallen[2]} - {getallen[1]} - {getallen[0]}");
        }
    }

    class Oefening3_8
    {
        public static void Run()
        {
            int getal;
            Console.WriteLine("Welke maaltafel wil je zien?");
            getal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            for (int i = 0; i <= 10; i++)
            {
                Console.WriteLine($"{getal} x {i} = {getal * i}");
            }
            Console.WriteLine();
        }
    }

    class Oefening3_9
    {
        public static void Run()
        {
            double snelheid, afstand;

            Console.WriteLine("Wat is de snelheid? (in km/u)");
            snelheid = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Wat is de afstand? (in km)");
            afstand = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine($"De vrachtwagen rijdt tegen een snelheid van {snelheid} km/u een afstand van {afstand} km. Dan is de vrachtwagen {Math.Round(afstand / snelheid)} uur {(afstand % snelheid != 0 ? "en " + Math.Round((afstand%snelheid)/snelheid*60) + " minuten " : "")}onderweg.");
            Console.WriteLine();
        }
    }

    class Oefening3_10
    {
        public static void Run()
        {
            double prijzenpot;
            int aantalWinnaars;

            Console.WriteLine("Hoeveel bedraagt de prijzenpot?");
            prijzenpot = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoeveel winnaars zijn er?");
            aantalWinnaars = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine($"Proficiat! U bent bij de winnaars van deze lotto wedstrijd! De prijzenpot bedraagt {prijzenpot} euro. Er zijn deze editie {aantalWinnaars} aantal winnaars. Dat wilt zeggen dat u {Math.Round(prijzenpot/aantalWinnaars*100)/100} euro gewonnen heeft.");
        }
    }
}
