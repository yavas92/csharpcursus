﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CevoraCursus
{
    class Oefening6_1_1
    {
        public static void Run()
        {
            int[] arrGetallen;
            string[] arrNamen;

            Console.WriteLine("Hoeveel cijfers wil je ingeven?");
            arrGetallen = new int[Convert.ToInt32(Console.ReadLine())];

            Console.WriteLine($"Geef {arrGetallen.Length} cijfers in: ");
            for (int i = 0; i < arrGetallen.Length; i++)
            {
                arrGetallen[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine($"De array bestaat uit {arrGetallen.Length} getallen.");
            Console.WriteLine("Je hebt deze ingevoerd in deze volgorde:");
            foreach (int getal in arrGetallen)
            {
                Console.WriteLine($"{getal} ");
            }

            Console.WriteLine("Na gebruik van de .Sort-functie ziet deze er zo uit: ");
            Array.Sort(arrGetallen);

            foreach (int getal in arrGetallen)
            {
                Console.WriteLine($"{getal} ");
            }

            Console.WriteLine("Hoeveel namen wil je ingeven?");
            arrNamen = new string[Convert.ToInt32(Console.ReadLine())];

            Console.WriteLine($"Geef {arrNamen.Length} namen in: ");
            for (int i = 0; i < arrNamen.Length; i++)
            {
                arrNamen[i] = Console.ReadLine();
            }

            Console.WriteLine($"De array bestaat uit {arrNamen.Length} namen.");
            Console.WriteLine("Je hebt deze ingevoerd in deze volgorde:");
            foreach (string naam in arrNamen)
            {
                Console.WriteLine($"{naam} ");
            }

            Console.WriteLine("Na gebruik van de .Sort-functie ziet deze er zo uit: ");
            Array.Sort(arrNamen);

            foreach (string naam in arrNamen)
            {
                Console.WriteLine($"{naam} ");
            }
        }
    }

    class Oefening6_1_2
    {
        public static void Run()
        {
            string[] namen = new string[20] { "Arthur", "Noah", "Adam", "Louis", "Liam", "Lucas", "Jules", "Victor", "Gabriel", "Mohamed", "Emma", "Olivia", "Louise", "Mila", "Alice", "Juliette", "Elena", "Marie", "Sofia", "Lina" };
            int aantalStudenten, aantalVakken = 4, teller = 0;
            string[] studenten = new string[] { };
            int[,] punten;
            double[] gemiddelde;
            double klasgemiddelde = 0;
            Random random = new Random();

            Console.WriteLine("Hoeveel studenten moet u ingeven?");
            aantalStudenten = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            // Instancieren van de arrays
            punten = new int[aantalStudenten, aantalVakken];
            gemiddelde = new double[aantalStudenten];
            studenten = new string[aantalStudenten];

            do
            {
                studenten[teller] = namen[teller];

                for (int i = 0; i < aantalVakken; i++)
                {
                    punten[teller, i] = random.Next(101); // Leest punt in per vak
                    gemiddelde[teller] += punten[teller, i]; // Telt de punten per vak op zodat we de gemiddelde kunnen berekenen
                }
                gemiddelde[teller] /= aantalVakken; // Deelt de opgetelde punten door het aantal vakken, zo bekomen we de gemiddelde
                klasgemiddelde += gemiddelde[teller]; // Telt de gemiddelden van de studenten op zodat we de klasgemiddelde kunnen berekenen

            } while (++teller < aantalStudenten);

            klasgemiddelde /= aantalStudenten; // Deelt de opgetelde gemiddelden door het aantal studenten, zo bekomen we de klasgemiddelde



            teller = 0;

            do
            {
                Console.WriteLine($"Punten van {studenten[teller]} :");

                for (int i = 0; i < aantalVakken; i++)
                {
                    switch (i)
                    {
                        case 0:
                            Console.Write("Nederlands: ");
                            break;
                        case 1:
                            Console.Write("Frans: ");
                            break;
                        case 2:
                            Console.Write("Engels: ");
                            break;
                        case 3:
                            Console.Write("Wiskunde: ");
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine($"{punten[teller, i]}%");
                }
                Console.WriteLine($"Gemiddelde: {gemiddelde[teller]}%");
                Console.WriteLine();
            } while (++teller < aantalStudenten);

            Console.WriteLine($"Klasgemiddelde: {klasgemiddelde}%");

            Array.Sort(gemiddelde); // Array sorteren voor de mediaan te selecteren
            Console.WriteLine($"Mediaan: {gemiddelde[gemiddelde.Length / 2 - 1]}%");
        }
    }
    class Oefening6_1_3
    {
        public static void Run()
        {
            int[] arrGetallen = new int[1000];
            int[] arrGetallenAnalyse = new int[16];
            Random random = new Random();

            for (int i = 0; i < arrGetallen.Length; i++)
            {
                arrGetallen[i] = random.Next(1, 7) + random.Next(1, 7) + random.Next(1, 7);
                arrGetallenAnalyse[arrGetallen[i] - 3]++;
                //Console.WriteLine(arrGetallen[i]);
            }

            Console.WriteLine($"{arrGetallen.Length} keer 3 dobbelstenen gegooid.\n");

            Console.WriteLine($"Minst gegooide getal: {Array.IndexOf(arrGetallenAnalyse, arrGetallenAnalyse.Min()) + 3}\taantal keer gegooid: {arrGetallenAnalyse.Min()}");
            Console.WriteLine($"Meest gegooide getal: {Array.IndexOf(arrGetallenAnalyse, arrGetallenAnalyse.Max()) + 3}\taantal keer gegooid: {arrGetallenAnalyse.Max()}");
        }
    }

    class Oefening6_1_4
    {
        public static void Run()
        {
            double[] arrGetallen = new double[10] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            int index;
            Random random = new Random();

            Console.Write("BEGINWAARDE: ");
            foreach (double getal in arrGetallen)
            {
                Console.Write($"{getal} | ");
            }

            while (arrGetallen.Contains(0))
            {
                Console.WriteLine("\n\nIn welke index wil je een decimaal getal steken?");
                index = Convert.ToInt32(Console.ReadLine());

                arrGetallen[index] = Convert.ToDouble((random.NextDouble() * 10).ToString("0.00"));

                foreach (double getal in arrGetallen)
                {
                    Console.Write($"{getal} | ");
                }
            }

            Console.WriteLine("\nAlle indexen zijn ingevuld!");
        }
    }

    class Oefening6_1_7
    {
        public static void Run()
        {
            string[] namen = new string[20] { "Arthur", "Noah", "Adam", "Louis", "Liam", "Lucas", "Jules", "Victor", "Gabriel", "Mohamed", "Emma", "Olivia", "Louise", "Mila", "Alice", "Juliette", "Elena", "Marie", "Sofia", "Lina" };
            int aantalStudenten, aantalVakken = 4, teller = 0;
            string[] studenten = new string[] { };
            int[] pNederlands, pWiskunde, pGeschiedenis, pMuziek;
            double[] gemiddelde;
            Random random = new Random();

            Console.WriteLine("Hoeveel studenten moet u ingeven?");
            aantalStudenten = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            // Instancieren van de arrays
            pNederlands = new int[aantalStudenten];
            pWiskunde = new int[aantalStudenten];
            pGeschiedenis = new int[aantalStudenten];
            pMuziek = new int[aantalStudenten];
            gemiddelde = new double[aantalStudenten];
            studenten = new string[aantalStudenten];

            namen.OrderBy(x => random.Next()).ToArray();

            do
            {
                studenten[teller] = namen[teller];
                pNederlands[teller] = random.Next(101);
                pWiskunde[teller] = random.Next(101);
                pGeschiedenis[teller] = random.Next(101);
                pMuziek[teller] = random.Next(101);
                gemiddelde[teller] = (pNederlands[teller] + pWiskunde[teller] + pGeschiedenis[teller] + pMuziek[teller]) / aantalVakken;

            } while (++teller < aantalStudenten);


            Console.WriteLine("Nederlands".PadLeft(40) + "Wiskunde".PadLeft(20) + "Geschiedenis".PadLeft(20) + "Muziek".PadLeft(20) + Environment.NewLine);

            for (int i = 0; i < aantalStudenten; i++)
            {
                Console.WriteLine(studenten[i].PadLeft(20) + pNederlands[i].ToString().PadLeft(20) + pWiskunde[i].ToString().PadLeft(20) + pGeschiedenis[i].ToString().PadLeft(20) + pMuziek[i].ToString().PadLeft(20));
            }

            Console.WriteLine(Environment.NewLine + Environment.NewLine + "Per vak is...");

            Console.WriteLine("de hoogste score:".PadLeft(20) + pNederlands.Max().ToString().PadLeft(20) + pWiskunde.Max().ToString().PadLeft(20) + pGeschiedenis.Max().ToString().PadLeft(20) + pMuziek.Max().ToString().PadLeft(20));

            Console.WriteLine("de laagste score:".PadLeft(20) + pNederlands.Min().ToString().PadLeft(20) + pWiskunde.Min().ToString().PadLeft(20) + pGeschiedenis.Min().ToString().PadLeft(20) + pMuziek.Min().ToString().PadLeft(20));

            Console.WriteLine("de gemiddelde:".PadLeft(20) + pNederlands.Average().ToString().PadLeft(20) + pWiskunde.Average().ToString().PadLeft(20) + pGeschiedenis.Average().ToString().PadLeft(20) + pMuziek.Average().ToString().PadLeft(20));

            // We gaan de arrays hier sorteren; om de mediaan te selecteren; aangezien de punten al afgedrukt zijn
            Array.Sort(pNederlands);
            Array.Sort(pWiskunde);
            Array.Sort(pGeschiedenis);
            Array.Sort(pMuziek);
            Array.Sort(gemiddelde);

            Console.WriteLine("de mediaan:".PadLeft(20) + pNederlands[pNederlands.Length / 2 - 1].ToString().PadLeft(20) + pWiskunde[pWiskunde.Length / 2 - 1].ToString().PadLeft(20) + pGeschiedenis[pGeschiedenis.Length / 2 - 1].ToString().PadLeft(20) + pMuziek[pMuziek.Length / 2 - 1].ToString().PadLeft(20) + Environment.NewLine + Environment.NewLine);

            Console.WriteLine("Van de klas is ...".PadLeft(20));
            Console.WriteLine("de hoogste score:".PadLeft(20) + gemiddelde.Max().ToString().PadLeft(20));
            Console.WriteLine("de laagste score:".PadLeft(20) + gemiddelde.Min().ToString().PadLeft(20));
            Console.WriteLine("het gemiddelde:".PadLeft(20) + gemiddelde.Average().ToString().PadLeft(20));
            Console.WriteLine("de mediaan:".PadLeft(20) + gemiddelde[gemiddelde.Length / 2 - 1].ToString().PadLeft(20));
        }
    }
}
