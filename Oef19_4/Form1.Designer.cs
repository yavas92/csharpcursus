﻿namespace Oef19_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbGsmType = new System.Windows.Forms.GroupBox();
            this.rdbLuxe = new System.Windows.Forms.RadioButton();
            this.rdbToets = new System.Windows.Forms.RadioButton();
            this.rdbDraai = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.btnContactToevoegen = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNummer = new System.Windows.Forms.TextBox();
            this.btnBellen = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnContact = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnHistoriek = new System.Windows.Forms.Button();
            this.btnSluiten = new System.Windows.Forms.Button();
            this.nud = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.gbGsmType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.SuspendLayout();
            // 
            // gbGsmType
            // 
            this.gbGsmType.Controls.Add(this.rdbLuxe);
            this.gbGsmType.Controls.Add(this.rdbToets);
            this.gbGsmType.Controls.Add(this.rdbDraai);
            this.gbGsmType.Location = new System.Drawing.Point(13, 13);
            this.gbGsmType.Name = "gbGsmType";
            this.gbGsmType.Size = new System.Drawing.Size(200, 100);
            this.gbGsmType.TabIndex = 0;
            this.gbGsmType.TabStop = false;
            this.gbGsmType.Text = "GSMType";
            // 
            // rdbLuxe
            // 
            this.rdbLuxe.AutoSize = true;
            this.rdbLuxe.Location = new System.Drawing.Point(7, 66);
            this.rdbLuxe.Name = "rdbLuxe";
            this.rdbLuxe.Size = new System.Drawing.Size(93, 17);
            this.rdbLuxe.TabIndex = 0;
            this.rdbLuxe.Text = "Luxe Telefoon";
            this.rdbLuxe.UseVisualStyleBackColor = true;
            // 
            // rdbToets
            // 
            this.rdbToets.AutoSize = true;
            this.rdbToets.Location = new System.Drawing.Point(6, 43);
            this.rdbToets.Name = "rdbToets";
            this.rdbToets.Size = new System.Drawing.Size(97, 17);
            this.rdbToets.TabIndex = 0;
            this.rdbToets.Text = "Toets Telefoon";
            this.rdbToets.UseVisualStyleBackColor = true;
            // 
            // rdbDraai
            // 
            this.rdbDraai.AutoSize = true;
            this.rdbDraai.Checked = true;
            this.rdbDraai.Location = new System.Drawing.Point(7, 20);
            this.rdbDraai.Name = "rdbDraai";
            this.rdbDraai.Size = new System.Drawing.Size(95, 17);
            this.rdbDraai.TabIndex = 0;
            this.rdbDraai.TabStop = true;
            this.rdbDraai.Text = "Draai Telefoon";
            this.rdbDraai.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Voeg simpel contact toe";
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(13, 156);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(154, 20);
            this.txtContact.TabIndex = 1;
            // 
            // btnContactToevoegen
            // 
            this.btnContactToevoegen.Location = new System.Drawing.Point(264, 154);
            this.btnContactToevoegen.Name = "btnContactToevoegen";
            this.btnContactToevoegen.Size = new System.Drawing.Size(118, 23);
            this.btnContactToevoegen.TabIndex = 2;
            this.btnContactToevoegen.Text = "Contact toevoegen";
            this.btnContactToevoegen.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 192);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bel een nummer";
            // 
            // txtNummer
            // 
            this.txtNummer.Location = new System.Drawing.Point(12, 218);
            this.txtNummer.Name = "txtNummer";
            this.txtNummer.Size = new System.Drawing.Size(154, 20);
            this.txtNummer.TabIndex = 1;
            // 
            // btnBellen
            // 
            this.btnBellen.Location = new System.Drawing.Point(263, 216);
            this.btnBellen.Name = "btnBellen";
            this.btnBellen.Size = new System.Drawing.Size(118, 23);
            this.btnBellen.TabIndex = 2;
            this.btnBellen.Text = "Bellen";
            this.btnBellen.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Bel een contact";
            // 
            // btnContact
            // 
            this.btnContact.Location = new System.Drawing.Point(263, 285);
            this.btnContact.Name = "btnContact";
            this.btnContact.Size = new System.Drawing.Size(118, 23);
            this.btnContact.TabIndex = 2;
            this.btnContact.Text = "Contact";
            this.btnContact.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 325);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Bel opgeslagen info";
            // 
            // btnHistoriek
            // 
            this.btnHistoriek.Location = new System.Drawing.Point(263, 346);
            this.btnHistoriek.Name = "btnHistoriek";
            this.btnHistoriek.Size = new System.Drawing.Size(118, 23);
            this.btnHistoriek.TabIndex = 2;
            this.btnHistoriek.Text = "Historiek";
            this.btnHistoriek.UseVisualStyleBackColor = true;
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(12, 415);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(118, 23);
            this.btnSluiten.TabIndex = 2;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            // 
            // nud
            // 
            this.nud.Location = new System.Drawing.Point(13, 288);
            this.nud.Name = "nud";
            this.nud.Size = new System.Drawing.Size(120, 20);
            this.nud.TabIndex = 3;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(14, 349);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown3.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 450);
            this.Controls.Add(this.numericUpDown3);
            this.Controls.Add(this.nud);
            this.Controls.Add(this.btnSluiten);
            this.Controls.Add(this.btnHistoriek);
            this.Controls.Add(this.btnContact);
            this.Controls.Add(this.btnBellen);
            this.Controls.Add(this.btnContactToevoegen);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNummer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtContact);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbGsmType);
            this.Name = "Form1";
            this.Text = "Oef 19.4";
            this.gbGsmType.ResumeLayout(false);
            this.gbGsmType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbGsmType;
        private System.Windows.Forms.RadioButton rdbLuxe;
        private System.Windows.Forms.RadioButton rdbToets;
        private System.Windows.Forms.RadioButton rdbDraai;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.Button btnContactToevoegen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNummer;
        private System.Windows.Forms.Button btnBellen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnContact;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnHistoriek;
        private System.Windows.Forms.Button btnSluiten;
        private System.Windows.Forms.NumericUpDown nud;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
    }
}

