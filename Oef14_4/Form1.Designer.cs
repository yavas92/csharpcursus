﻿namespace Oef14_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.btnVerplaatsIndex = new System.Windows.Forms.Button();
            this.txtIndex = new System.Windows.Forms.TextBox();
            this.lbIndex = new System.Windows.Forms.Label();
            this.lblIndex2 = new System.Windows.Forms.Label();
            this.txtIndex2 = new System.Windows.Forms.TextBox();
            this.btnWisselIndex = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Enabled = false;
            this.txtOutput.Location = new System.Drawing.Point(15, 83);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(221, 281);
            this.txtOutput.TabIndex = 0;
            // 
            // btnVerplaatsIndex
            // 
            this.btnVerplaatsIndex.Location = new System.Drawing.Point(319, 14);
            this.btnVerplaatsIndex.Name = "btnVerplaatsIndex";
            this.btnVerplaatsIndex.Size = new System.Drawing.Size(98, 23);
            this.btnVerplaatsIndex.TabIndex = 1;
            this.btnVerplaatsIndex.Text = "Verplaats index";
            this.btnVerplaatsIndex.UseVisualStyleBackColor = true;
            this.btnVerplaatsIndex.Click += new System.EventHandler(this.btnVerplaatsIndex_Click);
            // 
            // txtIndex
            // 
            this.txtIndex.Location = new System.Drawing.Point(174, 16);
            this.txtIndex.Name = "txtIndex";
            this.txtIndex.Size = new System.Drawing.Size(100, 20);
            this.txtIndex.TabIndex = 2;
            // 
            // lbIndex
            // 
            this.lbIndex.AutoSize = true;
            this.lbIndex.Location = new System.Drawing.Point(12, 19);
            this.lbIndex.Name = "lbIndex";
            this.lbIndex.Size = new System.Drawing.Size(156, 13);
            this.lbIndex.TabIndex = 3;
            this.lbIndex.Text = "Welke index wil je verplaatsen?";
            // 
            // lblIndex2
            // 
            this.lblIndex2.AutoSize = true;
            this.lblIndex2.Location = new System.Drawing.Point(12, 45);
            this.lblIndex2.Name = "lblIndex2";
            this.lblIndex2.Size = new System.Drawing.Size(156, 13);
            this.lblIndex2.TabIndex = 5;
            this.lblIndex2.Text = "Welke index wil je verplaatsen?";
            // 
            // txtIndex2
            // 
            this.txtIndex2.Location = new System.Drawing.Point(174, 42);
            this.txtIndex2.Name = "txtIndex2";
            this.txtIndex2.Size = new System.Drawing.Size(100, 20);
            this.txtIndex2.TabIndex = 4;
            // 
            // btnWisselIndex
            // 
            this.btnWisselIndex.Location = new System.Drawing.Point(319, 40);
            this.btnWisselIndex.Name = "btnWisselIndex";
            this.btnWisselIndex.Size = new System.Drawing.Size(98, 23);
            this.btnWisselIndex.TabIndex = 6;
            this.btnWisselIndex.Text = "Wissel index";
            this.btnWisselIndex.UseVisualStyleBackColor = true;
            this.btnWisselIndex.Click += new System.EventHandler(this.btnWisselIndex_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 387);
            this.Controls.Add(this.btnWisselIndex);
            this.Controls.Add(this.lblIndex2);
            this.Controls.Add(this.txtIndex2);
            this.Controls.Add(this.lbIndex);
            this.Controls.Add(this.txtIndex);
            this.Controls.Add(this.btnVerplaatsIndex);
            this.Controls.Add(this.txtOutput);
            this.Name = "Form1";
            this.Text = "Oef 14.4";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button btnVerplaatsIndex;
        private System.Windows.Forms.TextBox txtIndex;
        private System.Windows.Forms.Label lbIndex;
        private System.Windows.Forms.Label lblIndex2;
        private System.Windows.Forms.TextBox txtIndex2;
        private System.Windows.Forms.Button btnWisselIndex;
    }
}

