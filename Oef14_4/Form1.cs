﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef14_4
{
    public partial class Form1 : Form
    {
        int[,] arrGetallen = new int[3, 5];
        int[] arrTeVerplaatsen = new int[3];

        public Form1()
        {
            InitializeComponent();
        }

        private void btnVerplaatsIndex_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(txtIndex.Text);

            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                arrTeVerplaatsen[i] = arrGetallen[i, index];
            }

            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                for (int j = index; j > 0; j--)
                {
                    arrGetallen[i, j] = arrGetallen[i, j - 1];
                }
            }

            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                arrGetallen[i, 0] = arrTeVerplaatsen[i];
            }

            txtOutput.Text += Environment.NewLine + Environment.NewLine + $"Index {index} is verplaatst naar index 0." + Environment.NewLine;
            ArrayAfdrukken();
        }

        private void btnWisselIndex_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(txtIndex.Text);
            int index2 = Convert.ToInt32(txtIndex2.Text);

            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                arrTeVerplaatsen[i] = arrGetallen[i, index];
                arrGetallen[i, index] = arrGetallen[i, index2];
                arrGetallen[i, index2] = arrTeVerplaatsen[i];
            }

            txtOutput.Text += Environment.NewLine + Environment.NewLine + $"Index {index} heeft gewisseld met index {index2}." + Environment.NewLine;
            ArrayAfdrukken();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtOutput.Text += "Huidige array:" + Environment.NewLine;
            GenereerArray();
        }

        void GenereerArray()
        {
            Random random = new Random();
            for (int rij = 0; rij <= arrGetallen.GetUpperBound(0); rij++)
            {
                for (int kol = 0; kol <= arrGetallen.GetUpperBound(1); kol++)
                {
                    arrGetallen[rij, kol] = random.Next(101);
                }
            }

            ArrayAfdrukken();
        }

        private void ArrayAfdrukken()
        {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i <= arrGetallen.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= arrGetallen.GetUpperBound(1); j++)
                {
                    output.Append(arrGetallen[i, j].ToString().PadRight(10));
                }
                output.Append(Environment.NewLine);
            }
            txtOutput.Text += output.ToString();
        }
    }
}
