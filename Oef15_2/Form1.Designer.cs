﻿namespace Oef15_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutput = new System.Windows.Forms.Label();
            this.btnVermeerder = new System.Windows.Forms.Button();
            this.btnVerminder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.AutoSize = true;
            this.txtOutput.Font = new System.Drawing.Font("Courier New", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutput.Location = new System.Drawing.Point(37, 33);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(62, 18);
            this.txtOutput.TabIndex = 0;
            this.txtOutput.Text = "label1";
            this.txtOutput.Click += new System.EventHandler(this.txtOutput_Click);
            // 
            // btnVermeerder
            // 
            this.btnVermeerder.Location = new System.Drawing.Point(40, 78);
            this.btnVermeerder.Name = "btnVermeerder";
            this.btnVermeerder.Size = new System.Drawing.Size(196, 50);
            this.btnVermeerder.TabIndex = 1;
            this.btnVermeerder.Text = "Vermeerder";
            this.btnVermeerder.UseVisualStyleBackColor = true;
            this.btnVermeerder.Click += new System.EventHandler(this.btnVermeerder_Click);
            // 
            // btnVerminder
            // 
            this.btnVerminder.Location = new System.Drawing.Point(40, 134);
            this.btnVerminder.Name = "btnVerminder";
            this.btnVerminder.Size = new System.Drawing.Size(196, 50);
            this.btnVerminder.TabIndex = 2;
            this.btnVerminder.Text = "Verminder";
            this.btnVerminder.UseVisualStyleBackColor = true;
            this.btnVerminder.Click += new System.EventHandler(this.btnVerminder_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 209);
            this.Controls.Add(this.btnVerminder);
            this.Controls.Add(this.btnVermeerder);
            this.Controls.Add(this.txtOutput);
            this.Name = "Form1";
            this.Text = "Oef 15.2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtOutput;
        private System.Windows.Forms.Button btnVermeerder;
        private System.Windows.Forms.Button btnVerminder;
    }
}

