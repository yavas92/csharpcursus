﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_2
{
    public partial class Form1 : Form
    {
        TV tv = new TV();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void btnVermeerder_Click(object sender, EventArgs e)
        {
            tv.VermeerderKanaal();
            tv.VermeerderVolume();
            UpdateOutput();
        }

        void UpdateOutput()
        {
            txtOutput.Text = tv.ToonGegevens();
        }

        private void btnVerminder_Click(object sender, EventArgs e)
        {
            tv.VerminderKanaal();
            tv.VerminderVolume();
            UpdateOutput();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateOutput();

        }

        private void txtOutput_Click(object sender, EventArgs e)
        {

        }
    }
}
