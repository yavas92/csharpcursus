﻿namespace Oef15_6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBereken = new System.Windows.Forms.Button();
            this.lblNaam = new System.Windows.Forms.Label();
            this.lblGewicht = new System.Windows.Forms.Label();
            this.lblLengte = new System.Windows.Forms.Label();
            this.lblKg = new System.Windows.Forms.Label();
            this.lblM = new System.Windows.Forms.Label();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.txtGewicht = new System.Windows.Forms.TextBox();
            this.txtLengte = new System.Windows.Forms.TextBox();
            this.txtGegevens = new System.Windows.Forms.TextBox();
            this.txtBMI = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnBereken
            // 
            this.btnBereken.Location = new System.Drawing.Point(233, 12);
            this.btnBereken.Name = "btnBereken";
            this.btnBereken.Size = new System.Drawing.Size(107, 76);
            this.btnBereken.TabIndex = 0;
            this.btnBereken.Text = "Bereken";
            this.btnBereken.UseVisualStyleBackColor = true;
            this.btnBereken.Click += new System.EventHandler(this.btnBereken_Click);
            // 
            // lblNaam
            // 
            this.lblNaam.AutoSize = true;
            this.lblNaam.Location = new System.Drawing.Point(18, 15);
            this.lblNaam.Name = "lblNaam";
            this.lblNaam.Size = new System.Drawing.Size(41, 13);
            this.lblNaam.TabIndex = 1;
            this.lblNaam.Text = "Naam: ";
            // 
            // lblGewicht
            // 
            this.lblGewicht.AutoSize = true;
            this.lblGewicht.Location = new System.Drawing.Point(16, 45);
            this.lblGewicht.Name = "lblGewicht";
            this.lblGewicht.Size = new System.Drawing.Size(52, 13);
            this.lblGewicht.TabIndex = 1;
            this.lblGewicht.Text = "Gewicht: ";
            // 
            // lblLengte
            // 
            this.lblLengte.AutoSize = true;
            this.lblLengte.Location = new System.Drawing.Point(16, 71);
            this.lblLengte.Name = "lblLengte";
            this.lblLengte.Size = new System.Drawing.Size(43, 13);
            this.lblLengte.TabIndex = 1;
            this.lblLengte.Text = "Lengte:";
            // 
            // lblKg
            // 
            this.lblKg.AutoSize = true;
            this.lblKg.Location = new System.Drawing.Point(191, 45);
            this.lblKg.Name = "lblKg";
            this.lblKg.Size = new System.Drawing.Size(19, 13);
            this.lblKg.TabIndex = 1;
            this.lblKg.Text = "kg";
            // 
            // lblM
            // 
            this.lblM.AutoSize = true;
            this.lblM.Location = new System.Drawing.Point(195, 71);
            this.lblM.Name = "lblM";
            this.lblM.Size = new System.Drawing.Size(15, 13);
            this.lblM.TabIndex = 1;
            this.lblM.Text = "m";
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(85, 12);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(100, 20);
            this.txtNaam.TabIndex = 2;
            // 
            // txtGewicht
            // 
            this.txtGewicht.Location = new System.Drawing.Point(85, 42);
            this.txtGewicht.Name = "txtGewicht";
            this.txtGewicht.Size = new System.Drawing.Size(100, 20);
            this.txtGewicht.TabIndex = 2;
            this.txtGewicht.Leave += new System.EventHandler(this.txtGewicht_Leave);
            // 
            // txtLengte
            // 
            this.txtLengte.Location = new System.Drawing.Point(85, 68);
            this.txtLengte.Name = "txtLengte";
            this.txtLengte.Size = new System.Drawing.Size(100, 20);
            this.txtLengte.TabIndex = 2;
            this.txtLengte.Leave += new System.EventHandler(this.txtLengte_Leave);
            // 
            // txtGegevens
            // 
            this.txtGegevens.Enabled = false;
            this.txtGegevens.Location = new System.Drawing.Point(19, 112);
            this.txtGegevens.Multiline = true;
            this.txtGegevens.Name = "txtGegevens";
            this.txtGegevens.Size = new System.Drawing.Size(321, 94);
            this.txtGegevens.TabIndex = 3;
            // 
            // txtBMI
            // 
            this.txtBMI.Enabled = false;
            this.txtBMI.Location = new System.Drawing.Point(19, 212);
            this.txtBMI.Multiline = true;
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.Size = new System.Drawing.Size(321, 29);
            this.txtBMI.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 254);
            this.Controls.Add(this.txtBMI);
            this.Controls.Add(this.txtGegevens);
            this.Controls.Add(this.txtLengte);
            this.Controls.Add(this.txtGewicht);
            this.Controls.Add(this.txtNaam);
            this.Controls.Add(this.lblM);
            this.Controls.Add(this.lblKg);
            this.Controls.Add(this.lblLengte);
            this.Controls.Add(this.lblGewicht);
            this.Controls.Add(this.lblNaam);
            this.Controls.Add(this.btnBereken);
            this.Name = "Form1";
            this.Text = "Bereken je eigen BMI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBereken;
        private System.Windows.Forms.Label lblNaam;
        private System.Windows.Forms.Label lblGewicht;
        private System.Windows.Forms.Label lblLengte;
        private System.Windows.Forms.Label lblKg;
        private System.Windows.Forms.Label lblM;
        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.TextBox txtGewicht;
        private System.Windows.Forms.TextBox txtLengte;
        private System.Windows.Forms.TextBox txtGegevens;
        private System.Windows.Forms.TextBox txtBMI;
    }
}

