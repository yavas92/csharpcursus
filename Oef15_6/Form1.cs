﻿using Oef15_5;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef15_6
{
    public partial class Form1 : Form
    {
        BMI bmi = new BMI();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBereken_Click(object sender, EventArgs e)
        {
            if (txtNaam.Text == "")
            {
                MessageBox.Show("Geen naam opgegeven!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (!double.TryParse(txtGewicht.Text, out double x) || !double.TryParse(txtLengte.Text, out double y))
            {
                MessageBox.Show("Gewicht of Lengte is niet numeriek!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bmi.Naam = txtNaam.Text;
                bmi.Gewicht = Convert.ToDouble(txtGewicht.Text);
                bmi.Lengte = Convert.ToDouble(txtLengte.Text);
                txtGegevens.Text = bmi.ToonGegevens();
                txtBMI.Text = "BMI: " + bmi.BerekenBmi().ToString();
            }

        }

        private void txtGewicht_Leave(object sender, EventArgs e)
        {
            txtGewicht.Text = txtGewicht.Text.Replace(',', '.');
        }

        private void txtLengte_Leave(object sender, EventArgs e)
        {
            txtLengte.Text = txtLengte.Text.Replace(',', '.');
        }
    }
}
