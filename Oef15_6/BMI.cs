﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef15_5
{
    class BMI
    {
        // Variabelen
        private string _naam;
        private double _gewicht;
        private double _lengte;

        // Properties
        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }

        public double Gewicht
        {
            get { return _gewicht; }
            set { _gewicht = value; }
        }

        public double Lengte
        {
            get { return _lengte; }
            set { _lengte = value; }
        }

        // Constructor
        public BMI()
        {

        }

        public BMI(string naam, double gewicht, double lengte)
        {
            Naam = naam;
            Gewicht = gewicht;
            Lengte = lengte;
        }

        // Methods
        public string ToonGegevens()
        {
            string output = $"Naam: {Naam + Environment.NewLine}Soort gewicht: ";
            double BMI = BerekenBmi();

            if (BMI < 18.5)
                output += "Ondergewicht";
            else if (18.5 <= BMI && BMI < 25)
                output += "Gezond gewicht";
            else if (25 <= BMI && BMI < 30)
                output += "Overgewicht";
            else
                output += "Obesitas";

            output += Environment.NewLine;

            return output;
        }

        public double BerekenBmi()
        {
            return Math.Round(Gewicht / Math.Pow(Lengte, 2) * 100000) / 10;
        }
    }
}
