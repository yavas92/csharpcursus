﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace Les13
{
    public partial class Oef13_1 : Form
    {
        public Oef13_1()
        {
            InitializeComponent();
            ListViewItem lv = new ListViewItem(txtNaam.Text);
            lv.SubItems.Add(new ListViewSubItem(lv, "33"));
            lv.SubItems.Add(new ListViewSubItem(lv, "Man"));
            lvNamen.Items.Add(lv);

        }

        private void btnToevoegen_Click(object sender, EventArgs e)
        {
            // Valideren
            // txtName valideren
            if (string.IsNullOrWhiteSpace(txtNaam.Text))
            {
                MessageBox.Show("Gelieve een naam in te vullen.", "Ontbrekende data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (!(rbtnMan.Checked || rbtnVrouw.Checked || rbtnManVrouw.Checked))
            {
                MessageBox.Show("Gelieve een geslacht te selecteren.", "Ontbrekende data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (string.IsNullOrWhiteSpace(cmbCategorie.Text))
            {
                MessageBox.Show("Gelieve een categorie te selecteren.", "Ontbrekende data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            lstNamen.Items.Add(txtNaam.Text);
        }

        private void btnVerwijderen_Click(object sender, EventArgs e)
        {
            lstNamen.Items.RemoveAt(lstNamen.SelectedIndex);
        }
    }
}
