﻿namespace Les13
{
    partial class Oef13_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("");
            this.lblNaam = new System.Windows.Forms.Label();
            this.lblGeslacht = new System.Windows.Forms.Label();
            this.lblCategorie = new System.Windows.Forms.Label();
            this.btnToevoegen = new System.Windows.Forms.Button();
            this.btnVerwijderen = new System.Windows.Forms.Button();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.cmbCategorie = new System.Windows.Forms.ComboBox();
            this.rbtnMan = new System.Windows.Forms.RadioButton();
            this.rbtnVrouw = new System.Windows.Forms.RadioButton();
            this.rbtnManVrouw = new System.Windows.Forms.RadioButton();
            this.cbInvalide = new System.Windows.Forms.CheckBox();
            this.lstNamen = new System.Windows.Forms.ListBox();
            this.lvNamen = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lblNaam
            // 
            this.lblNaam.AutoSize = true;
            this.lblNaam.Location = new System.Drawing.Point(12, 17);
            this.lblNaam.Name = "lblNaam";
            this.lblNaam.Size = new System.Drawing.Size(38, 13);
            this.lblNaam.TabIndex = 0;
            this.lblNaam.Text = "Naam:";
            // 
            // lblGeslacht
            // 
            this.lblGeslacht.AutoSize = true;
            this.lblGeslacht.Location = new System.Drawing.Point(12, 42);
            this.lblGeslacht.Name = "lblGeslacht";
            this.lblGeslacht.Size = new System.Drawing.Size(52, 13);
            this.lblGeslacht.TabIndex = 1;
            this.lblGeslacht.Text = "Geslacht:";
            // 
            // lblCategorie
            // 
            this.lblCategorie.AutoSize = true;
            this.lblCategorie.Location = new System.Drawing.Point(12, 68);
            this.lblCategorie.Name = "lblCategorie";
            this.lblCategorie.Size = new System.Drawing.Size(55, 13);
            this.lblCategorie.TabIndex = 2;
            this.lblCategorie.Text = "Categorie:";
            // 
            // btnToevoegen
            // 
            this.btnToevoegen.Location = new System.Drawing.Point(241, 120);
            this.btnToevoegen.Name = "btnToevoegen";
            this.btnToevoegen.Size = new System.Drawing.Size(95, 55);
            this.btnToevoegen.TabIndex = 7;
            this.btnToevoegen.Text = "Toevoegen";
            this.btnToevoegen.UseVisualStyleBackColor = true;
            this.btnToevoegen.Click += new System.EventHandler(this.btnToevoegen_Click);
            // 
            // btnVerwijderen
            // 
            this.btnVerwijderen.Location = new System.Drawing.Point(241, 180);
            this.btnVerwijderen.Name = "btnVerwijderen";
            this.btnVerwijderen.Size = new System.Drawing.Size(95, 55);
            this.btnVerwijderen.TabIndex = 8;
            this.btnVerwijderen.Text = "Verwijderen";
            this.btnVerwijderen.UseVisualStyleBackColor = true;
            this.btnVerwijderen.Click += new System.EventHandler(this.btnVerwijderen_Click);
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(75, 14);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(261, 20);
            this.txtNaam.TabIndex = 9;
            // 
            // cmbCategorie
            // 
            this.cmbCategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategorie.FormattingEnabled = true;
            this.cmbCategorie.Items.AddRange(new object[] {
            "< 3 j",
            "3 - 6 j",
            "6 - 12 j",
            "12 - 18 j",
            "18 - 65 j",
            "> 65 j"});
            this.cmbCategorie.Location = new System.Drawing.Point(75, 65);
            this.cmbCategorie.Name = "cmbCategorie";
            this.cmbCategorie.Size = new System.Drawing.Size(261, 21);
            this.cmbCategorie.TabIndex = 10;
            // 
            // rbtnMan
            // 
            this.rbtnMan.AutoSize = true;
            this.rbtnMan.Location = new System.Drawing.Point(75, 40);
            this.rbtnMan.Name = "rbtnMan";
            this.rbtnMan.Size = new System.Drawing.Size(46, 17);
            this.rbtnMan.TabIndex = 11;
            this.rbtnMan.TabStop = true;
            this.rbtnMan.Text = "Man";
            this.rbtnMan.UseVisualStyleBackColor = true;
            // 
            // rbtnVrouw
            // 
            this.rbtnVrouw.AutoSize = true;
            this.rbtnVrouw.Location = new System.Drawing.Point(127, 40);
            this.rbtnVrouw.Name = "rbtnVrouw";
            this.rbtnVrouw.Size = new System.Drawing.Size(55, 17);
            this.rbtnVrouw.TabIndex = 12;
            this.rbtnVrouw.TabStop = true;
            this.rbtnVrouw.Text = "Vrouw";
            this.rbtnVrouw.UseVisualStyleBackColor = true;
            // 
            // rbtnManVrouw
            // 
            this.rbtnManVrouw.AutoSize = true;
            this.rbtnManVrouw.Location = new System.Drawing.Point(188, 40);
            this.rbtnManVrouw.Name = "rbtnManVrouw";
            this.rbtnManVrouw.Size = new System.Drawing.Size(60, 17);
            this.rbtnManVrouw.TabIndex = 13;
            this.rbtnManVrouw.TabStop = true;
            this.rbtnManVrouw.Text = "XM/XV";
            this.rbtnManVrouw.UseVisualStyleBackColor = true;
            // 
            // cbInvalide
            // 
            this.cbInvalide.AutoSize = true;
            this.cbInvalide.Location = new System.Drawing.Point(75, 97);
            this.cbInvalide.Name = "cbInvalide";
            this.cbInvalide.Size = new System.Drawing.Size(63, 17);
            this.cbInvalide.TabIndex = 14;
            this.cbInvalide.Text = "Invalide";
            this.cbInvalide.UseVisualStyleBackColor = true;
            // 
            // lstNamen
            // 
            this.lstNamen.FormattingEnabled = true;
            this.lstNamen.Location = new System.Drawing.Point(12, 120);
            this.lstNamen.Name = "lstNamen";
            this.lstNamen.Size = new System.Drawing.Size(217, 186);
            this.lstNamen.TabIndex = 15;
            // 
            // lvNamen
            // 
            this.lvNamen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvNamen.HideSelection = false;
            this.lvNamen.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem4,
            listViewItem5,
            listViewItem6});
            this.lvNamen.Location = new System.Drawing.Point(12, 317);
            this.lvNamen.Name = "lvNamen";
            this.lvNamen.Size = new System.Drawing.Size(265, 169);
            this.lvNamen.TabIndex = 16;
            this.lvNamen.UseCompatibleStateImageBehavior = false;
            this.lvNamen.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Naam";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Geslacht";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Categorie";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Invalide";
            // 
            // Oef13_1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 498);
            this.Controls.Add(this.lvNamen);
            this.Controls.Add(this.lstNamen);
            this.Controls.Add(this.cbInvalide);
            this.Controls.Add(this.rbtnManVrouw);
            this.Controls.Add(this.rbtnVrouw);
            this.Controls.Add(this.rbtnMan);
            this.Controls.Add(this.cmbCategorie);
            this.Controls.Add(this.txtNaam);
            this.Controls.Add(this.btnVerwijderen);
            this.Controls.Add(this.btnToevoegen);
            this.Controls.Add(this.lblCategorie);
            this.Controls.Add(this.lblGeslacht);
            this.Controls.Add(this.lblNaam);
            this.Name = "Oef13_1";
            this.Text = "Oef13_1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNaam;
        private System.Windows.Forms.Label lblGeslacht;
        private System.Windows.Forms.Label lblCategorie;
        private System.Windows.Forms.Button btnToevoegen;
        private System.Windows.Forms.Button btnVerwijderen;
        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.ComboBox cmbCategorie;
        private System.Windows.Forms.RadioButton rbtnMan;
        private System.Windows.Forms.RadioButton rbtnVrouw;
        private System.Windows.Forms.RadioButton rbtnManVrouw;
        private System.Windows.Forms.CheckBox cbInvalide;
        private System.Windows.Forms.ListBox lstNamen;
        private System.Windows.Forms.ListView lvNamen;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
    }
}