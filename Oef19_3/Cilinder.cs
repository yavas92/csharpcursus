﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef19_3
{
    class Cilinder : Cirkel
    {
        // Attributen
        private double _h;

        // Properties
        public double H
        {
            get { return _h; }
            set { _h = value; }
        }


        // Constructor
        public Cilinder() : base()
        {

        }

        public Cilinder(double x, double y, double r, double h) : base(x, y, r)
        {
            H = h;
        }

        // Methods
        public override string Gegevens()
        {
            return base.Gegevens() + $"Hoogte: {H}" + Environment.NewLine + $"Oppervlakte: {Oppervlakte()}" + Environment.NewLine + $"Volume: {Volume()}";
        }

        public double Volume()
        {
            return base.Oppervlakte() * H;
        }

        public override double Oppervlakte()
        {
            return 2 * base.Oppervlakte() + base.Omtrek() * H;
        }

        public override string ToString()
        {
            return $"({X}, {Y}) coördinaten" + Environment.NewLine + $"Straal: {R}" + Environment.NewLine + $"Hoogte: {H}" + Environment.NewLine;
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            Cilinder c = (Cilinder)obj;

            return this.H == c.H;
        }
    }
}
