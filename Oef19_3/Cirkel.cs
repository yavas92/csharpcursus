﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef19_3
{
    class Cirkel : Punt
    {
        // Attributen
        private double _r;


        // Properties

        public double R
        {
            get { return _r; }
            set { _r = value; }
        }


        // Constructor
        public Cirkel() : base()
        {

        }

        public Cirkel(double x, double y, double r) : base(x, y)
        {
            R = r;
        }

        // Methods
        public virtual string Gegevens()
        {
            return base.ToString() + $"Straal: {R}" + Environment.NewLine + $"Oppervlakte: {Oppervlakte()}" + Environment.NewLine + $"Omtrek: {Omtrek()}" + Environment.NewLine;
        }

        public double Omtrek()
        {
            return 2 * R * Math.PI;
        }

        public virtual double Oppervlakte()
        {
            return R * R * Math.PI;
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            Cirkel c = (Cirkel)obj;

            return this.R==c.R;
        }
    }
}

