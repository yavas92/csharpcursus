﻿namespace Oef19_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPunt = new System.Windows.Forms.Button();
            this.btnCirkel = new System.Windows.Forms.Button();
            this.btnCilinder = new System.Windows.Forms.Button();
            this.btnEquals = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPunt
            // 
            this.btnPunt.Location = new System.Drawing.Point(51, 31);
            this.btnPunt.Name = "btnPunt";
            this.btnPunt.Size = new System.Drawing.Size(208, 70);
            this.btnPunt.TabIndex = 0;
            this.btnPunt.Text = "Test Punt";
            this.btnPunt.UseVisualStyleBackColor = true;
            this.btnPunt.Click += new System.EventHandler(this.btnPunt_Click);
            // 
            // btnCirkel
            // 
            this.btnCirkel.Location = new System.Drawing.Point(51, 115);
            this.btnCirkel.Name = "btnCirkel";
            this.btnCirkel.Size = new System.Drawing.Size(208, 70);
            this.btnCirkel.TabIndex = 0;
            this.btnCirkel.Text = "Test Cirkel";
            this.btnCirkel.UseVisualStyleBackColor = true;
            this.btnCirkel.Click += new System.EventHandler(this.btnCirkel_Click);
            // 
            // btnCilinder
            // 
            this.btnCilinder.Location = new System.Drawing.Point(51, 203);
            this.btnCilinder.Name = "btnCilinder";
            this.btnCilinder.Size = new System.Drawing.Size(208, 70);
            this.btnCilinder.TabIndex = 0;
            this.btnCilinder.Text = "Test Cilinder";
            this.btnCilinder.UseVisualStyleBackColor = true;
            this.btnCilinder.Click += new System.EventHandler(this.btnCilinder_Click);
            // 
            // btnEquals
            // 
            this.btnEquals.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEquals.Location = new System.Drawing.Point(120, 298);
            this.btnEquals.Name = "btnEquals";
            this.btnEquals.Size = new System.Drawing.Size(75, 23);
            this.btnEquals.TabIndex = 1;
            this.btnEquals.Text = "Equals?";
            this.btnEquals.UseVisualStyleBackColor = true;
            this.btnEquals.Click += new System.EventHandler(this.btnEquals_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 333);
            this.Controls.Add(this.btnEquals);
            this.Controls.Add(this.btnCilinder);
            this.Controls.Add(this.btnCirkel);
            this.Controls.Add(this.btnPunt);
            this.Name = "Form1";
            this.Text = "Overerving";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPunt;
        private System.Windows.Forms.Button btnCirkel;
        private System.Windows.Forms.Button btnCilinder;
        private System.Windows.Forms.Button btnEquals;
    }
}

