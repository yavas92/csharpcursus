﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oef19_3
{
    public partial class Form1 : Form
    {
        Punt punt;
        Cirkel cirkel;
        Cilinder cilinder;

        public Form1()
        {
            InitializeComponent();

            punt = new Punt(3, 4);
            cirkel = new Cirkel(3, 4, 5);
            cilinder = new Cilinder(3, 4, 1, 2);
        }

        private void btnPunt_Click(object sender, EventArgs e)
        {
            MessageBox.Show(punt.ToString(), "Coördinaten van het punt");
        }

        private void btnCirkel_Click(object sender, EventArgs e)
        {
            MessageBox.Show(cirkel.Gegevens(), "Coördinaten van de cirkel");
        }

        private void btnCilinder_Click(object sender, EventArgs e)
        {
            MessageBox.Show(cilinder.Gegevens(), "Gegevens van de cilinder");
        }

        private void btnEquals_Click(object sender, EventArgs e)
        {
            Punt p1 = new Punt(3, 4);
            Punt p2 = new Punt(5, 4);
            Punt p3 = new Punt(3, 4);
            Cirkel c1 = new Cirkel(3, 4, 5);
            Cirkel c2 = new Cirkel(4, 4, 5);
            Cirkel c3 = new Cirkel(3, 4, 5);
            Cilinder cil1 = new Cilinder(3, 4, 5, 6);
            Cilinder cil2 = new Cilinder(3, 4, 5, 8);
            Cilinder cil3 = new Cilinder(3, 4, 5, 6);

            MessageBox.Show("p1.Equals(p2) =" + p1.Equals(p2));
            MessageBox.Show("p1.Equals(p3) =" + p1.Equals(p3));

            MessageBox.Show("c1.Equals(c2) =" + c1.Equals(c2));
            MessageBox.Show("c1.Equals(c3) =" + c1.Equals(c3));

            MessageBox.Show("cil1.Equals(cil2) =" + cil1.Equals(cil2));
            MessageBox.Show("cil1.Equals(cil3) =" + cil1.Equals(cil3));

        }
    }
}
