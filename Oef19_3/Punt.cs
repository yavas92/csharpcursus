﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef19_3
{
    class Punt
    {
        // Variabelen
        private double _x;
        private double _y;

        // Properties
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }


        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }


        // Constructor
        public Punt()
        {

        }

        public Punt(double x, double y)
        {
            X = x;
            Y = y;
        }

        // Methodes
        public override string ToString()
        {
            return $"({X}, {Y}) coördinaten" + Environment.NewLine;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !GetType().Equals(obj.GetType())) return false;

            Punt p = (Punt)obj;

            return this.X == p.X && this.Y == p.Y;
        }
    }
}
